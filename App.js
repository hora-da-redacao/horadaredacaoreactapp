/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { Provider } from 'react-redux';
import { Router, Scene, Drawer, Stack, Lightbox } from 'react-native-router-flux';
import {Platform, StyleSheet, Text, View, ImageBackground, Image, TouchableOpacity, TouchableWithoutFeedback, Modal} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import OneSignal from 'react-native-onesignal'; 
import SplashScreen from 'react-native-splash-screen';


//Componentes
import PdfView from './app/component/pdfview';
import VideoView from './app/component/videoview';
import AudioPlayer from './app/component/soundplayer';
import PlayerScreen from './app/component/playerscreen';
import RootModal from './app/modals/RootModal';
import CompreModal from './app/modals/CompreModal';


//Scenes
import Login from './app/scenes/login';
import ComeceGratis from './app/scenes/ComeceGratis';
import Cadastro from './app/scenes/Cadastro';
import Perfil from './app/scenes/Perfil';
import Home from './app/scenes/home';
import Chat from './app/scenes/chat';
import EbooksScreen from './app/scenes/EbooksScreen';
import ExemplarScreen from './app/scenes/ExemplarScreen';
import EsquemaScreen from './app/scenes/EsquemaScreen';
import AulaEnemScreen from './app/scenes/AulaEnemScreen';
import AulaEnem2020Screen from './app/scenes/AulaEnem2020Screen';
import AulaMilitarScreen from './app/scenes/AulaMilitarScreen';
import AulaConcursoScreen from './app/scenes/AulaConcursoScreen';
import AulaUERJScreen from './app/scenes/AulaUERJScreen';
import FacaRedacaoScreen from './app/scenes/FacaRedacaoScreen';
import JogoScreen from './app/scenes/JogoScreen';
import MaratonaScreen from './app/scenes/MaratonaScreen';
import PodcastsScreen from './app/scenes/PodcastsScreen';
import PptsScreen from './app/scenes/PptsScreen';
import QuadroScreen from './app/scenes/QuadroScreen';
import TemasScreen from './app/scenes/TemasScreen';
import VideoScreen from './app/scenes/VideoScreen';
import VideoScreenDefault from './app/scenes/VideoScreenDefault';
import PodcastPlayerScreen from './app/scenes/PodcastPlayerScreen';
import EnviarRedacaoScreen from './app/scenes/EnviarRedacaoScreen';
import PDFViewScreen from './app/scenes/PDFViewScreen';
import ImageViewScreen from './app/scenes/ImageViewScreen';
import EstatisticaScreen from './app/scenes/EstatisticaScreen';
import NotificacoesScreen from './app/scenes/NotificacoesScreen';

//Scenes - Jogo
import IntroScreen from './app/scenes/jogo/IntroScreen';
import Dev1Screen from './app/scenes/jogo/Dev1Screen';
import Dev2Screen from './app/scenes/jogo/Dev2Screen';
import ConclusaoScreen from './app/scenes/jogo/ConclusaoScreen';
import RedacaoScreen from './app/scenes/jogo/RedacaoScreen';
import RedacaoCorrigidaScreen from './app/scenes/jogo/RedacaoCorrigidaScreen';


//https://github.com/oblador/react-native-vector-icons#icon-component
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// Menu
import AppDrawer from './app/scenes/drawer';

// Reducers
//import { createStore } from 'redux';
//import {ModalReducer} from './app/store/modules/Modal/ModalReducer';
import store from './app/component/store';
//const store = createStore(ModalReducer);

// Estilo
import Colors from './app/constants/Colors';

//Store
import {retrieveItem} from './app/store/store';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const slides = [
  {
    key: 1,
    title: 'Bem vindo (a)',
    text: 'Aprenda redação com método\nexclusivo e revolucionário',
    image: require('./app/assets/images/logo-hredacao.png'),
    backgroundColor: '#374B60',
  },
  {
    key: 2,
    title: 'Uma Biblioteca\nno seu bolso',
    text: 'Ebooks, Podcasts e vídeos\nà sua disposição',
    image: require('./app/assets/images/logo-hredacao.png'),
    backgroundColor: '#374B60',
  }
]; 

type Props = {};


export default class App extends Component<Props> {
  constructor(props) {
      super(props);

      this.state = {
          showRealApp: false,
          initial:false,
          new: false
      }

      //Remove this method to stop OneSignal Debugging 
      OneSignal.setLogLevel(6, 0);
      
      // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
      OneSignal.init("c52b9e72-62c6-41b7-b1ca-a6a6929b7c17", {kOSSettingsKeyAutoPrompt : false, kOSSettingsKeyInAppLaunchURL: false, kOSSettingsKeyInFocusDisplayOption:2});
      OneSignal.inFocusDisplaying(2); // Controls what should happen if a notification is received while the app is open. 2 means that the notification will go directly to the device's notification center.
      
      // The promptForPushNotifications function code will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step below)
      //OneSignal.promptForPushNotificationsWithUserResponse(myiOSPromptCallback);

      OneSignal.addEventListener('received', this.onReceived);
      OneSignal.addEventListener('opened', this.onOpened);
      OneSignal.addEventListener('ids', this.onIds);
  }
  componentDidMount(){
    var user = retrieveItem('horadaredacao.user');
    user.then(u => {
      if(u==null){
        this.setState({ showRealApp: false, initial:false });
      }else{
        this.setState({ showRealApp: true, initial:true });
      }
    });
    SplashScreen.hide();
  };

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    console.log('Device info: ', device);
  }

  _renderItem = ({ item }) => {
    return (
        <View 
          style={[
            styles.slide,
            {
              backgroundColor: item.backgroundColor,
            },
          ]}>
            <ImageBackground source={require('./app/assets/images/drawable-hdpi/imagem1.png')} style={styles.bgslider}>
                <Image source={require('./app/assets/images/logo-hredacao.png')} style={styles.logoImg}/>
                <Text style={styles.title}>{item.title}</Text>
                <Text style={styles.text}>{item.text}</Text>
                <TouchableOpacity style={styles.button} onPress={() => {this._goGratis()}}>
                    <Text style={styles.buttonText}>Iniciar</Text>
                </TouchableOpacity>
                <View style={styles.just}>
                    <Text style={styles.accountCheck}>Já tem uma conta?</Text>
                    <TouchableWithoutFeedback onPress={() => {this._onDone()}}><Text style={[styles.accountCheck, styles.login]}> Entrar</Text></TouchableWithoutFeedback>
                </View>
            </ImageBackground>
        </View>
    );
  }
  _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon
          name="arrow-right"
          color="rgba(255, 255, 255, .9)"
          size={30}
          style={{ backgroundColor: 'transparent' }}
        />
      </View>
    );
  };
  _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon
          name="check"
          color="rgba(255, 255, 255, .9)"
          size={30}
          style={{ backgroundColor: 'transparent' }}
        />
      </View>
    );
  };
  _goGratis = () => {
    this.setState({ showRealApp: true, new: true, initial: false });
  };
  _onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    this.setState({ showRealApp: true });
  };
  _onSkip = () => {
    this.setState({ showRealApp: true });
  };

  renderBackButton = () => {
      return (
          <TouchableOpacity
              onPress={() => {}}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Icon
                    name="arrow-left"
                    color="rgba(255, 255, 255, .9)"
                    size={30}
                    style={{ backgroundColor: 'transparent' }}
                  />
                  <Text>Back</Text>
              </View>
          </TouchableOpacity>
      );
  };


  render() {
    if (!this.state.showRealApp) {
      //Slider
      return <AppIntroSlider 
      renderItem={this._renderItem} 
      data={slides} 
      onDone={this._onDone}
      renderDoneButton={this._renderDoneButton}
      renderNextButton={this._renderNextButton}
      />;
    } else {
    return (
      <Provider store={store}>
          <Router key="root" navigationBarStyle={styles.navBar} titleStyle={styles.navTitle} >
              {/*<Drawer key="drawer" contentComponent={AppDrawer} hideNavBar tabs={true}*/}
              {/*tabBarPosition={'bottom'} tabBarComponent={FooterHome}*/}
              {/*initial={this.state.isAuthenticated}>*/}
              <Drawer key="drawer" contentComponent={AppDrawer} drawerIcon={<Icon name='menu' size={36} color={Colors.textInPrimary} />} hideNavBar >
                  
                  <Lightbox key="lightbox" hideNavBar>
                    <Stack
                        key="rootStack"
                        drawerLockMode={Platform.OS === 'ios' ? 'locked-closed' : 'unlocked'}
                    >
                          <Scene key="chat" component={Chat} title="Tire suas Dúvidas"/>
                          <Scene key="pdfView" component={PdfView} title="PDF"/>
                          <Scene key="videoView" component={VideoView} title="Video" />
                          <Scene key="VideoScreen" component={VideoScreen} title="Video" navTransparent={true}/>
                          <Scene key="VideoScreenDefault" component={VideoScreenDefault} title="Video" hideNavBar />
                          <Scene key="EnviarRedacaoScreen" component={EnviarRedacaoScreen} title="Enviar redação" />
                          <Scene key="PodcastPlayerScreen" component={PodcastPlayerScreen} navTransparent={true} />
                          <Scene key="EstatisticaScreen" component={EstatisticaScreen} title="Estatísticas" />
                          <Scene key="NotificacoesScreen" component={NotificacoesScreen} title="Notificações" />
                          <Scene key="PDFViewScreen" component={PDFViewScreen} title="" hideNavBar />
                          <Scene key="ImageViewScreen" component={ImageViewScreen} title="" hideNavBar />
                          <Scene key="audioPlayer" component={AudioPlayer} title="Audio" />
                          <Scene key="playerScreen" component={PlayerScreen} title="Audio" />
                          <Scene type='reset' key="home" initial={this.state.initial} component={Home} title="Home" hideNavBar />

                          <Scene key="Ebook" component={EbooksScreen} navTransparent={true} />
                          <Scene key="Exemplar" component={ExemplarScreen} navTransparent={true} />
                          <Scene key="Esquema" component={EsquemaScreen} navTransparent={true} />
                          <Scene key="AulaEnem" component={AulaEnemScreen} navTransparent={true} />
                          <Scene key="AulaEnem2020" component={AulaEnem2020Screen} navTransparent={true} />
                          <Scene key="AulaConcurso" component={AulaConcursoScreen} navTransparent={true} />
                          <Scene key="AulaMilitar" component={AulaMilitarScreen} navTransparent={true} />
                          <Scene key="AulaUERJ" component={AulaUERJScreen} navTransparent={true} />
                          <Scene key="FacaRedacao" component={FacaRedacaoScreen} navTransparent={true} />
                          <Scene key="Maratona" component={MaratonaScreen} navTransparent={true} />
                          <Scene key="Podcasts" component={PodcastsScreen}  navTransparent={true} />
                          <Scene key="Ppts" component={PptsScreen} navTransparent={true} />
                          <Scene key="Quadro" component={QuadroScreen} navTransparent={true} />
                          <Scene key="Temas" component={TemasScreen}  navTransparent={true} />
                          
                          <Scene key="Jogo" component={JogoScreen} navTransparent={true} />
                          <Scene key="IntroScreen" component={IntroScreen} title="Introdução" />
                          <Scene key="Dev1Screen" component={Dev1Screen} title="Desenvolvimento Parte 1" />
                          <Scene key="Dev2Screen" component={Dev2Screen} title="Desenvolvimento Parte 2" />
                          <Scene key="ConclusaoScreen" component={ConclusaoScreen} title="Conclusão" />
                          <Scene key="RedacaoScreen" component={RedacaoScreen} title="Sua Redação" />
                          <Scene key="RedacaoCorrigidaScreen" component={RedacaoCorrigidaScreen} title="Sua Redação" />
                      
                          <Scene key="login" type='reset' initial={(this.state.new == false ? !this.state.initial : this.state.initial)} component={Login} title="Login" hideNavBar />
                          
                            {/* <Scene key="ComeceGratis" initial={false} component={ComeceGratis} title="ComeceGratis" hideNavBar/> */}
                            <Scene key="cadastro" initial={this.state.new} component={Cadastro} title="Cadastro" hideNavBar />
                            <Scene key="perfil" initial={this.state.new} component={Perfil} title="Perfil" />
                          <Scene key="CompreModal" component={CompreModal} hideNavBar />
                    </Stack>
                  </Lightbox>
              </Drawer>
          </Router>
          <RootModal></RootModal>
      </Provider>
    );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  slide: {
    flex: 1,
  },
  logoImg: {
      alignSelf: 'center',
      width: 127,
      height: 127,
      marginTop: 80,
      marginBottom: 50
  },
  title: {
      fontFamily: "Quicksand-Bold",
      fontSize: 25,
      lineHeight: 25,
      letterSpacing: 0,
      color: "#ffffff",
      alignSelf: 'center',
      textAlign: 'center',
      marginBottom: 15
  },
  text: {
      fontFamily: "Quicksand-Regular",
      fontSize: 20,
      lineHeight: 20,
      letterSpacing: 0,
      color: "#ffffff",
      alignSelf: 'center',
      textAlign: 'center',
      marginBottom: 15,

  },
  bgslider: {
    flex: 1,
    resizeMode: "cover",
    width: "100%",
  },
  just: {
      flexDirection: 'row',
      alignSelf: 'center',
  },
  button: {
      width: 260,
      height: 41,
      backgroundColor: "#107ae8",
      alignSelf: 'center',
      marginVertical: 20,
      borderRadius: 20,
  },
  buttonText: {
      fontFamily: "Quicksand-Bold",
      fontSize: 16,
      textAlign: "center",
      lineHeight: 20,
      letterSpacing: 0,
      color: "#fefdfc",
      marginTop: 12,
      textTransform: 'uppercase'
  },
  accountCheck: {
      fontFamily: "Quicksand-Regular",
      fontSize: 14,
      lineHeight: 18,
      letterSpacing: 0,
      color: "#ffffff",
      alignSelf: 'center',
      marginBottom: 89
  },
  login: {
      fontWeight: 'bold'
  },
  navBar: {
      backgroundColor: "#0E5AA7",
      shadowColor: 'transparent',
      shadowOpacity: 0,
  },
  navTitle: {
    fontFamily: "Quicksand-Bold",
    fontSize: 20,
    color: "#ffffff",
  },
});
