
import Api from "../services/api";
import {storeItem} from '../store/store';

export default class Providers {
    
    static home(params) {
        //console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('home/home.json?usuid='+params.userid).then(result => {
                //console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }
    
    static jogo(params) {
        //console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('temas/lista_temas_2020.json?categoria='+params.categoria+'&usuid='+params.userid).then(result => {
                //console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }
    
    static jogo_tema(params) {
        //console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('temas/devolve_tema.json?id_tema='+params.id).then(result => {
                console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }
    
    static grava_redacao(params) {
        //console.log(params);
        return new Promise((resolve, reject) => {
            Api.post('redacoes/grava_redacao.json',params).then(result => {
                console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }

    static ppts(params) {
        //console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('ppts/lista_ppts_2020.json?categoria='+params.categoria+'&usuid='+params.userid).then(result => {
                //console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }

    static videos(params) {
        //console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('videos/lista_videos_2020.json?categoria='+params.categoria+'&usuid='+params.userid).then(result => {
                //console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }

    static podcasts(params) {
        //console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('audios/lista_audios_2020.json?usuid='+params.userid).then(result => {
                //console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }
    
    static maratona(params) {
        //console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('maratonas/lista_maratonas.json?usuid='+params.userid).then(result => {
                console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }
    
    static correcoes(params) {
        //console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('correcoes/lista_correcoes.json?usuid='+params.userid).then(result => {
                //console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }

    static setplayerid(params) {
        console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('usuarios/setplayerid.json?id='+params.userid+'&player_id='+params.player_id).then(result => {
                console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }
    
    static setstatistic(params) {
        console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('usuarios/setstatistic.json?id='+params.userid+'&tipo='+params.tipo).then(result => {
                console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }
    
    static media_usuario(params) {
        console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('usuarios/media_usuario.json?id='+params.userid).then(result => {
                console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }
    
    static getstatistics(params) {
        console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('usuarios/getstatistics.json?id='+params.userid).then(result => {
                console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }
    
    static countunread(params) {
        console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('usuarios/countunread.json?id='+params.userid).then(result => {
                console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }
    
    static notificacoes(params) {
        console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('usuarios/notificacoes.json?id='+params.userid).then(result => {
                console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }
    
    static setread(params) {
        console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('usuarios/setread.json?id='+params.userid).then(result => {
                console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }
    
    static check_creditos(params) {
        console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('correcoes/check_creditos.json?usuid='+params.userid).then(result => {
                console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }
    
    static conteudo_prata() {
        return new Promise((resolve, reject) => {
            Api.get('compras/conteudo_prata.json').then(result => {
                console.log(result);
                if(result != null){
                    resolve(result);
                }else{
                    reject("error");
                }
            }, error => {
                //console.log(error);
                reject(error);
            });
        });
    }
    
    static compra_creditos(params) {
        console.log(params);
        return new Promise((resolve, reject) => {
            Api.post('compras/compra_creditos.json',params).then(result => {
                console.log(result);
                if(result != null){
                    // storeItem('user',result);
                    resolve(true);
                }else{
                    reject("error");
                }
            })
            .catch(error => {
                console.log("Uepa")
                console.log(error.response.data);
                reject(error.response.data);
            });
        });
    }
    
    static compra_prata(params) {
        console.log(params);
        return new Promise((resolve, reject) => {
            Api.post('compras/compra_prata.json',params).then(result => {
                console.log(result);
                if(result != null){
                    // storeItem('user',result);
                    resolve(true);
                }else{
                    reject("error");
                }
            })
            .catch(error => {
                console.log("Uepa")
                console.log(error.response.data);
                reject(error.response.data);
            });
        });
    }
}