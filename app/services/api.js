import Axios from "axios";

import env from '../../env.json';

export default class Api {
    static BASE_URL = env.api_url;
    static TIMEOUT = env.max_timeout;

    static post(endpoint, data) {
        return new Promise((resolve, reject) => {
            const abort = Axios.CancelToken.source();
            const id = setTimeout(() => abort.cancel(), this.TIMEOUT * 1000);

            //console.log(this.BASE_URL + endpoint);

            Axios.post(this.BASE_URL + endpoint, data, {cancelToken: abort.token, timeout: 5000}).then(response => {
                clearTimeout(id);

                resolve(response.data);
            }, error => {
                reject(error);
            });
        });
    }

    static get(endpoint) {
        return new Promise((resolve, reject) => {
            const abort = Axios.CancelToken.source();
            const id = setTimeout(() => abort.cancel(), this.TIMEOUT * 1000);

            //console.log(this.BASE_URL + endpoint);

            Axios.get(this.BASE_URL + endpoint, {cancelToken: abort.token, timeout: 5000}).then(response => {
                clearTimeout(id);
                // console.log(response.data);
                resolve(response.data);
            }, error => {
                // console.log(error);
                reject(error);
            });
        });
    }
}