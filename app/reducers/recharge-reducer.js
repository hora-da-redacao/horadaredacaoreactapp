const INITIAL_STATE = {
    total_recharge: 0,
    cards_charge: []
};
 
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'update_total':
            return { ...state, total_recharge: action.payload };
        case 'update_card':
            state.cards_charge[action.id] = {
                ...state.cards_charge[action.id],
                ...action.payload
            }
            return {
                ...state
            }
        case 'add_card':
            return { 
                ...state,
                cards_charge: state.cards_charge.concat(action.payload)
            };
            // return { ...state, cards_charge: action.payload };
        default:
            return state;
    }
};