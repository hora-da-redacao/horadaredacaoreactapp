export const updateNotifications = (notifications) => {
    return {
        type: 'update_notifications',
        payload: notifications
    };
};

export const updateTotalValue = (total_recharge) => {
    return {
        type: 'update_total',
        payload: total_recharge
    };
};

export const addCardRecharge = (card_info) => {
    return {
        type: 'add_card',
        payload: card_info
    };
};

export const updateCardlValue = (id, card_info) => {
    return {
        type: 'update_card',
        id: id,
        payload: card_info
    };
};

export const restApp = (value) => {
    return {
    type: 'RESET_APP',
    newValue: value
    };
};
