const INITIAL_STATE = {
    notifications: []
};
 
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'update_notifications':
            return { ...state, notifications: action.payload };
        default:
            return state;
    }
};