import { combineReducers } from 'redux';
import notificationsReducer from './notifications-reducer';
import logoutReducer from './logout-reducer';
import rechargeReducer from './recharge-reducer';

export default combineReducers({
    notificationsReducer,
    rechargeReducer,
    logoutReducer,
});
