import AsyncStorage from "@react-native-community/async-storage";
import Api from "../services/api";
import {storeItem, removeItem, resetState} from '../store/store';
import Providers from '../providers/Providers';

export default class Auth {
    static USERID = "horadaredacao.user";
    static loggedUser = {};

    static login(params, player_id) {
        //console.log(params);
        return new Promise((resolve, reject) => {
            Api.get('usuarios/usuario_auth.json?email='+params.username+'&senha='+params.password).then(result => {
                console.log('LOGIN');
                console.log(result);
                if(result != null){
                    storeItem(this.USERID,result);
                    
                    let id = result._id.$oid;
                    let values = { player_id: player_id, userid: id }
                    Providers.setplayerid(values)
                    .then(response => {
                        console.log("[GetStats - Success]")
                        console.log(response);
                    })
                    .catch(error => {
                        console.log("GetStats - Error")
                        //this.setModalVisible(false);
                        console.log(error);
                    });

                    resolve(true);
                }else{
                    reject("error");
                }
            }, error => {
                console.log(error);
                reject(error);
            });
        });
    }
    
    static cadastrar(params) {
        console.log(params);
        return new Promise((resolve, reject) => {
            Api.post('usuarios/cadastrar_usuario.json',params).then(result => {
                console.log(result);
                if(result != null){
                    // storeItem('user',result);
                    resolve(true);
                }else{
                    reject("error");
                }
            })
            .catch(error => {
                console.log("Uepa")
                console.log(error.response.data);
                reject(error.response.data);
            });
        });
    }
    
    static atualizar(params) {
        console.log(params);
        return new Promise((resolve, reject) => {
            Api.post('usuarios/atualizar_usuario.json',params).then(result => {
                console.log(result);
                if(result != null){
                    AsyncStorage.clear().then(() => resolve(true));
                    resetState();
                    storeItem(this.USERID,result);
                    resolve(true);
                }else{
                    reject("error");
                }
            })
            .catch(error => {
                console.log("Uepa")
                console.log(error.response.data);
                reject(error.response.data);
            });
        });
    }
    
    static rememberPassword(email) {
        return new Promise((resolve, reject) => {
            Api.get('usuarios/retrieve_password.json?email='+email).then(result => {
                //console.log(result);
                resolve(result);
            });
        });
    }

    /**
     * Realiza o logout
     */
    static logout() {
        this.loggedUser = {};
        return new Promise(resolve => {
            //removeItem(this.USERID);
            //AsyncStorage.removeItem(this.USERID).then(() => {
            //    resolve(true);
            //});
            //AsyncStorage.getAllKeys()
            //.then(keys => AsyncStorage.multiRemove(keys))
            //.then(() => resolve(true));
            AsyncStorage.clear().then(() => resolve(true));
            resetState();
        });
    }

    async removeItemValue(key) {
        try {
            await AsyncStorage.removeItem(this.USERID);
            return true;
        }
        catch(exception) {
            return false;
        }
    }
    
}