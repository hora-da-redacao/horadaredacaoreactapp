import React, {Component} from 'react';

import {StyleSheet, Text, Dimensions, Image, TouchableOpacity, View, ScrollView, Alert} from 'react-native';

//https://github.com/zmxv/react-native-sound-demo/blob/master/main.js
import Sound from 'react-native-sound';
import {StackNavigator} from 'react-navigation';
import PlayerScreen from 'react-native-sound-playerview'


//https://github.com/oblador/react-native-vector-icons#icon-component
import Icon from 'react-native-vector-icons/FontAwesome';


var whoosh = null;

export default class SoundPlayer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            card:[],
            trackTitle: "",
            isAuthenticated: false,
            duration: "",
        };
    }

    componentDidMount() {
        this.setState({trackTitle: this.state.card.title});
        this.setState({ card: this.props.mensagem.card });
    }

    playSound(){
        // Load the sound file 'whoosh.mp3' from the app bundle
        // See notes below about preloading sounds within initialization code below.
        whoosh = new Sound(this.props.mensagem.card.buttons[0].postback, Sound.MAIN_BUNDLE, (error) => {
            if (error) {
            console.log('failed to load the sound', error);
            return;
            }
            // loaded successfully
            console.log('duration in seconds: ' + whoosh.getDuration() + 'number of channels: ' + whoosh.getNumberOfChannels());
            this.setState({ duration: whoosh.getDuration() });
            // Play the sound with an onEnd callback
            whoosh.play((success) => {
                if (success) {
                    console.log('successfully finished playing');
                } else {
                    console.log('playback failed due to audio decoding errors');
                }
            });
        });
    }

    stopSound(){
        // Stop the sound and rewind to the beginning
        whoosh.stop();
    }
    pauseSound(){
        // Pause the sound
        whoosh.pause();
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>{this.state.card.title}</Text>
                <Image
                style={[styles.mapView]}
                source={{uri: this.state.card.imageUri}}
                />
                <View style={styles.controles}>
                    <TouchableOpacity style={styles.playeractions} onPress={() => this.playSound()}>
                        <Icon name="play" size={30} color="#333" />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.playeractions} onPress={() => this.stopSound()}>
                        <Icon name="stop" size={30} color="#333" />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.playeractions} onPress={() => this.pauseSound()}>
                        <Icon name="pause" size={30} color="#333" />
                    </TouchableOpacity>
                    <Text style={styles.button}>{this.state.duration}</Text>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,
    },
    mapView: {
        width: 200,
        height: 150,
        borderRadius: 13,
        margin: 3,
    },
    view: {
        flex:1,
        width:Dimensions.get('window').width,
    },
    controles:{
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: 200,
        margin: 3,
    },
    playeractions:{
        margin: 5
    },
    button: {
      fontSize: 20,
      backgroundColor: 'rgba(220,220,220,1)',
      borderRadius: 4,
      borderWidth: 1,
      borderColor: 'rgba(80,80,80,0.5)',
      overflow: 'hidden',
      padding: 7,
    }
});