import React, {Component} from 'react';
import { View, TouchableOpacity, Text, StyleSheet} from "react-native";
import colors from '../constants/Colors';
import Icon from "react-native-vector-icons/MaterialIcons";
import { jogoStyles, accordionStyles } from '../styles/jogo';

export default class Accordian extends Component{

    constructor(props) {
        super(props);
        this.state = { 
          data: props.data,
          expanded : false,
        }
    }
    
    textColor(){
        let nota = this.props.nota
        if(nota === 300 || nota === 200 || nota === 300){
            return 'green';
        }else if(nota === 260 || nota === 160 || nota === 260){
            return 'rgb(210,210,0)';
        }else if(nota === 180 || nota === 80 || nota === 180){
            return 'red';
        }
    }
  
  render() {

    return (
       <View>
            <TouchableOpacity style={accordionStyles.row} onPress={()=>this.toggleExpand()}>
                <Text style={[accordionStyles.title, accordionStyles.font],{color: this.textColor()}}>{this.props.paragrafo}</Text>
                <Icon name={this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} color={colors.darkGray} />
            </TouchableOpacity>
            <View style={accordionStyles.parentHr}/>
            {
                this.state.expanded &&
                <View style={accordionStyles.child}>
                    <Text style={jogoStyles.textTipo}>{this.props.desccor}</Text> 
                    <Text style={jogoStyles.textParagrafo}>{this.props.correcao}</Text>    
                    <Text style={jogoStyles.textTipo}>a) Competência 1</Text>  
                    <Text style={[jogoStyles.textParagrafo, jogoStyles.borderBottom]}>{this.props.comp1.correcao}</Text>  

                    <Text style={jogoStyles.textTipo}>b) Competência 2</Text>
                    <Text style={[jogoStyles.textParagrafo, jogoStyles.borderBottom]}>{this.props.comp2.correcao}</Text>

                    <Text style={jogoStyles.textTipo}>c) Competência 3</Text>  
                    <Text style={[jogoStyles.textParagrafo, jogoStyles.borderBottom]}>{this.props.comp3.correcao}</Text>   

                    <Text style={jogoStyles.textTipo}>d) Competência 4</Text>
                    <Text style={[jogoStyles.textParagrafo, jogoStyles.borderBottom]}>{this.props.comp4.correcao}</Text>
                    {
                        this.props.comp5 != undefined && 
                        <View style={jogoStyles.textTipo}>
                            <Text>e) Competência 5</Text>
                            <Text style={[jogoStyles.textParagrafo, jogoStyles.borderBottom]}>{this.props.comp5.correcao}</Text> 
                        </View>
                    }
                </View>
            }
       </View>
    )
  }

  toggleExpand=()=>{
    this.setState({expanded : !this.state.expanded})
  }

}