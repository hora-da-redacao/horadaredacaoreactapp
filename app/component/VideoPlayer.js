import React from 'react';
import { StyleSheet, Dimensions, View, BackHandler, PixelRatio, Text, ScrollView, ImageBackground} from 'react-native';
import YouTube, { YouTubeStandaloneIOS, YouTubeStandaloneAndroid }  from 'react-native-youtube'

import ParallaxScroll from '@monterosa/react-native-parallax-scroll';

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Orientation from 'react-native-orientation';
import { PropTypes } from 'prop-types';
import { Actions } from 'react-native-router-flux';

import {retrieveItem} from '../store/store';
import Providers from '../providers/Providers';
import colors from '../constants/Colors';


export default class VideoScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isReady: false,
            status: null,
            quality: null,
            error: null,
            isPlaying: true,
            isLooping: true,
            duration: 0,
            currentTime: 0,
            fullscreen: true,
            isAuthenticated: false,
            playerWidth: Dimensions.get('window').width,
        };

        //this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        Orientation.lockToPortrait();
        //BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    
    handleBackButtonClick() {
        Actions.pop();
        return true;
    }

    DefinindoOrientacao() {
        //Orientation.lockToLandscape();
    }

    componentWillUnmount() {
        //BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        //Orientation.lockToPortrait();
    }

    componentDidMount() {
        //BackHandler.addEventListener('backPress', () => {
        //  Actions.Quadro({ type:'reset' });
        //  //Actions.reset("Quadro");
        //});
        var user = retrieveItem('horadaredacao.user');
        user.then(u => {
            if(u==null){
                Actions.login({ type: 'replace' });
            }else{
                console.log(u);
                let id = u._id.$oid;
                let values = { tipo: 'video', userid: id }
                Providers.setstatistic(values)
                .then(response => {
                    console.log("[GetStats - Success]")
                    console.log(response);
                })
                .catch(error => {
                    console.log("GetStats - Error")
                    //this.setModalVisible(false);
                    console.log(error);
                });
            }
        });
        //Orientation.lockToLandscape();
        Orientation.lockToPortrait();
    }

    render() {
        return (
            <View style={styles.container}>
                <YouTube
                    videoId={this.props.uid}   // The YouTube video ID
                    play={true}             // control playback of video with true/false
                    fullscreen={true}       // control whether the video should play in fullscreen or inline
                    loop={true}             // control whether the video should loop when ended
                    controls={2}
                    modestbranding={true}
                    resumePlayAndroid={true}
                    showFullscreenButton={false}
                    showinfo={false}
                    rel={true}
                    style={[
                        { height: 250, width: wp('90%') },
                        styles.player,
                        ]}
                    style={[
                        { 
                        height: PixelRatio.roundToNearestPixel(this.state.playerWidth / (16 / 9)) 
                        },
                        styles.player,
                    ]}
                    // You must have an API Key for the player to load in Android
                    apiKey="AIzaSyA9YHVqf6jPqSYvsIuZZBu7_uLR41MOHig"
                    onError={e => {
                        this.setState({ error: e.error });
                    }}
                    onReady={e => {
                        this.setState({ isReady: true });
                    }}
                    onChangeState={e => {
                        this.setState({ status: e.state });
                    }}
                    onChangeQuality={e => {
                        this.setState({ quality: e.quality });
                    }}
                    onChangeFullscreen={e => {
                        this.setState({ fullscreen: e.isFullscreen });
                        if(e.isFullscreen==false){
                            Orientation.lockToPortrait();
                            Actions.pop();
                        }
                        //else{
                        //    Orientation.lockToLandscape();
                        //}
                    }}
                    onProgress={e => {
                        this.setState({ currentTime: e.currentTime });
                    }}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
    },
    contentContainer: {
      paddingHorizontal: 10,
    },
    view: {
        //flex:1,
        //width:Dimensions.get('window').width,
    },
    player: {
      alignSelf: 'stretch',
      marginVertical: 10,
    },
    titulo: {
      fontSize: 25,
      color: colors.darkGray,
      fontFamily: "Quicksand-Bold",
      lineHeight: 38,
      textAlign: 'center',
    },
    descricao: {
      fontSize: 16,
      color: colors.lightGray,
      fontFamily: "Quicksand-Regular",
      lineHeight: 24,
      textAlign: 'center',
      marginVertical: 10
    },
});