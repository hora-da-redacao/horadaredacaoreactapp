import React from 'react';
import {retrieveItem} from '../store/store';
import { Text,View,StyleSheet,TouchableOpacity } from 'react-native';
import { Card, Button, Image } from 'react-native-elements';
import ImageLoad from 'react-native-image-placeholder'; 
import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import { Cadeado } from '../component/Cadeado';
import FastImage from 'react-native-fast-image';
import Providers from '../providers/Providers';
import YouTube, { YouTubeStandaloneIOS, YouTubeStandaloneAndroid } from 'react-native-youtube';


function setStatistic(){
  var user = retrieveItem('horadaredacao.user');
  user.then(u => {
      if(u==null){
          Actions.login({ type: 'replace' });
      }else{
          console.log(u);
          let id = u._id.$oid;
          let values = { tipo: 'video', userid: id }
          Providers.setstatistic(values)
          .then(response => {
              console.log("[GetStats - Success]")
              console.log(response);
          })
          .catch(error => {
              console.log("GetStats - Error")
              //this.setModalVisible(false);
              console.log(error);
          });
      }
  });
}

export function VideoThumb(props) {
    if (props.uid == undefined) {
        return <View style={[styles.item, styles.itemEmpty]} />;  
    } 
    let video_uri = 'https://img.youtube.com/vi/'+props.uid+'/mqdefault.jpg';
    console.log(props);
    return (
      <Card containerStyle={styles.card}>
        <FastImage
          style={{
              flex: 1,
              alignSelf: 'stretch',
              width: undefined,
              height: 150,
          }}
          source={{ uri: video_uri, priority: FastImage.priority.high}}
          resizeMode={FastImage.resizeMode.cover}
          onError={(e) => console.log(e)}
        />

        {!Boolean(props.liberado) && (
          <Cadeado right={7} top={5}></Cadeado>
        )}
        <Text style={styles.text}>
          {props.title}
        </Text>
        {/* <Button
          onPress={() => (Boolean(props.liberado) ? Actions.VideoScreen({ uid: props.uid, title: props.title }) : Actions.CompreModal())}
          buttonStyle={styles.btn}
          titleStyle={styles.textBtn}
          title='Assistir' /> */}


        {Platform.OS === 'ios' && YouTubeStandaloneIOS && (
          <View style={styles.buttonGroup}>
            <Button
              title="Assistir"
              buttonStyle={styles.btn}
              titleStyle={styles.textBtn}
              onPress={() => {
                (Boolean(props.liberado) ?
                YouTubeStandaloneIOS.playVideo(props.uid)
                  .then(message => {
                    console.log(message);
                    setStatistic();
                  })
                  .catch(errorMessage => {
                    console.log({ error: errorMessage });
                  })
                  : Actions.CompreModal())
              }}
            />
          </View>
        )}

        {Platform.OS === 'android' && YouTubeStandaloneAndroid && (
          <View style={styles.buttonGroup}>
            <Button
              buttonStyle={styles.btn}
              titleStyle={styles.textBtn}
              title="Assistir"
              onPress={() => {
                (Boolean(props.liberado) ?
                YouTubeStandaloneAndroid.playVideo({
                  apiKey: "AIzaSyA9YHVqf6jPqSYvsIuZZBu7_uLR41MOHig",
                  videoId: props.uid,
                  autoplay: true,
                  controls: 2,
                  lightboxMode: false,
                })
                .then(() => {
                  console.log('Android Standalone Player Finished');
                  setStatistic();
                })
                .catch(errorMessage => {
                  console.log({ error: errorMessage });
                }) : Actions.CompreModal())
              }}
            />
          </View>
        )}


      </Card>
    );
}

const styles = StyleSheet.create({
    item: {
      alignItems: "center",
      flexGrow: 1,
      margin: 4,
      height: 150
    },
    text: {
      fontFamily: "Quicksand-Regular",
      color: "#333333",
      fontSize: 18,
      marginVertical: 10,
      lineHeight: 22
    },
    itemEmpty: {
      display: 'none'
    },
    card:{
      borderRadius: 8,
      borderWidth: 0,
      marginBottom: 10,
    },
    btn: {
      borderRadius: 4, 
      marginTop: 4, 
      backgroundColor: Colors.primary,
      fontFamily: "Quicksand-Regular",
    },
    textBtn:{
      fontFamily: "Quicksand-Bold",
      fontSize: 20,
    }
  });
  
