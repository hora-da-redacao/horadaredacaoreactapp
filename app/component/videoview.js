import React from 'react';
import { StyleSheet, Dimensions, View, Text} from 'react-native';
import YouTube from 'react-native-youtube'
import { PropTypes } from 'prop-types';

export default class VideoView extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            card:[],
            isAuthenticated: false,
        };
    }

    componentWillMount() {
        console.log(this.props.mensagem);
        console.log(this.props.mensagem.card.buttons[0].postback);
        this.setState({ card: this.props.mensagem.card });
    }

    removeYutubeId(url){
        let regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        let match = url.match(regExp);
        if (match && match[2].length == 11) {
            return match[2];
        } else {
            //error
        }
    }

    render() {
        

        return (
            <YouTube
            videoId={this.removeYutubeId(this.state.card.buttons[0].postback)}   // The YouTube video ID
            play={false}             // control playback of video with true/false
            fullscreen={false}       // control whether the video should play in fullscreen or inline
            loop={false}             // control whether the video should loop when ended
            controls={1}
            showFullscreenButton={true}
            modestbranding={false}
            onReady={e => this.setState({ isReady: true })}
            onChangeState={e => this.setState({ status: e.state })}
            onChangeQuality={e => this.setState({ quality: e.quality })}
            onError={e => this.setState({ error: e.error })}
            style={styles.container}
            // You must have an API Key for the player to load in Android
            apiKey="AIzaSyA9YHVqf6jPqSYvsIuZZBu7_uLR41MOHig"
            />
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 25,
        width: 250,
        height: 140,
    },
    view: {
        flex:1,
        width:Dimensions.get('window').width,
    }
});