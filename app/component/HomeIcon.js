import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Colors from '../constants/Colors';

export default function HomeIcon(props) {
  return (
    <Icon
      name={props.iconname}
      size={36}
      style={{ marginBottom: -3, marginRight: 10, marginLeft: 5, color: '#FFFFFF' }}
    />
  );
}