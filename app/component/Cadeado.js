import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export function Cadeado(props) {
  return (
    <View style={{ 
      width: 35, 
      height: 35, 
      borderRadius: 30, 
      backgroundColor: "rgba(180,180,180,0.3)", 
      position: "absolute", 
      right: props.right, 
      top: props.top, 
      alignItems: "center", 
      justifyContent: "center", 
      shadowOffset:{  width: 10,  height: 10,  },
      shadowColor: 'black',
      shadowOpacity: 1.0,
    }}>
        <Icon name='lock' size={20} color={"#FFF"} />
    </View>
  );
}


const styles = StyleSheet.create({
  cadeado: { 
    width: 35, 
    height: 35, 
    borderRadius: 30, 
    backgroundColor: "rgba(255,255,255,0.3)", 
    position: "absolute", 
    alignItems: "center", 
    justifyContent: "center",
    shadowColor: "rgb(0,  0,  0)",
    shadowOpacity: 0.3,
    shadowOffset: {
      width: 3,
      height: 3
    }
  }
});