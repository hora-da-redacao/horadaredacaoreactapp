import React from 'react';
import { Text,View,StyleSheet,TouchableOpacity } from 'react-native';
import { Card, Button } from 'react-native-elements'
import ImageLoad from 'react-native-image-placeholder'; 
import { Actions } from 'react-native-router-flux';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Colors from '../constants/Colors';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FastImage from 'react-native-fast-image';

export function MaratonaThumb(props) {
    console.log(props);
    if (props.maratona.imagem == undefined) {
        return <View style={[styles.item, styles.itemEmpty]} />;  
    } 
    return (
        <Card containerStyle={styles.card}>
            <FastImage
                style={{
                    flex: 1,
                    alignSelf: 'stretch',
                    width: undefined,
                    height: 200,
                }}
                source={{ uri: props.maratona.imagem, priority: FastImage.priority.high}}
                resizeMode={FastImage.resizeMode.cover}
                onError={(e) => console.log(e)}
            />
            <Text style={styles.text}>
            {props.maratona.titulo}
            </Text>
            <View style={styles.buttomBox}>
                <Button
                icon={<Icon name='book' size={20} style={styles.buttonStyle} />}
                onPress={() => Actions.PDFViewScreen({ pdf: props.maratona.arquivo })}
                buttonStyle={{minWidth: 110, justifyContent: 'flex-start' ,backgroundColor: Colors.primary}}
                titleStyle={styles.textBtn}
                title='Ver Ppt' />
                {props.maratona.audio != "" && (
                <Button
                icon={<Icon name='podcast' size={20} style={styles.buttonStyle} />}
                onPress={() => Actions.PodcastPlayerScreen({ audio: {imagem: props.maratona.imagem, arquivo: props.maratona.audio, titulo: props.maratona.titulo}})}
                buttonStyle={{justifyContent: 'flex-start' ,backgroundColor: Colors.primary}}
                titleStyle={styles.textBtn}
                title='Ouvir Podcast' />
                )}
            </View>
        </Card>
    );
}

const styles = StyleSheet.create({
    item: {
        alignItems: "center",
        flexGrow: 1,
        margin: 4,
        height: 200
    },
    text: {
        fontFamily: "Quicksand-Regular",
        color: "#333333",
        fontSize: 18,
        marginVertical: 10,
        lineHeight: 22
    },
    itemEmpty: {
        display: 'none'
    },
    buttomBox:{
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    buttonStyle:{
        marginBottom: -3, marginRight: 7, marginLeft: 4, color: '#FFFFFF'
    },
    card:{
        borderRadius: 8,
        borderWidth: 0,
      marginBottom: 10,
    },
    textBtn:{
      fontFamily: "Quicksand-Bold",
      fontSize: 20,
    },
  });
  
