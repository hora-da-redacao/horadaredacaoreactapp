import React, { Component } from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  SafeAreaView,
  View,
  FlatList
} from 'react-native';

import { CorrecaoThumb } from './CorrecaoThumb';

export function CorrecaoGrid(props) {
    return (
        <View style={styles.container}>
            <SafeAreaView
                contentContainerStyle={styles.contentContainer}>
                <View>
                <FlatList
                data={createRows(props.data, props.columns)}
                keyExtractor={(item, index) => index.toString()}
                numColumns={props.columns} // Número de colunas
                removeClippedSubviews={true}
                renderItem={({ item }) => {
                    return (
                        <CorrecaoThumb data_envio={item.data_envio} pdf={item.arquivo_correcao} imagem={item.arquivo_redacao} status={item.status} />
                    );
                }}
                />
                </View>
            </SafeAreaView>
        </View>
    );
}

function createRows(data, columns) {
    const rows = Math.floor(data.length / columns); // [A]
    let lastRowElements = data.length - rows * columns; // [B]
    while (lastRowElements !== columns) { // [C]
    data.push({ // [D]
        _id: {$oid: `empty-${lastRowElements}`},
        name: `empty-${lastRowElements}`,
        empty: true
    });
    lastRowElements += 1; // [E]
    }
    return data; // [F]
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 60
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  item: {
    alignItems: "center",
    flexGrow: 1,
    margin: 4,
    height: 150
  },
  text: {
    color: "#333333",
    fontSize: 18
  },
  itemEmpty: {
    display: 'none'
  },
});
