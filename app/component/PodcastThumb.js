import React from 'react';
import { Text,View,StyleSheet,TouchableOpacity } from 'react-native';
import { Card, Button } from 'react-native-elements'
import ImageLoad from 'react-native-image-placeholder'; 
import { Actions } from 'react-native-router-flux';
import Colors from '../constants/Colors';
import { Cadeado } from '../component/Cadeado';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FastImage from 'react-native-fast-image';

export function PodcastThumb(props) {
    if (props.audio.imagem == undefined) {
        console.log(props);
        return <View style={[styles.item, styles.itemEmpty]} />;  
    } 
    return (
      <Card containerStyle={styles.card}>
        <FastImage
                style={{
                    flex: 1,
                    alignSelf: 'stretch',
                    width: undefined,
                    height: 150,
                }}
                source={{ uri: props.audio.imagem, priority: FastImage.priority.high}}
                resizeMode={FastImage.resizeMode.cover}
                onError={(e) => console.log(e)}
        />
        {!Boolean(props.audio.liberado) && (
          <Cadeado right={7} top={5}></Cadeado>
        )}
        <Text style={styles.text}>
          {props.audio.titulo}
        </Text>
        <Button
          icon={<Icon name='podcast' size={20} style={{ marginBottom: -3, marginRight: 10, marginLeft: 5, color: '#FFFFFF' }} />}
          onPress={() => (Boolean(props.audio.liberado) ?  Actions.PodcastPlayerScreen({ audio: props.audio}) : Actions.CompreModal())}
          buttonStyle={styles.btn}
          titleStyle={styles.textBtn}
          title='Ouvir Podcast' />
      </Card>
    );
}

const styles = StyleSheet.create({
    item: {
      alignItems: "center",
      flexGrow: 1,
      margin: 4,
      height: 150
    },
    text: {
      fontFamily: "Quicksand-Regular",
      color: "#333333",
      fontSize: 18,
      marginVertical: 5
    },
    itemEmpty: {
      display: 'none'
    },
    card:{
      borderRadius: 8,
      borderWidth: 0,
      marginBottom: 10,
    },
    btn: {
      borderRadius: 4, 
      marginTop: 4, 
      backgroundColor: Colors.primary,
      fontFamily: "Quicksand-Regular",
    },
    textBtn:{
      fontFamily: "Quicksand-Bold",
      fontSize: 20,
    },
  });
  
