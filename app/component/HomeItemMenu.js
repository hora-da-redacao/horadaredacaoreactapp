import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';

import { Actions as NavigationActions } from 'react-native-router-flux';

import Colors from '../constants/Colors';
import HomeIcon from './HomeIcon';

class HomeItemMenu extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
    return (
        <View>
            <TouchableOpacity onPress={NavigationActions[this.props.route]} style={styles.menuHome}>
                <HomeIcon iconname={this.props.iconName}/>
                <Text style={styles.menuHomeText}>
                {this.props.text}
                </Text>
            </TouchableOpacity>
        </View>
        );
    }
}

export default withNavigation(HomeItemMenu);

const styles = StyleSheet.create({
  menuHome: {
    display: 'flex',
    paddingVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
    width: 320,
    backgroundColor: Colors.primary,
    padding: 5,
    marginBottom: 10,
    borderRadius: 2
  },
  menuHomeText: {
    fontSize: 22,
    color: '#FFFFFF',
  },
});
