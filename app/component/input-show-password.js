import React from 'react';
import InputShowPasswordComponent from './input-show-password-component';

export default function inputShowPasswordTemplate(locals) {
    if (locals.hidden) {
        return null;
    }

    return (
        <InputShowPasswordComponent ref="input" locals={locals} />
    );
}
