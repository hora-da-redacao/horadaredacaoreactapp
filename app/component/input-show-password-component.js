/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    StyleSheet, Text, TextInput, View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class InputShowPasswordComponent extends Component {

    constructor() {
        super();

        this.state = {
            passwordHidden: true,
        };
    }

    togglePassword() {
        this.setState({ passwordHidden: !this.state.passwordHidden });
    }

    render() {
        const locals = this.props.locals;

        const stylesheet = locals.stylesheet;
        let formGroupStyle = stylesheet.formGroup.normal;
        let controlLabelStyle = stylesheet.controlLabel.normal;
        let textboxStyle = stylesheet.controlIcon.container.normal;
        let textboxInnerStyle = stylesheet.controlIcon.input.normal;
        let buttonStyle = stylesheet.controlIcon.button.normal;
        let helpBlockStyle = stylesheet.helpBlock.normal;
        const errorBlockStyle = stylesheet.errorBlock;

        if (locals.hasError) {
            formGroupStyle = stylesheet.formGroup.error;
            controlLabelStyle = stylesheet.controlLabel.error;
            textboxStyle = stylesheet.controlIcon.container.error;
            textboxInnerStyle = stylesheet.controlIcon.input.error;
            buttonStyle = stylesheet.controlIcon.button.error;
            helpBlockStyle = stylesheet.helpBlock.error;
        }

        if (locals.editable === false) {
            textboxStyle = stylesheet.textbox.notEditable;
        }

        const label = locals.label ? <Text style={controlLabelStyle}>{locals.label}</Text> : null;
        const help = locals.help ? <Text style={helpBlockStyle}>{locals.help}</Text> : null;
        const error = locals.hasError && locals.error ?
            (<Text
                accessibilityLiveRegion="polite"
                style={errorBlockStyle}
            >{locals.error}</Text>) : null;

        let buttonIcon = 'eye-slash';
        let buttonHideIcon = 'eye';

        if (locals.options) {
            if (locals.options.buttonIcon) {
                buttonIcon = locals.options.buttonIcon;
            }

            if (locals.options.buttonHideIcon) {
                buttonHideIcon = locals.options.buttonHideIcon;
            }
        }

        return (
            <View style={formGroupStyle}>
                {locals.label !== '' && label}

                <View style={[textboxStyle, styles.container]}>
                    <TextInput
                        accessibilityLabel={locals.label}
                        ref="input"
                        autoCapitalize={locals.autoCapitalize}
                        autoCorrect={locals.autoCorrect}
                        autoFocus={locals.autoFocus}
                        blurOnSubmit={locals.blurOnSubmit}
                        editable={locals.editable}
                        keyboardType={locals.keyboardType}
                        maxLength={locals.maxLength}
                        multiline={locals.multiline}
                        onBlur={locals.onBlur}
                        onEndEditing={locals.onEndEditing}
                        onFocus={locals.onFocus}
                        onLayout={locals.onLayout}
                        onSelectionChange={locals.onSelectionChange}
                        onSubmitEditing={locals.onSubmitEditing}
                        placeholderTextColor={locals.placeholderTextColor}
                        secureTextEntry={this.state.passwordHidden}
                        selectTextOnFocus={locals.selectTextOnFocus}
                        selectionColor={locals.selectionColor}
                        numberOfLines={locals.numberOfLines}
                        underlineColorAndroid={locals.underlineColorAndroid}
                        clearButtonMode={locals.clearButtonMode}
                        clearTextOnFocus={locals.clearTextOnFocus}
                        enablesReturnKeyAutomatically={locals.enablesReturnKeyAutomatically}
                        keyboardAppearance={locals.keyboardAppearance}
                        onKeyPress={locals.onKeyPress}
                        returnKeyType={locals.returnKeyType}
                        selectionState={locals.selectionState}
                        onChangeText={(value) => locals.onChange(value)}
                        onChange={locals.onChangeNative}
                        placeholder={locals.placeholder}
                        style={[textboxInnerStyle, styles.textbox]}
                        value={locals.value}
                    />

                    <Icon
                        name={this.state.passwordHidden ? buttonIcon : buttonHideIcon}
                        style={buttonStyle}
                        onPress={() => this.togglePassword()}
                    />
                </View>

                {help}
                {error}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    textbox: {
        flex: 1,
        margin: 0,
        marginRight: 10,
        padding: 0,
    }
});
