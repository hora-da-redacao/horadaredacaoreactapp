import React from 'react';
import InputCellphoneMaskComponent from './input-cellphone-mask-component';

export default function inputCell(locals) {
    if (locals.hidden) {
        return null;
    }

    return (
        <InputCellphoneMaskComponent ref="input" locals={locals} />
    );
}
