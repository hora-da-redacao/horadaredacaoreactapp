import React from 'react';
import { Text } from 'react-native';

export function Cadeado(props) {
  return (
    <Text {...props} style={[props.style, { fontFamily: 'space-mono' }]} />
  );
}
