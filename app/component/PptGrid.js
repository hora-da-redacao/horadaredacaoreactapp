import React, { Component } from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  SafeAreaView,
  View,
  FlatList
} from 'react-native';

import { PptThumb } from './PptThumb';


const columns = 1;

export function Pptgrid(props) {
    return (
        <View style={styles.container}>
                <FlatList
                data={createRows(props.data, props.columns)}
                keyExtractor={item => item.pdf_updated_at}
                numColumns={props.columns} // Número de colunas
                renderItem={({ item }) => {
                    return (
                        <PptThumb imagem={item.imagem} pdf={item.pdf} titulo={item.titulo} liberado={item.liberado} />
                    );
                }}
                />
        </View>
    );
}

function createRows(data, columns) {
    const rows = Math.floor(data.length / columns); // [A]
    let lastRowElements = data.length - rows * columns; // [B]
    while (lastRowElements !== columns) { // [C]
    data.push({ // [D]
        _id: {$oid: `empty-${lastRowElements}`},
        name: `empty-${lastRowElements}`,
        empty: true
    });
    lastRowElements += 1; // [E]
    }
    return data; // [F]
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  item: {
    alignItems: "center",
    flexGrow: 1,
    margin: 4,
    height: 150
  },
  text: {
    color: "#333333",
    fontSize: 18
  },
  itemEmpty: {
    display: 'none'
  },
});
