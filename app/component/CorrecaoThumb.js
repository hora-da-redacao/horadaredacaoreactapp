import React from 'react';
import { Text,View,StyleSheet,TouchableOpacity } from 'react-native';
import ImageLoad from 'react-native-image-placeholder'; 
import { Card, Button, Image, colors } from 'react-native-elements'
import { Actions } from 'react-native-router-flux';
import 'moment';
import 'moment/locale/pt-br';
import moment from 'moment-timezone';
import Colors from '../constants/Colors';
import FastImage from 'react-native-fast-image';


function formatDate(data){
  moment().locale('pt-br');
  return moment(data).format('DD-MM-YYYY')
}

function showStatus(status){
  if(status==0){
    return "Aguardando Correção";
  }
  return "Redação Corrigida";
}

export function CorrecaoThumb(props) {
  //console.log(props);
  if (props.data_envio == undefined) {
    return <View style={[styles.item, styles.itemEmpty]} />;  
  } 
  return (
    <Card containerStyle={styles.card}>
      <Text style={styles.text}>
        {formatDate(props.data_envio)}
      </Text>
      <FastImage
          style={{
              flex: 1,
              alignSelf: 'stretch',
              width: undefined,
              height: 150,
          }}
          source={{ uri: props.imagem, priority: FastImage.priority.high}}
          resizeMode={FastImage.resizeMode.cover}
          onError={(e) => console.log(e)}
      />
      <Text style={styles.textStatus}>
        {showStatus(props.status)}
      </Text>
      {props.status==1
        ? <Button
        onPress={() => Actions.PDFViewScreen({ pdf: props.pdf })}
          buttonStyle={{borderRadius: 4, marginTop: 4, backgroundColor: Colors.primary}}
          titleStyle={styles.textBtn}
          title='Ver Correção' />
        : <Button
        onPress={() => Actions.ImageViewScreen({ image: props.imagem })}
          buttonStyle={{borderRadius: 4, marginTop: 4, backgroundColor: Colors.primary}}
          titleStyle={styles.textBtn}
          title='Ver Redação' />
      }
    </Card>
  );
}

const styles = StyleSheet.create({
  item: {
    alignItems: "center",
    flexGrow: 1,
    margin: 4,
    height: 150
  },
  text: {
    color: colors.grey3,
    fontSize: 12,
    marginBottom: 5,
    marginTop: 5,
    alignSelf: 'flex-end',
    fontFamily: "Quicksand-Regular",
  },
  textStatus: {
    color: colors.grey3,
    fontSize: 14,
    marginBottom: 10,
    marginTop: 10,
    alignSelf: 'flex-start',
  },
  itemEmpty: {
    display: 'none'
  },
  card:{
      borderRadius: 8,
      borderWidth: 0
  },
  textBtn:{
    fontFamily: "Quicksand-Bold",
    fontSize: 20,
  },
});