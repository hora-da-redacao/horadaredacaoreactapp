import React from 'react';
import { Text,View,StyleSheet,TouchableOpacity, ImageBackground, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Cadeado } from '../component/Cadeado';
import FastImage from 'react-native-fast-image';

export function PptThumb(props) {
  console.log(props);
  if (props.imagem == undefined) {
    return <View style={[styles.item, styles.itemEmpty]} />;  
  } 
  //console.log(props.imagem);
  return (
      <TouchableOpacity  style={styles.item} onPress={() => (Boolean(props.liberado) ?  Actions.PDFViewScreen({ pdf: props.pdf }) : Actions.CompreModal())}>
        <View
            style={{
              width: "100%",
              height: 180,
              backgroundColor:'##FFF',
              top:0,
              alignItems: "flex-end",
              justifyContent: 'flex-end'
            }}>

              <FastImage
                style={{
                    flex: 1,
                    alignSelf: 'stretch',
                    width: undefined,
                    height: 150,
                }}
                source={{ uri: props.imagem, priority: FastImage.priority.high}}
                resizeMode={FastImage.resizeMode.cover}
                onError={(e) => console.log(e)}
              />
              <Image
                  style={styles.image}
                  resizeMode={'stretch'}
                  source={require('../assets/images/bg-branco-transparent.png')}
              />
        </View>
        <Text style={styles.text}>{props.titulo}</Text>
        {!Boolean(props.liberado) && (
          <Cadeado right={7} top={5}></Cadeado>
        )}
      </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
    item: {
      alignItems: "center",
      flexGrow: 1,
      margin: 7,
      borderWidth: 1,
      borderRadius: 8,
      borderColor: "#9B9B9B",
      backgroundColor: '#FFF'
    },
    text: {
      color: "#9B9B9B",
      fontSize: 16,
      lineHeight: 20,
      fontFamily: "Quicksand-Regular",
      textAlign: 'left',
      alignSelf: 'flex-start',
      marginHorizontal: 10,
      paddingBottom: 10
    },
    image: {
      flex: 1,
      alignSelf: 'flex-end',
      justifyContent: 'flex-end',
      width: '100%',
      height: 60,
      position: 'absolute',
      bottom: -2,
    }
  });
  
