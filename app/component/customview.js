import PropTypes from 'prop-types';
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  ViewPropTypes,
  Text,
  View,
  Image,
  Button
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import YouTube, { YouTubeStandaloneIOS, YouTubeStandaloneAndroid } from 'react-native-youtube';

//components
import VideoView from './videoview';
import SoundPlayer from './soundplayer';
import PlayerScreen from './playerscreen';

export default class CustomView extends React.Component {

    openObject(mensagem){
      console.log(mensagem);
      if(mensagem.cardType == "video"){
        Actions.videoView({ object: mensagem });
      }else if(mensagem.cardType == "ppt"){
        Actions.pdfView({ object: mensagem });
      }else if(mensagem.cardType == "audio"){
        Actions.audioPlayer({ object: mensagem });
      }
    }

    playVideo(uid){
      if(Platform.OS === 'android'){
        YouTubeStandaloneAndroid.playVideo({
          apiKey: "AIzaSyA9YHVqf6jPqSYvsIuZZBu7_uLR41MOHig",
          videoId: uid,
          autoplay: true,
          controls: 2,
          lightboxMode: false,
        })
        .then(() => {
          console.log('Android Standalone Player Finished');
        })
        .catch(errorMessage => {
          this.setState({ error: errorMessage });
        })
      }else{
        YouTubeStandaloneIOS.playVideo(uid)
        .then(message => {
          console.log(message);
        })
        .catch(errorMessage => {
          this.setState({ error: errorMessage });
        })
      }
    }

    removeYutubeId(url){
        let regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        let match = url.match(regExp);
        if (match && match[2].length == 11) {
            return match[2];
        } else {
            //error
        }
    }

    renderComponent(mensagem){
      if(mensagem.cardType == "ppt"){
          return (
              <TouchableOpacity 
                style={[styles.container, this.props.containerStyle]} 
                onPress={() => this.openObject(mensagem)}>
                  <Image
                  style={[styles.mapView]}
                  source={{uri: mensagem.card.imageUri}}
                  />
                  <Text style={[styles.title]}>{mensagem.card.title}</Text>
                  {this.subtitle(mensagem.card.subtitle)}
              </TouchableOpacity>
          );
      }else if(mensagem.cardType == "video"){
        console.log(mensagem);
        let video_id = this.removeYutubeId(mensagem.card.buttons[0].postback)
        let video_uri = 'https://img.youtube.com/vi/'+video_id+'/mqdefault.jpg';
        console.log(this.props);
        return (
          <View style={[styles.container, this.props.containerStyle]}>
            <TouchableOpacity style={{justifyContent: 'center'}} onPress={() => this.playVideo(video_id) }>
            {/* <TouchableOpacity 
                style={{justifyContent: 'center'}}
                onPress={() => Actions.VideoScreen({ uid:  video_id, title: mensagem.card.title })}> */}
                  <FastImage
                    style={{
                        width: 250,
                        height: 150,
                        borderTopLeftRadius: 12,
                        borderTopRightRadius: 12,
                    }}
                    source={{ uri: video_uri, priority: FastImage.priority.high}}
                    resizeMode={FastImage.resizeMode.cover}
                    onError={(e) => console.log(e)}
                  />
                  <Icon name="play" size={50} style={[styles.icon]} />
              </TouchableOpacity>
              <Text style={[styles.title]}>{mensagem.card.title}</Text>
              {this.subtitle(mensagem.card.subtitle)}
          </View>
        );
      }else if(mensagem.cardType == "audio"){
        return (
          <View >
              <PlayerScreen mensagem={mensagem}></PlayerScreen>
          </View>
        );
      }
    }

    subtitle(text) {
        console.log(text);
        if (text != "" && text != undefined) {
          return <Text style={[styles.subtitle]}>{text}</Text>;
        }
    }

    render() {
        //console.log(this.props.currentMessage);
        if (this.props.currentMessage.card != undefined) {
          return this.renderComponent(this.props.currentMessage);
        }
        return null;
    }
}

const styles = StyleSheet.create({
  container: {
  },
  mapView: {
    width: 200,
    height: 150,
    borderRadius: 13,
    margin: 3,
  },
  title: {
    margin: 10,
    fontWeight: "bold",
    fontSize: 16
  },
  subtitle: {
    margin: 10,
    fontSize: 14
  },
  icon: {
    position: 'absolute',
    alignSelf: 'center',
    color: "#FFFFFF",
  }
});

CustomView.defaultProps = {
  currentMessage: {},
  containerStyle: {},
  mapViewStyle: {},
};

CustomView.propTypes = {
  currentMessage: PropTypes.object,
  containerStyle: ViewPropTypes.style,
  mapViewStyle: ViewPropTypes.style,
};