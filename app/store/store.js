import AsyncStorage from '@react-native-community/async-storage';
import {combineReducers} from 'redux';
import {configureStore} from '@reduxjs/toolkit';
import {ModalReducer} from './modules/Modal/ModalReducer';

export default configureStore({
  reducer: combineReducers({
    modal: ModalReducer,
  }),
});

export const storeItem = (key, item) => {
    try {
        //we want to wait for the Promise returned by AsyncStorage.setItem()
        //to be resolved to the actual value before returning the value
        var jsonOfItem = AsyncStorage.setItem(key, JSON.stringify(item));
        return jsonOfItem;
    } catch (error) {
        console.log(error.message);
    }
};

export const retrieveItem = async (key) => {
    try {
        const retrievedItem =  await AsyncStorage.getItem(key);
        const item = JSON.parse(retrievedItem);
        return item;
    } catch (error) {
        console.log(error.message);
    }
    return
}

export const resetState = async (state = [], action) => {
    try {
        state = null
        return state;
    } catch (error) {
        console.log(error.message);
    }
    return
}