const showModal = () => {
  return dispatch => {
    dispatch({
      type: 'MODAL__SET_HIDEMODAL',
      payload: true,
    });
  };
};

const hideModal = () => {
  console.log("teste");
  return dispatch => {
    dispatch({
      type: 'MODAL__SET_HIDEMODAL',
      payload: false,
    });
  };
};

export const ModalActions = {
  showModal,
  hideModal,
};
