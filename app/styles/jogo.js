import { StyleSheet } from 'react-native';
import colors from '../constants/Colors';

export const jogoStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    contentContainer: {
        padding: 15
    },
    headerContainer: {
        flexDirection: 'column',
        backgroundColor: '#F5FCFF',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 100
    },
    temaJogo: {
        color: 'black',
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 10
    },
    textTipo: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 14,
    },
    textParagrafo:{
        color: 'black',
        fontSize: 14,
        marginBottom: 5,
        paddingBottom: 5,
    },
    borderBottom:{
        marginBottom: 10,
        paddingBottom: 10,
        borderBottomColor: colors.lineGrey,
        borderBottomWidth: 1
    },
    buttonText: {
        color: 'blue',
        marginBottom: 20,
        fontSize: 20,
        fontFamily: "Quicksand-Bold",
    },
    toolbar:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    toolbarBtn:{
        backgroundColor: colors.primary,
        borderRadius: 0,
        flexGrow: 2,
        flexDirection: 'column',
        justifyContent: 'center',
        padding: 10,
        alignItems: 'center'
    },
    toolbarText:{
        color: '#FFFFFF',
        fontSize: 18,
        fontFamily: "Quicksand-Bold",
    }
});

export const overlayStyle = StyleSheet.create({
    content:{
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10
    },
    title:{
        fontSize: 20,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        margin: 7
    },
    text:{
        fontSize: 14
    }
});

export const accordionStyles = StyleSheet.create({
    title:{
        fontSize: 14,
        fontWeight:'bold',
        color: colors.darkGray,
    },
    row:{
        flexDirection: 'row',
        justifyContent:'space-between',
        paddingLeft:0,
        paddingRight:25,
        alignItems:'center'
    },
    parentHr:{
        color: "#FFFFFF"
    },
    child:{
        backgroundColor: colors.lightGray,
    }
});