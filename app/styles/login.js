import { StyleSheet } from 'react-native';
import colors from '../constants/Colors';

const defaultFontSize = 18;

export const loginStyles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: colors.dark,
    },
    container: {
        padding: 40,
    },
    logo: {
        alignSelf: 'center',
        padding: 20,
    },
    btn: {
        backgroundColor: colors.primary,
        color: colors.primaryText,
        fontSize: 14,
        textAlign: 'center',
        padding: 10,
        borderRadius: 25,
        marginHorizontal: 10,
        fontFamily: 'OpenSans-Bold',
    },
});

export const formCadastroStyles = {
    cadastro: {
        flex: 1,
        paddingHorizontal: 20,
        justifyContent: 'space-between',
    },
    titulo_cadastro: {
        padding: 15,
        fontSize: 22,
        color: colors.defaultText,
        fontWeight: 'bold',
        fontFamily: 'Josefin Sans',
        alignSelf: 'center'
    },
    text_auxiliar: {
        paddingBottom: 20,
        fontSize: 15,
        color: colors.defaultText,
        fontWeight: 'bold',
        fontFamily: 'Josefin Sans',
        textAlign: 'center',
        alignSelf: 'center'
    },
};
export const formLoginStyles = {
    modal: {
        backgroundColor: colors.bgLogin,
        padding: 30
    },
    container: {
        padding: 30,
    },
    input: {
        backgroundColor: '#1CBFA3',
        color: '#FFFFFF',
        placeholderTextColor: '#FFFFFF',
        borderRadius: 26,
        padding: 10,
        fontSize: 18
    },
    inputCadastro: {
        color: colors.dark,
        fontSize: 14,
        height: 36,
        padding: 10,
        borderColor: '#BDBDBD', // <= relevant style here
        borderBottomWidth: 1,
        marginBottom: 10,
    },
    tituloModal: {
        fontSize: 25,
        color: '#FFF',
        padding: 15,
        fontWeight: 'bold',
        fontFamily: 'Josefin Sans',
        alignSelf: 'center'
    },
    buttonText: {
        fontSize: 18,
        color: colors.textInPrimary,
        fontWeight: 'bold',
        fontFamily: 'Josefin Sans',
        alignSelf: 'center',
    },
    button: {
        padding: 10,
        backgroundColor: colors.bgLogin,
        borderRadius: 26,
        marginVertical: 10,
    },
    secondaryButton: {
        padding: 10,
        backgroundColor: colors.secondary,
        borderRadius: 26,
        marginVertical: 10,
    },
    secondaryButtonText: {
        fontSize: 18,
        color: colors.textInSecondary,
        fontWeight: 'bold',
        fontFamily: 'Josefin Sans',
        alignSelf: 'center',
    },
};
export const formLoginStyles2 = {
    textbox: {
        // the style applied wihtout errors
        normal: {
            color: colors.textInPrimary,
            fontSize: 14,
            paddingVertical: 10,
            paddingHorizontal: 20,
            marginBottom: 5,
            borderBottomWidth: 0.5,
            borderBottomColor: '#d6d7da',
            fontFamily: 'Josefin Sans',
        },

        // the style applied when a validation error occours
        error: {
            color: colors.danger2,
            fontSize: 14,
            paddingVertical: 10,
            paddingHorizontal: 20,
            marginBottom: 5,
            borderBottomColor: colors.danger2,
            borderBottomWidth: 0.5,
            backgroundColor: colors.primaryLight,
            fontFamily: 'Josefin Sans',
        }
    },
    controlIcon: {
        container: {
            normal: {
                paddingVertical: 10,
                paddingHorizontal: 20,
                marginBottom: 5,
                borderBottomWidth: 0.5,
                borderBottomColor: '#d6d7da',
                marginBottom: 5,
            },

            error: {
                paddingVertical: 10,
                paddingHorizontal: 20,
                borderRadius: 8,
                marginBottom: 5,
                borderColor: colors.danger2,
                borderWidth: 1,
                backgroundColor: colors.primaryLight,
            },
        },

        input: {
            normal: {
                color: colors.textInPrimary,
                fontSize: 14,
                fontFamily: 'Josefin Sans',
            },

            error: {
                color: colors.danger2,
                fontSize: 14,
                fontFamily: 'Josefin Sans',
            }
        },

        button: {
            normal: {
                color: colors.textInPrimary,
                fontSize: 14,
            },

            error: {
                color: colors.textInPrimary,
                fontSize: 14,
            }
        },
    },
    errorBlock: {
        color: colors.danger2,
        fontSize: 14,
        textAlign: 'center',
    },
};
