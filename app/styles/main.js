import { StyleSheet } from 'react-native';
import colors from '../constants/Colors';

export const mainStyles = StyleSheet.create({
    container: {
        flex: 1,
    },
    body: {
        flex: 1,
        backgroundColor: colors.defaultBackground,
    },
    // body: {
    //     flex: 1,
    //     alignItems: 'center',
    //     alignSelf: 'stretch',
    //     backgroundColor: colors.bgDefault,
    //     flexDirection: 'column',
    //     justifyContent: 'space-between',
    // },
    bold: {
        fontWeight: 'bold',
    },
    h2: {
        marginVertical: 10,
        textAlign: 'center',
        fontFamily: 'Raleway-ExtraBold',
        color: colors.defaultText,
        fontSize: 16,
        fontFamily: "Quicksand-Regular",
    },
    sendIcon:{
        color: colors.primary,
    },
    btn: {
        padding: 10,
        borderRadius: 4,
        marginVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    },
    btnPrimary: {
        backgroundColor: colors.primary,
    },
    btnPrimaryIcon: {
        fontSize: 18,
        color: colors.textInPrimary,
        marginHorizontal: 5,
    },
    btnPrimaryText: {
        fontSize: 18,
        color: colors.textInPrimary,
        fontFamily: 'Josefin Sans',
        fontWeight: 'bold',
    },
    btnSmPrimaryText: {
        fontSize: 11,
        color: colors.textInPrimary,
        fontFamily: 'Josefin Sans',
        fontWeight: 'bold',
    },
    btnSecondary: {
        backgroundColor: colors.secondary,
    },
    btnSecondaryText: {
        fontSize: 18,
        color: colors.textInSecondary,
        fontWeight: 'bold',
        fontFamily: 'Josefin Sans',
    },
    btnTertiary: {
        backgroundColor: colors.tertiary,
    },
    btnTertiaryText: {
        fontSize: 18,
        color: colors.textInTertiary,
        fontWeight: 'bold',
        fontFamily: 'Josefin Sans',
    },
    btnSmTertiaryText: {
        fontSize: 11,
        color: colors.textInTertiary,
        fontWeight: 'bold',
        fontFamily: 'Josefin Sans',
    },
    modalContent: {
        padding: 20,
        justifyContent: 'center',
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
    },
    modalHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.primary,
        padding: 10,
    },
    modalClose: {
        color: colors.textInPrimary,
        alignSelf: 'center',
        fontSize: 22,
        marginLeft: 10,
    },
    modalTitle: {
        color: colors.textInPrimary,
        alignSelf: 'center',
        marginVertical: 5,
        marginLeft: 13,
        fontSize: 18,
        fontFamily: 'Josefin Sans',
        fontWeight: 'bold',
        flex: 1,
        textAlign: 'left',
    },
    modalBody: {
        backgroundColor: '#fff',
        padding: 20,
    },
    modalInput: {
        color: colors.textInPrimary,
        fontSize: 14,
        height: 45,
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 8,
        marginBottom: 5,
        backgroundColor: colors.primaryLight,
        fontFamily: 'Josefin Sans',
    },
    footer: {
        justifyContent: 'space-around',
        flexDirection: 'row',
        fontSize: 16,
        fontFamily: "Quicksand-Regular",
        backgroundColor: '#34495E',
        alignSelf: 'stretch'
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'stretch',
        paddingHorizontal: 15,
        paddingVertical: 15
    },
    emptyWarning: {
        marginVertical: 20,
        color: colors.defaultText,
        fontFamily: 'Josefin Sans',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 20,
    },
    noConnectionWarning: {
        color: colors.defaultText,
        fontFamily: 'Josefin Sans',
        textAlign: 'center',
        fontSize: 12,
    },
    noConnectionIcon: {
        color: colors.defaultText,
        textAlign: 'center',
        fontSize: 18,
    },
    bgLightGrey: {
        backgroundColor: colors.lightGray,
    },
});

export const fonts = {
    titulo_home: 'Chalkduster',
    texto_home: 'Josefin Sans',
    sans_bold: 'Josefin Sans',
    texto_icones: 'Rono',
};

export const htmlContentStyles = StyleSheet.create({
    p: {
        marginVertical: 5,
        color: colors.defaultText,
        fontFamily: 'Josefin Sans',
        fontSize: 12,
        textAlign: 'justify',
    },
    li: {
        marginVertical: 5,
        color: colors.defaultText,
        fontFamily: 'Josefin Sans',
        fontSize: 12,
        textAlign: 'justify',
    }
});

export const defaultFormStyles = {
    placeholderColor: '#929292',
    
    textbox: {
        // the style applied wihtout errors
        normal: {
            color: colors.defaultText,
            fontSize: 16,
            fontFamily: "Quicksand-Regular",
            padding: 10,
            borderColor: colors.gray, // <= relevant style here
            borderBottomWidth: 1,
            marginTop: 5,
        },

        notEditable: {
            color: colors.darkGray,
            fontSize: 16,
            fontFamily: "Quicksand-Regular",
            padding: 10,
            borderColor: colors.gray, // <= relevant style here
            borderBottomWidth: 1,
            marginTop: 5,
        },

        // the style applied when a validation error occours
        error: {
            color: colors.danger,
            fontSize: 16,
            fontFamily: "Quicksand-Regular",
            padding: 10,
            borderColor: colors.danger, // <= relevant style here
            borderBottomWidth: 1,
            marginTop: 5
        }
    },
    controlIcon: {
        container: {
            normal: {
                padding: 10,
                borderColor: colors.gray,
                borderBottomWidth: 1,
                marginTop: 5,
            },

            error: {
                padding: 10,
                borderColor: colors.danger,
                borderBottomWidth: 1,
                marginTop: 5
            },

            notEditable: {
                padding: 10,
                borderColor: colors.gray,
                borderBottomWidth: 1,
                marginTop: 5,
            }
        },

        input: {
            normal: {
                color: colors.defaultText,
                fontSize: 16,
            fontFamily: "Quicksand-Regular",
                padding: 10,
            },

            error: {
                color: colors.danger,
                fontSize: 16,
            fontFamily: "Quicksand-Regular",
            }
        },

        button: {
            normal: {
                color: colors.darkGray,
                fontSize: 18,
            },

            error: {
                color: colors.darkGray,
                fontSize: 18,
            }
        },
    },
    errorBlock: {
        color: colors.danger,
        fontSize: 14,
    },
    textboxView: {
        padding: 10
    },
    controlLabel: {
        normal: {
            color: colors.defaultText,
            fontSize: 14,
        },
    },
    checkbox: {
        normal: {
            marginRight: 10,
        }
    }
};
