import React from 'react';
import { createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import Home from '../scenes/home';
import EbooksScreen from '../scenes/EbooksScreen';
import EsquemaScreen from '../scenes/EsquemaScreen';
import FacaRedacaoScreen from '../scenes/FacaRedacaoScreen';
import JogoScreen from '../scenes/JogoScreen';
import MaratonaScreen from '../scenes/MaratonaScreen';
import PptsScreen from '../scenes/PptsScreen';
import PodcastsScreen from '../scenes/PodcastsScreen';
import QuadroScreen from '../scenes/QuadroScreen';
import TemasScreen from '../scenes/TemasScreen';

const AppStack = MainTabNavigator;
const HomeStack = createStackNavigator({ screen: Home });
const EbookStack = createStackNavigator({ screen: EbooksScreen });
const EsquemaStack = createStackNavigator({ screen: EsquemaScreen });
const FacaRedacaoStack = createStackNavigator({ screen: FacaRedacaoScreen });
const JogoStack = createStackNavigator({ screen: JogoScreen });
const MaratonaStack = createStackNavigator({ screen: MaratonaScreen });
const PptsStack = createStackNavigator({ screen: PptsScreen });
const PodcastsStack = createStackNavigator({ screen: PodcastsScreen });
const QuadroStack = createStackNavigator({ screen: QuadroScreen });
const TemasStack = createStackNavigator({ screen: TemasScreen });

export default createAppContainer(
  createSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    App: AppStack,
    Home: HomeStack,
    Ebooks: EbookStack,
    Esquemas: EsquemaStack,
    FacaRedacao: FacaRedacaoStack,
    Jogo: JogoStack,
    Maratona: MaratonaStack,
    Ppts: PptsStack,
    Podcasts: PodcastsStack,
    Quadro: QuadroStack,
    Temas: TemasStack,
  })
);
