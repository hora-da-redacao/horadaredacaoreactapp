const tintColor = '#2f95dc';

export const gradientLoadingColors = [
  '#D5D5D5',
  '#7F7F7F',
];

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  
  primary:    '#107AE8',
  primaryLight: '#4589CB',
  primaryLighter: '#A2C4E5',
  textInPrimary: '#FFFFFF',

  secondary:  '#EF8712',
  secondaryLight: '#EC8600',
  textInSecondary: '#FFFFFF',

  tertiary: '#003F57',
  tertiaryLight: '#FFFFFF',
  textInTertiary: '#FFFFFF',
  
  defaultText: '#003F57',
  secondaryText: '#7E4408',

  inputColor: '#A2C4E5',
  
  gray: '#E0E7E8',
  darkGray: '#929292',
  light:      '#f4f4f4',
  lightGray: '#FAFAFA',
  lineGrey: '#e6dbd9',
  defaultBackground: '#FFFFFF',
  footerBackground: '#fff',
  danger:     '#f53d3e',
  danger2: '#FFA966',
  cardCircle: '#FFFFFF',

  dark: '#222',
  darkBlue: '#0E5AA7',
};
