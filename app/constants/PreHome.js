import { StyleSheet } from 'react-native';
import colors from './Colors';
import { mainStyles, fonts } from './Main';

export const prehomeStyles = StyleSheet.create({
    banners: {
        alignSelf: 'stretch',
        height: 300
    },
    banner: {
        alignSelf: 'stretch',
        flexDirection: 'column',
    },
    bannerList: {
        alignSelf: 'stretch',
    },
    textoBanner: {
        alignSelf: 'stretch',
    },
    bannerImg: {
        alignSelf: 'stretch',
    },
    icones: {
        alignSelf: 'stretch',
        flexDirection: 'row',
        height: 70,
        justifyContent: 'space-evenly',
    },
    icone: {
        width: 89,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        flex: 1
    },
    texto_icone: {
        color: colors.defaultText,
        fontFamily: fonts.texto_icones,
        fontWeight: 'bold',
        marginTop: 10,
        fontSize: 10,
        alignSelf: 'stretch',
        textAlign: 'center'
    },
    fitImage: {
        borderRadius: 20,
    },
    fitImageWithSize: {
        height: 100,
        width: 30,
    },
    // body: mainStyles.body,
    titulo: {
        color: colors.defaultText,
        fontFamily: fonts.titulo_home,
        fontSize: 25,
        marginHorizontal: 10,
        marginVertical: 20,
    },
    texto_auxiliar: {
        color: colors.defaultText,
        fontFamily: fonts.texto_home,
        fontSize: 12,
        textAlign: 'center'
    },
    bold: {
        fontFamily: fonts.texto_home,
        fontWeight: 'bold',
        fontSize: 16,
    },
});

export const footerPrehome = StyleSheet.create({
    footer: {
        backgroundColor: colors.footerBackground,
        justifyContent: 'space-between',
        flexDirection: 'row',
        borderTopWidth: 1,
        borderTopColor: colors.gray,
        alignSelf: 'stretch',
        paddingHorizontal: 10,
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'stretch',
        padding: 15,
    },
    icon: {
        fontSize: 22,
        color: colors.defaultText,
    },
    iconInactive: {
        fontSize: 22,
        color: colors.darkGray,
    }
});
