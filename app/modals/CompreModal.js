import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet, Animated, Dimensions, Text, TouchableOpacity, TouchableWithoutFeedback, ImageBackground, Linking, Button} from 'react-native';
import {Actions} from 'react-native-router-flux';
import AsyncStorage from "@react-native-community/async-storage";
//import * as RNIap from 'react-native-iap';
import RNIap, {
    Product,
    ProductPurchase,
    PurchaseError,
    acknowledgePurchaseAndroid,
    purchaseErrorListener,
    purchaseUpdatedListener,
} from 'react-native-iap';
//import {Product, RNIap} from 'react-native-iap';
import Icon from 'react-native-vector-icons/Feather';
import Providers from '../providers/Providers';
import {retrieveItem} from '../store/store';
import env from '../../env.json';

const {height: deviceHeight, width: deviceWidth} = Dimensions.get('window');

const itemSkus = Platform.select({
    ios: [
      'com.ionicframework.horadaredacao.plano_prata'
    ],
    android: [
      'planoprata'
    ]
});

let purchaseUpdateSubscription
let purchaseErrorSubscription

export default class CompreModal extends Component {
    
    static propTypes = {
        children: PropTypes.any,
        horizontalPercent: PropTypes.number,
        verticalPercent: PropTypes.number,
    };

    constructor(props) {
        super(props);

        this.state = {
            opacity: new Animated.Value(0),
            products: null,
            conteudo: null,
            user: null
        };
    }

    async componentDidMount(): void {
        var user = retrieveItem('horadaredacao.user');
        user.then(u => {
            if(u==null){
                Actions.login({ type: 'replace' });
            }else{
                this.setState({
                  user: u
                }, async function () {

                    try{
                        const result = await RNIap.initConnection();
                        console.log('Conectou?', result);
                        await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
                        RNIap.getSubscriptions(itemSkus).then((products) => {
                            //handle success of fetch product list
                            console.log("Produtos");
                            console.log(products);
                            this.setState({products: products});
                        }).catch((error) => {
                            console.log(error.message);
                        });

                        Animated.timing(this.state.opacity, {
                            duration: 500,
                            toValue: 1,
                        }).start();
                        
                        Providers.conteudo_prata()
                        .then(response => {
                            console.log(response);
                            this.setState({conteudo: response.conteudo.split(','), titulo: response.titulo});
                        })
                        .catch(error => {
                            //this.setModalVisible(false);
                            console.log(error);
                        });
                    }catch(err){
                        console.log('Deu Erro', err);
                    }

                    purchaseUpdateSubscription = purchaseUpdatedListener(
                        async (purchase: ProductPurchase) => {
                        console.log('purchaseUpdatedListener', purchase);
                        if (purchase.purchaseStateAndroid === 1 && !purchase.isAcknowledgedAndroid) {
                            try {
                                    const ackResult = await acknowledgePurchaseAndroid(
                                    purchase.purchaseToken,
                                );
                                console.log('ackResult', ackResult);
                            } catch (ackErr) {
                                console.warn('ackErr', ackErr);
                            }
                        }

                        this.purchaseConfirmed();
                        this.setState({receipt: purchase.transactionReceipt});
                        purchaseErrorSubscription = purchaseErrorListener(
                            (error: PurchaseError) => {
                                console.log('purchaseErrorListener', error);
                            // alert('purchase error', JSON.stringify(error));
                            },
                        );
                    });

                });
            }
        });
    }

    closeModal(){
        Animated.timing(this.state.opacity, {
        duration: 500,
        toValue: 0,
        }).start(Actions.pop);
        console.log('log');
    };

    openPolitica(){
        Linking.openURL(env.url_politica);
    }

    requestPurchase = async (sku) => {
        try {
        await RNIap.requestPurchase(sku, false);
        } catch (err) {
        console.warn(err.code, err.message);
        }
    }

    requestSubscription = async (sku) => {
        console.log(itemSkus[0]);
        console.log(Platform.OS);

        RNIap.requestSubscription(itemSkus[0]).then((result) => {
            console.log('result request', result);
            let usuid = this.state.user._id.$oid;
            var data = {
              userid: usuid,
              platform: Platform.OS,
              result: result
            }
            Providers.compra_prata(data)
            .then(response => {
                console.log(result);
                AsyncStorage.getItem( 'horadaredacao.user' )
                .then( data => {

                    // the string value read from AsyncStorage has been assigned to data
                    console.log( data );

                    // transform it back to an object
                    data = JSON.parse( data );
                    console.log( data );

                    // Decrement
                    data.tipo=3;
                    console.log( data );

                    //save the value to AsyncStorage again
                    AsyncStorage.setItem( 'horadaredacao.user', JSON.stringify( data ) );
                    this.closeModal();

                }).done();
                //Actions.drawer({ type: 'reset' }); 
                //Actions.refresh();
            })
            .catch(error => {
                //this.setModalVisible(false);
                console.log(error);
            });
        }).catch((e) => {
            console.log('error request', e);
        });
    }

    _renderLightBox = () => {
        const {children, horizontalPercent = 1, verticalPercent = 1} = this.props;
        const height = verticalPercent
        ? deviceHeight * verticalPercent
        : deviceHeight;
        const width = horizontalPercent
        ? deviceWidth * horizontalPercent
        : deviceWidth;
        return (
            <View style={styles.container}>
                <ImageBackground 
                source={
                    Platform.OS === 'android'
                    ? require('../assets/images/drawable-hdpi/imagem2.png')
                    : require('../assets/images/drawable-hdpi/imagem2.png')
                }
                style={styles.imgBack}>
                    <View style={{ alignItems: 'flex-end',  top: 50, right: 20}}>
                        <TouchableWithoutFeedback style={{ position: 'absolute', backgroundColor: '#FFF',padding: 26, margin: 26, width: 90}} onPress={() => {this.closeModal()}}>
                            <View style={styles.iconClose}>
                                <Icon name='x' size={18} style={styles.statsIcon} />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>

                        <View style={styles.just}>
                            {/* <View style={styles.plano}>
                                <Icon name="star" size={23} color="#107ae8" style={styles.star}/>
                                <Text style={styles.planoText}>plano prata</Text>
                            </View> */}
                            <Text style={styles.title}>comece agora</Text>
                        </View>
                    {(this.state.products != null && this.state.conteudo != null) && (
                        <View>
                        <View style={styles.just}>
                            {/* <View style={styles.plano}>
                                <Icon name="star" size={23} color="#107ae8" style={styles.star}/>
                                <Text style={styles.planoText}>plano prata</Text>
                            </View> */}
                            <View style={styles.list}>
                                {this.state.conteudo.map(r => 
                                    <View style={styles.listItem}>
                                        <Icon name="check-circle" size={15} color="#fff" style={styles.check}/>
                                        <Text style={styles.listText}>{r}</Text>
                                    </View>
                                )}
                            </View>
                        </View>
                        {this.state.products[0] != undefined && (
                        <View>
                            <TouchableOpacity style={styles.button} onPress={() => {this.requestSubscription(this.state.products[0])}}>
                                <Text style={styles.buttonText}>Assine o {this.state.titulo}</Text>
                            </TouchableOpacity>
                            <Text style={styles.text}>7 dias de graça, depois R$ {(this.state.products[0].price/12).toFixed(2).replace('.',',')} / mês pagos anualmente.{"\n"}Cancele a qualquer momento</Text>
                        </View>
                        )}
                        </View>
                    )}
                    {children}
                    <View style={styles.subTexts}>
                        <TouchableWithoutFeedback style={styles.terms} onPress={() => this.openPolitica()}>
                            <Text style={styles.termsText}>Termos e Condições</Text>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback style={styles.pol} onPress={() => this.openPolitica()}>
                            <Text style={styles.polText}>Política de Privacidade</Text>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback style={styles.pol} onPress={() => this.getSubscriptions()}>
                            <Text style={styles.polText}>teste</Text>
                        </TouchableWithoutFeedback>
                    </View>
                </ImageBackground>
            </View>
    );
  };

  render() {
    return (
      <Animated.View style={[styles.container, {opacity: this.state.opacity}]}>
        {this._renderLightBox()}
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(0,90,215,0.8)',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0,
    },
    iconClose: {
        opacity: 1,
        backgroundColor: "#0A5CAF",
        justifyContent: "center",
        alignItems: "center",
        width: 40,
        height: 40,
        top: 10,
        right: 0,
        borderRadius: 20
    },
    statsIcon: {
        color: "#FFFFFF",
        alignSelf: "center"
    },
    imgBack: {
        flex: 1,
        margin: 0,
        width: "100%",
    },
    just: {
        width: "80%",
        marginHorizontal: 30,
    },
    plano: {
        width: 150,
        height: 34,
        borderRadius: 4,
        backgroundColor: "#ffffff",
        marginTop: 20,
        flexDirection: "row"
    },
    star: {
        marginTop: 5,
        marginLeft: 3
    },
    planoText: {
        fontFamily: "Quicksand-Bold",
        fontSize: 16,
        textAlign: "left",
        color: "#107ae8",
        textTransform: 'uppercase',
        marginVertical: 9,
        marginLeft: 4
    },
    title: {
        fontFamily: "Quicksand-Regular",
        fontSize: 25,
        lineHeight: 31,
        textAlign: "left",
        color: "#ffffff",
        marginTop: 25,
        textTransform: 'uppercase'
    },
    exp: {
        width: 177,
        height: 28,
        borderRadius: 16,
        backgroundColor: "#094e96",
        marginTop: 24
    },
    expText: {
        fontFamily: "Quicksand-Regular",
        fontSize: 12,
        textAlign: "center",
        color: "#ffffff",
        marginVertical: 8
    },
    list: {
        marginTop: 25,
        marginBottom: 44
    },
    listItem: {
        flexDirection: 'row',
        marginBottom: 15,
    },
    check: {
        marginRight: 5
    },
    listText: {
        fontFamily: "Quicksand-Regular",
        fontSize: 20,
        lineHeight: 20,
        textAlign: "left",
        color: "#ffffff"
    },
    button: {
        width: 300,
        height: 47,
        backgroundColor: "#ffffff",
        borderRadius: 20,
        alignSelf: 'center',
        marginBottom: 13
    },
    buttonText: {
        fontFamily: "Quicksand-Bold",
        fontSize: 20,
        textAlign: "center",
        color: "#107ae8",
        marginVertical: 13,
        textTransform: 'uppercase'
    },
    text: {
        fontFamily: "Quicksand-Regular",
        fontSize: 12,
        lineHeight: 15,
        textAlign: "center",
        color: "#ffffff",
        alignSelf: "center"
    },
    subTexts: {
        flexDirection: "row",
        justifyContent: 'space-around',
        marginBottom: 34,
        marginTop: 36,
        width: 316,
        alignSelf: 'center'
    },
    terms: {fontFamily: "Quicksand-Bold",},
    termsText: {
        fontFamily: "Quicksand-Bold",
        fontSize: 12,
        lineHeight: 15,
        textAlign: "center",
        color: "#ffffff"
    },
    pol: {},
    polText: {
        fontFamily: "Quicksand-Bold",
        fontSize: 12,
        lineHeight: 15,
        textAlign: "center",
        color: "#ffffff"
    }
});
