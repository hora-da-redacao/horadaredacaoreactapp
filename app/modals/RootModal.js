import * as React from 'react';
import {Modal, Button, View, Text} from 'react-native';
import {connect} from 'react-redux';
import Error from './Error';
import Success from './Success';

// import our new actions
import {ModalActions} from '../store/modules/Modal/ModalActions';

export class RootModal extends React.Component {
  constructor(props) {
      super(props);
  }
  componentDidMount(){
    //console.log(this.props);
  }

  close(){
    this.setState({ hideModal: false });
  }

  render() {
    const {id, hideModal} = this.props;

    // assign a constant that is either one of our custom views or a noop function if the id is not set
    //const ModalView = Modals[id] || function() {};

    return (
      // show the Modal if the id is set to a truthy value
      <Modal visible={Boolean(id)} animationType="fade" testID="modal">
        <View
          style={{
            flex: 1,
            padding: 20,
            justifyContent: 'space-between',
          }}>
          {/* inject the custom view */}
          <Text>Textop</Text>
          <Button onPress={() => this.close()} title="Close" color="blue" />
        </View>
      </Modal>
    );
  }
}

const mapStateToProps = state => {
  //console.log(state);
  return {
    id: state.id,
  };
};

// add hideModal action to props
const mapDispatchToProps = {
  hideModal: ModalActions.hideModal,
  showModal: ModalActions.showModal,
};

const ConnectedRootModal = connect(
  mapStateToProps,
  mapDispatchToProps,
)(RootModal);

export default ConnectedRootModal;
