import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ImageBackground,
    Image,
    StyleSheet
} from 'react-native';

import { Actions } from 'react-native-router-flux';

export default class Inicio extends Component {
    render(){
        return(
            <View style={styles.container}>
                <ImageBackground source={require('../assets/images/drawable-hdpi/imagem1.png')} style={styles.imgBack}>
                    <Image source={require('../assets/images/logo-hredacao.png')} style={styles.logoImg}/>
                    <Text style={styles.title}>Bem vindo (a)</Text>
                    <Text style={styles.text}>Aprende redação com método{"\n"}exclusivo e revolucionário</Text>
                    <View style={styles.just}>
                        <View style={styles.eachDot}></View>
                        <View style={styles.eachDot}></View>
                        <View style={styles.eachDot}></View>
                    </View>
                    <TouchableOpacity style={styles.button} onPress={() => {Actions.ComeceGratis()}}>
                        <Text style={styles.buttonText}>Iniciar</Text>
                    </TouchableOpacity>
                    <View style={styles.just}>
                        <Text style={styles.accountCheck}>Já tem uma conta?</Text>
                        <TouchableWithoutFeedback onPress={() => {Actions.NewLogin()}}><Text style={[styles.accountCheck, styles.login]}> Entrar</Text></TouchableWithoutFeedback>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(1,25,50,0.8)'
    },
    imgBack: {
        flex: 1,
    },
    logoImg: {
        alignSelf: 'center',
        width: 127,
        height: 127,
        marginTop: 80,
        marginBottom: 70
    },
    title: {
        fontFamily: "Quicksand",
        fontSize: 20,
        fontWeight: "bold",
        fontStyle: "normal",
        lineHeight: 25,
        letterSpacing: 0,
        color: "#ffffff",
        alignSelf: 'center',
        marginBottom: 9
    },
    text: {
        fontFamily: "Quicksand",
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0,
        color: "#ffffff",
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: 15
    },
    just: {
        flexDirection: 'row',
        alignSelf: 'center',
    },
    eachDot: {
        width: 13,
        height: 13,
        backgroundColor: "#ffffff",
        borderRadius: 100,
        marginLeft: 4.3,
        marginRight: 4.3,
        marginBottom: 96
    },
    button: {
        width: 260,
        height: 41,
        backgroundColor: "#107ae8",
        alignSelf: 'center',
        marginBottom: 17.4,
        borderRadius: 20,
    },
    buttonText: {
        fontFamily: "Quicksand",
        fontSize: 16,
        fontWeight: "bold",
        textAlign: "center",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0,
        color: "#fefdfc",
        marginTop: 12,
        textTransform: 'uppercase'
    },
    accountCheck: {
        fontFamily: "Quicksand",
        fontSize: 14,
        fontWeight: "normal",
        fontStyle: "normal",
        lineHeight: 18,
        letterSpacing: 0,
        color: "#ffffff",
        alignSelf: 'center',
        marginBottom: 89
    },
    login: {
        fontWeight: 'bold'
    },
});