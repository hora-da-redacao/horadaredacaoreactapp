import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ImageBackground,
    Image,
    ScrollView,
    StyleSheet
} from 'react-native';

//Importando Objeto de Autenticação
import Auth from '../auth/auth';

import { Actions } from 'react-native-router-flux';
import t from 'tcomb-form-native';
import { defaultFormStyles, mainStyles } from '../styles/main';
import inputShowPasswordTemplate from '../component/input-show-password';

const Form = t.form.Form;

const Email = t.refinement(t.String, email => {
    const reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/; //or any other regexp
    return reg.test(email);
});

export default class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            logging: false,
            modalVisible: false,
            modalVisibleSenha: false,
            recEmail: '',
            passwordHidden: true,
            user: {},
        };

        this.User = t.struct({
            username: Email,
            password: t.String
        });

        this.formOptions = {
            fields: {
                username: {
                    label: '',
                    placeholder: 'E-mail',
                    error: 'Campo obrigatório',
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    returnKeyType: 'next',
                    textContentType: 'emailAddress',
                    //autoFocus: true,
                    keyboardType: 'email-address',
                    autoCapitalize: 'none',
                    onSubmitEditing: () => this._form.getComponent('password')
                        .refs
                        .input
                        .refs
                        .input
                        .focus(),
                },
                password: {
                    label: '',
                    placeholder: 'Senha',
                    template: inputShowPasswordTemplate,
                    error: 'Campo obrigatório',
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    onSubmitEditing: () => this.login(),
                    returnKeyType: 'go',
                },
            },
            stylesheet: {
                ...Form.stylesheet,
                ...defaultFormStyles,
            },
            auto: 'none'
        };
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    setModalVisibleSenha(visible) {
        this.setState({ modalVisibleSenha: visible });
    }

    login() {
        var values = this._form.getValue();
        var result = this._form.validate(values, this.User);

        if (result.isValid()) {
            this.setState({ logging: true });

            console.log('sou valido');
            //this.setModalVisible(true);
            Auth.login(values)
            .then(response => {
                //console.log(response);
                if(response === true){
                    //Actions.drawer({ type: 'reset' });
                    Actions.home({ type: 'replace' });
                }
                //this.setModalVisible(false);
            })
            .catch(error => {
                //this.setModalVisible(false);
                console.log(error);
                this.setState({ logging: false });

                Alert.alert(
                    'FALHA NO LOGIN',
                    'Usuário ou senha inválidos',
                    [
                        {
                            text: 'OK',
                            onPress: () => {
                                this.setState({ user: { username: values.username } });
                                setTimeout(() => {
                                    this._form.getComponent('password')
                                        .refs
                                        .input
                                        .refs
                                        .input
                                        .focus();
                                }, 500);
                            }
                        },
                    ],
                    { cancelable: false }
                );
            });
        } else {
            //alert('Verifique as informações e tente novamente');
            console.log(result.firstError().message);
        }
    }
 
    render(){
        return(
            <View style={styles.container}>
                <ScrollView keyboardShouldPersistTaps="always">
                    <ImageBackground source={require('../assets/images/drawable-hdpi/imagem3.png')} style={styles.imgBack}>
                        <Image source={require('../assets/images/logo-hredacao.png')} style={styles.logo}/>
                        <View style={styles.fields}>
                            <Form
                                type={this.User}
                                options={this.formOptions}
                                ref={c => this._form = c}
                                value={this.state.user}
                                onChange={(user) => this.setState({ user })}
                            />
                        </View>
                        <TouchableOpacity style={styles.button}><Text style={styles.buttonText} onPress={() => this.login()}>entrar</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.button}><Text style={styles.buttonText} onPress={() => {Actions.cadastro()}}>cadastrar</Text></TouchableOpacity>
                    </ImageBackground>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    imgBack: {
        flex: 1,
    },
    logo: {
        width: 157,
        height: 157,
        marginTop: 43,
        marginBottom: 25,
        alignSelf: 'center'
    },
    fields: {
        width: 316,
        alignSelf: 'center'
    },
    button: {
        width: 310,
        height: 48.5,
        backgroundColor: '#107ae8',
        marginTop: 35.5,
        alignSelf: 'center',
        borderRadius: 30
    },
    buttonText: {
        fontFamily: "Quicksand-Bold",
        fontSize: 18,
        lineHeight: 23,
        textAlign: "center",
        color: "#fefdfc",
        textTransform: 'uppercase',
        marginTop: 14
    },
});