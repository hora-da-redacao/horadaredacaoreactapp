import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ImageBackground,
    Image,
    StyleSheet,
    ScrollView
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Home extends Component {
    constructor(props) {
        super(props)

        /* this.state = {
            user: {},
            nomeUsuario: ''
        };  */ 
    }

    render(){
        return(
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.header}>
                        <Image source={require('../assets/images/logo-hredacao.png')} style={styles.imgPerfil}/>
                         <Text style={styles.text}>Olá,{"\n"}<Text style={styles.name}>Jonatas</Text></Text> 
                        <TouchableOpacity style={styles.topButton}>
                            <Icon name="trophy" size={24} color="#fff" style={styles.icon}/>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        height: 179,
        backgroundColor: '#107ae8',
        flexDirection: 'row',
    },
    imgPerfil: {
        width: 60,
        height: 60,
        backgroundColor: "#ffffff",
        borderRadius: 100,
        marginTop: 25,
        marginLeft: 28
    },
    text: {
        fontSize: 16,
        fontFamily: 'Quicksand',
        textAlign: 'left',
        color: '#fff',
        marginTop: 33,
        marginLeft: 12
    },
    name: {
        fontWeight: 'bold'
    },
    topButton: {
        width: 45,
        height: 45,
        backgroundColor: "#0a5caf",
        borderRadius: 100,
        marginTop: 32,
        position: 'absolute',
        right: 28
    },
    icon: {
        textAlign: 'center',
        marginTop: 11
    }
});