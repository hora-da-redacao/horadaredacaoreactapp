import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ImageBackground,
    Image,
    ScrollView,
    StyleSheet,
    Alert,
    ActivityIndicator
} from 'react-native';

import ImagePicker from 'react-native-image-picker';
//import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

import { Actions } from 'react-native-router-flux';
import t from 'tcomb-form-native';
import { defaultFormStyles, mainStyles } from '../styles/main';
import colors from '../constants/Colors';
import { isValidCPF, isValidEmail } from '../component/tcomb-validators';
import inputShowPasswordTemplate from '../component/input-show-password';
import inputCell from '../component/input-cell';
import {retrieveItem} from '../store/store';
import Auth from '../auth/auth';

import env from '../../env.json';


const Form = t.form.Form;

export default class Perfil extends Component {
    constructor(props) {
        super(props)

        this.state = {
            logging: false,
            user: {
                fullImage: '',
                imagem_file_name: null
            },
        };

        this.samePassword = t.refinement(t.String, (s) => {
            return s == this.state.user.senha;
        });

        this.Email = t.refinement(t.String, email => {
            return isValidEmail(email);
        });

        this.User = t.struct({
            nome: t.String,
            email: this.Email,
            celular: t.maybe(t.String),
            cidade: t.String,
            estado: t.String,
            senha: t.maybe(t.String),
            confirmacaoSenha: t.maybe(this.samePassword),
        });

        this.formOptions = {
            fields: {
                nome: {
                    label: '',
                    placeholder: 'Nome Completo',
                    error: 'Preencha o nome',
                    value: "Mauro",
                    returnKeyType: 'next',
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    onSubmitEditing: () => this._form.getComponent('email').refs.input.focus(),
                },
                email: {
                    label: '',
                    placeholder: 'E-mail',
                    returnKeyType: 'next',
                    textContentType: 'emailAddress',
                    keyboardType: 'email-address',
                    autoCapitalize: 'none',
                    error: 'E-mail inválido',
                    editable: false,
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    onSubmitEditing: () => this._form.getComponent('celular').refs.input.refs.input
                        .focus(),
                },
                celular: {
                    label: '',
                    placeholder: 'Celular',
                    error: 'Preencha o celular',
                    template: inputCell,
                    keyboardType: 'numeric',
                    returnKeyType: 'next',
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    onSubmitEditing: () => this._form.getComponent('cidade').refs.input.focus(),
                },
                cidade: {
                    label: '',
                    placeholder: 'Cidade',
                    error: 'Preencha a cidade',
                    returnKeyType: 'next',
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    onSubmitEditing: () => this._form.getComponent('estado').refs.input.focus(),
                },
                estado: {
                    label: '',
                    placeholder: 'Estado',
                    error: 'Preencha o estado',
                    returnKeyType: 'next',
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    onSubmitEditing: () => this._form.getComponent('senha').refs.input
                        .refs.input
                        .focus(),
                },
                senha: {
                    label: '',
                    placeholder: 'Senha',
                    template: inputShowPasswordTemplate,
                    error: 'Preencha a senha',
                    returnKeyType: 'next',
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    onSubmitEditing: () => this._form.getComponent('confirmacaoSenha').refs.input
                        .refs.input
                        .focus(),
                },
                confirmacaoSenha: {
                    label: '',
                    placeholder: 'Confirmar senha',
                    template: inputShowPasswordTemplate,
                    error: 'Confirmação incorreta',
                    returnKeyType: 'go',
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    onSubmitEditing: () => this.atualizar(),
                },
            },
            stylesheet: {
                ...Form.stylesheet,
                ...defaultFormStyles,
            },
            auto: 'none'
        };

        this.validate = null;
    }



    createFormData = (photo, body) => {
      const data = new FormData();
    
      data.append("file", {
        name: photo.fileName,
        type: 'image/jpeg', // This is important for Android!!
        uri:
          Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
      });
    
      Object.keys(body).forEach(key => {
        data.append(key, body[key]);
      });
    
      //console.log(data);
      return data;
    };

    showImagePicker(){
        Alert.alert(
          'Enviar Redação',
          'Escolha como quer enviar sua redação',
          [
            {
              text: 'Escolher na Galeria',
              onPress: () => this.chooseFileFromGallery()
            },
            { text: 'Tirar Foto',
              onPress: () => this.chooseFileFromCam()
            },
            {
              text: 'Cancelar',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel'
            }
          ],
          { cancelable: false }
        );
    }
  
    handleUploadPhoto = () => {
      let url = env.api_url + "usuarios/envia_foto.json";
      let usuid = this.state.user.id;
      this.setState({ logging: true });
  
      fetch(url, {
        method: "POST",
        header:{
          'Content-Type': 'multipart/form-data',
        },
        body: this.createFormData(this.state.photo, { usuid: usuid })
      })
      .then(response => response.json())
      .then(response => {
        console.log(response.info, response.menssagem);
        //alert("Upload success!");
        let title = response.info=="0" ? "Atenção!" : "Sucesso!";
        //this.getAlert(title, response.menssagem);
        this.atualizar();
        this.setState({ logging: false });
      })
      .catch(error => {
        console.log("upload error", error);
        //alert("Upload failed!");
        this.getAlert("Erro!", error);
        this.setState({ isLoading: false })
      });
    };
  
    chooseFile = () => {
      var options = {
        title: 'Escolha a Imagem',
        cancelButtonTitle: 'Cancelar',
        takePhotoButtonTitle: 'Tirar Foto...',
        chooseFromLibraryButtonTitle: 'Escolher na Galeria',
        storageOptions: {
          skipBackup: true,
          path: 'images'
        }
      };
      ImagePicker.showImagePicker(options, response => {
        console.log('Response = ', response);
  
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
          alert(response.customButton);
        } else {
          if (response.uri) {
            this.setState({ isLoading: true, photo: response },
              () => this.handleUploadPhoto()
            );
          }
        }
      });
    };

    chooseFileFromCam = () => {
      var options = {
        title: 'Escolha a Imagem',
        cancelButtonTitle: 'Cancelar',
        takePhotoButtonTitle: 'Tirar Foto...',
        chooseFromLibraryButtonTitle: 'Escolher na Galeria',
        storageOptions: {
          skipBackup: true,
          path: 'images'
        }
      };
        ImagePicker.showImagePicker(options, response => {
        //launchImageLibrary(options, response => {
        //launchCamera(options, response => {
        //console.log('Response = ', response);
  
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
          alert(response.customButton);
        } else {
          if (response.uri) {
            console.log(response);
            if(Platform.OS === 'android' || response.fileName == undefined){
              this.setState({ isLoading: true, photo: response },
                () => this.handleUploadPhoto()
              );
            }else{
              if(response.fileName.split('.').pop()=='heic' || response.fileName.split('.').pop()=='HEIC'){
                RNHeicConverter
                .convert({ // options
                    path: response.origURL,
                })
                .then((result) => {
                    console.log(result); // { success: true, path: "path/to/jpg", error, base64, }
                    this.setState({ isLoading: true, photo: result },
                      () => this.handleUploadPhoto()
                    );
                });
              }else{
                console.log(response.uri);
                this.setState({ isLoading: true, photo: response },
                  () => this.handleUploadPhoto()
                );
              }
            }
          }
        }
      });
    };
  
    chooseFileFromGallery = () => {
      var options = {
        title: 'Escolha a Imagem',
        cancelButtonTitle: 'Cancelar',
        takePhotoButtonTitle: 'Tirar Foto...',
        chooseFromLibraryButtonTitle: 'Escolher na Galeria',
        storageOptions: {
          skipBackup: true,
          path: 'images'
        }
      };
      //ImagePicker.showImagePicker(options, response => {
        launchImageLibrary(options, response => {
        //launchCamera(options, response => {
        //console.log('Response = ', response);
  
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
          alert(response.customButton);
        } else {
          if (response.uri) {
            console.log(response);
            if(Platform.OS === 'android' || response.fileName == undefined){
              this.setState({ isLoading: true, photo: response },
                () => this.handleUploadPhoto()
              );
            }else{
              if(response.fileName.split('.').pop()=='heic' || response.fileName.split('.').pop()=='HEIC'){
                RNHeicConverter
                .convert({ // options
                    path: response.origURL,
                })
                .then((result) => {
                    console.log(result); // { success: true, path: "path/to/jpg", error, base64, }
                    this.setState({ isLoading: true, photo: result },
                      () => this.handleUploadPhoto()
                    );
                });
              }else{
                console.log(response.uri);
                this.setState({ isLoading: true, photo: response },
                  () => this.handleUploadPhoto()
                );
              }
            }
          }
        }
      });
    };

    onChange(user) {
        this.setState({ user });
        if (user.confirmacaoSenha != null && user.confirmacaoSenha != '') {
            var values = this._form.getValue();
            var result = this._form.validate(values, this.User);
            this.validate = result.isValid();
        }
    };

    componentWillMount(){
        var user = retrieveItem('horadaredacao.user');
        
        user.then(u => {
          if(u==null){
            Actions.login({ type: 'replace' });
          }else{
            this.setState({
                user: {
                    id: u._id.$oid,
                    nome: u.Nome,
                    email: u.Email,
                    cidade: u.Cidade,
                    celular: u.WhatsUp,
                    estado: u.Estado,
                    senha: '',
                    confirmacaoSenha: '',
                    fullImage: u.full_imagem,
                    imagem_file_name: u.imagem_file_name
                }
            });
            console.log(u.full_imagem);
          }
        });
    }

    getAlert(title, msg){
      Alert.alert(
        title,
        msg,
        [
            {
                text: 'OK',
                onPress: () => {
                }
            },
        ],
        { cancelable: false }
      );
    }

    atualizar() {

        var values = this._form.getValue();
        var result = this._form.validate(values, this.User);

        if (result.isValid()) {
            this.setState({ logging: true });

            console.log('sou valido');
            let data = {
                'email': values.email,
                'nome': values.nome,
                'cidade': values.cidade,
                'estado': values.estado,
                'celular': values.celular,
                'password': values.senha,
            };

            Auth.atualizar(data)
                .then(response => {
                    if(response === true){
                        //Actions.drawer({ type: 'reset' });
                        Alert.alert(
                            'PARABÉNS!',
                            'Seus dados foram atualizados com sucesso.',
                            [
                                { text: 'OK', onPress: () => Actions.home()},
                            ],
                            { cancelable: false }
                        );
                    }
                })
                .catch(error => {
                    console.log(error);
                    // alert(error);
                    Alert.alert(
                        'ERRO',
                        'Ocorreu um erro ao atualizar seus dados. Por favor tente novamente mais tarde.',
                        [
                            { text: 'OK', onPress: () => console.log('OK Pressed') },
                        ],
                        { cancelable: false }
                    );
                    this.setState({ logging: false });
                });
        } else {
            //alert('Verifique as informações e tente novamente');
            console.log(result.firstError().message);
        }
    }
 
    render(){
        return(
            <View style={styles.container}>
                   <ScrollView
            style={styles.scroll_view}
            contentContainerStyle={styles.contentContainer}
            >
                        {!this.state.logging && (
                        <TouchableWithoutFeedback onPress={() => this.chooseFile()}>
                            <View>
                                <Image
                                source={
                                    (this.state.user.fullImage === '' || this.state.user.imagem_file_name === null || this.state.user.imagem_file_name === undefined)
                                    ? require('../assets/images/no-photo.png')
                                    : {uri: this.state.user.fullImage}
                                }
                                style={styles.logo}/>
                                <Text style={styles.textFoto}>Toque para alterar a foto</Text>
                            </View>
                        </TouchableWithoutFeedback>
                        )}
                        {this.state.logging && (
                            <ActivityIndicator size="large" color="#FFFFFF" style={styles.loader} />
                        )}
                        <Text style={styles.title}>Alterar Dados</Text>
                        <View style={styles.fields}>
                            <Form
                                type={this.User}
                                options={this.formOptions}
                                ref={c => this._form = c}
                                value={this.state.user}
                                onChange={(v) => this.onChange(v)}
                            />
                        </View>
                        
                        {this.state.logging && (
                            <ActivityIndicator size="large" color="#FFFFFF" style={styles.loader} />
                        )}
                        {!this.state.logging && (
                        <TouchableOpacity style={styles.button} onPress={() => this.atualizar()}>
                            <Text style={styles.buttonText}>Salvar</Text>
                        </TouchableOpacity>
                        )}
                        </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.defaultBackground,
    },
    contentContainer: {
      paddingTop: 0,
      paddingBottom: 20
    },
    imgBack: {
        flex: 1,
    },
    loader:{
      padding:20,
    },
    logo: {
        width: 100,
        height: 100,
        marginTop: 20,
        marginBottom: 10,
        alignSelf: 'center'
    },
    title: {
        fontFamily: "Quicksand-Bold",
        fontSize: 25,
        lineHeight: 25,
        textAlign: "left",
        color: colors.defaultText,
        textTransform: 'uppercase',
        marginBottom: 21,
        alignSelf: 'center'
    },
    textFoto: {
        fontFamily: "Quicksand-Bold",
        fontSize: 12,
        lineHeight: 12,
        textAlign: "left",
        color: colors.defaultText,
        textTransform: 'uppercase',
        marginBottom: 21,
        alignSelf: 'center'
    },
    fields: {
        width: 316,
        alignSelf: 'center',
    },
    button: {
        width: 310,
        height: 48.5,
        backgroundColor: colors.primary,
        marginTop: 35.5,
        alignSelf: 'center',
        borderRadius: 30
    },
    buttonText: {
        fontFamily: "Quicksand-Bold",
        fontSize: 18,
        lineHeight: 23,
        textAlign: "center",
        color: "#fefdfc",
        textTransform: 'uppercase',
        marginTop: 14
    },
    form: {

    },
    accountCheck: {
        fontFamily: "Quicksand-Regular",
        fontSize: 14,
        lineHeight: 18,
        letterSpacing: 0,
        color: colors.defaultText,
        alignSelf: 'center',
        marginBottom: 89
    },
    login: {
        fontWeight: 'bold'
    },
    just: {
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: 26
    }
});