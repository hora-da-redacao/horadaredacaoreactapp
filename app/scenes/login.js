import React, {Component} from 'react';
import {
    View,
    Modal,
    TextInput,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ImageBackground,
    Image,
    ScrollView,
    ActivityIndicator,
    StyleSheet,
    Alert
} from 'react-native';

import OneSignal from 'react-native-onesignal'; 
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Importando Objeto de Autenticação
import Auth from '../auth/auth';

import { Actions } from 'react-native-router-flux';
import t from 'tcomb-form-native';
import { defaultFormStyles, mainStyles } from '../styles/main';
import colors from '../constants/Colors';
import inputShowPasswordTemplate from '../component/input-show-password';

const Form = t.form.Form;

const Email = t.refinement(t.String, email => {
    const reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/; //or any other regexp
    return reg.test(email);
});

export default class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            logging: false,
            modalVisible: false,
            modalVisibleSenha: false,
            recEmail: '',
            passwordHidden: true,
            user: {},
            player_id: null
        };

        this.User = t.struct({
            username: Email,
            password: t.String,
        });

        this.formOptions = {
            fields: {
                username: {
                    label: '',
                    placeholder: 'E-mail',
                    error: 'Campo obrigatório',
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    returnKeyType: 'next',
                    textContentType: 'emailAddress',
                    //autoFocus: true,
                    keyboardType: 'email-address',
                    autoCapitalize: 'none',
                    onSubmitEditing: () => this._form.getComponent('password')
                        .refs
                        .input
                        .refs
                        .input
                        .focus(),
                },
                password: {
                    label: '',
                    placeholder: 'Senha',
                    template: inputShowPasswordTemplate,
                    error: 'Campo obrigatório',
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    onSubmitEditing: () => this.login(),
                    returnKeyType: 'go',
                },
            },
            stylesheet: {
                ...Form.stylesheet,
                ...defaultFormStyles,
            },
            auto: 'none'
        };

    }

    componentDidMount(){
        //Actions.CompreModal();
        OneSignal.init("c52b9e72-62c6-41b7-b1ca-a6a6929b7c17", {
            kOSSettingsKeyAutoPrompt: true,
        });
        OneSignal.getPermissionSubscriptionState( (status) => {
            let userID = status.userId;
            this.setState({player_id: userID});
            //alert(userID);
        });
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    setModalVisibleSenha(visible) {
        this.setState({ modalVisibleSenha: visible });
    }

    esqueciSenha() {
        const reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

        if (reg.test(this.state.recEmail)) {
            //this.setModalVisibleSenha(false);
            this.setModalVisible(true);
            Auth.rememberPassword(this.state.recEmail).then(
                result => {
                    console.log(result);
                    // alert('Um email com sua senha foi enviado para o email informado');
                    if(result.info == 1){
                        Alert.alert(
                            "RECUPERAR SENHA",
                            "Um e-mail com sua senha foi enviado para o e-mail informado",
                            [
                                {
                                    text: "OK",
                                    onPress: () => this.setModalVisibleSenha(false)
                                }
                            ],
                            { cancelable: false }
                        );
                    }else{
                        Alert.alert(
                            "RECUPERAR SENHA",
                            "Usuário não encontrado. Tente novamente.",
                            [
                                {
                                    text: "OK",
                                    onPress: () => this.setModalVisibleSenha(true)
                                }
                            ],
                            { cancelable: false }
                        );
                    }
                },
                errorCode => {
                    console.log(errorCode);
                    // alert(errorCode);
                    Alert.alert(
                        "RECUPERAR SENHA",
                        errorCode,
                        [
                            {
                                text: "OK",
                                onPress: () => console.log("OK Pressed")
                            }
                        ],
                        {
                            cancelable: false
                        }
                    );
                    this.setModalVisible(false);
                    this.setModalVisibleSenha(false);
                }
            );
        } else {
            // alert('Preencha o email corretamente');
            Alert.alert(
                "RECUPERAR SENHA",
                "Preencha o email corretamente",
                [{ text: "OK", onPress: () => console.log("OK Pressed") }],
                {
                    cancelable: false
                }
            );
        }
    }

    login() {
        var values = this._form.getValue();
        var result = this._form.validate(values, this.User);

        if (result.isValid()) {
            this.setState({ logging: true });

            console.log('sou valido');
            //this.setModalVisible(true);
            Auth.login(values, this.state.player_id)
            .then(response => {
                //console.log(response);
                if(response === true){
                    //Actions.drawer({ type: 'reset' });
                    Actions.home({ type: 'replace' });
                }
                //this.setModalVisible(false);
            })
            .catch(error => {
                //this.setModalVisible(false);
                console.log(error);
                this.setState({ logging: false });

                Alert.alert(
                    'FALHA NO LOGIN',
                    'Usuário ou senha inválidos',
                    [
                        {
                            text: 'OK',
                            onPress: () => {
                                this.setState({ user: { username: values.username } });
                                setTimeout(() => {
                                    this._form.getComponent('password')
                                        .refs
                                        .input
                                        .refs
                                        .input
                                        .focus();
                                }, 500);
                            }
                        },
                    ],
                    { cancelable: false }
                );
            });
        } else {
            //alert('Verifique as informações e tente novamente');
            console.log(result.firstError().message);
        }
    }
 
    render(){
        return(
            <View style={styles.container}>
                <ImageBackground source={require('../assets/images/drawable-hdpi/imagem3.png')} style={styles.imgBack}>
                    <ScrollView keyboardShouldPersistTaps="always">
                            <Image source={require('../assets/images/logo-hredacao.png')} style={styles.logo}/>
                            <View style={styles.fields}>
                                <Form
                                    type={this.User}
                                    options={this.formOptions}
                                    ref={c => this._form = c}
                                    value={this.state.user}
                                    onChange={(user) => this.setState({ user })}
                                />
                            </View>
                            {this.state.logging && (
                                <ActivityIndicator size="large" color="#107ae8" style={styles.loader} />
                            )}
                            {!this.state.logging && (
                            <View>
                                <TouchableOpacity style={styles.button}><Text style={styles.buttonText} onPress={() => this.login()}>entrar</Text></TouchableOpacity>
                                <TouchableOpacity style={styles.button}><Text style={styles.buttonText} onPress={() => {Actions.cadastro()}}>cadastrar</Text></TouchableOpacity>
                            </View>
                            )}
                            <View style={styles.just}>
                                <Text style={styles.accountCheck}>Esqueceu a senha?</Text>
                                <TouchableWithoutFeedback onPress={() => this.setModalVisibleSenha(true)}><Text style={[styles.accountCheck, styles.login]}> Clique aqui.</Text></TouchableWithoutFeedback>
                            </View>
                    
                            <Modal
                        transparent
                        animationType="fade"
                        onRequestClose={() =>
                            this.setState({ modalVisibleSenha: false })
                        }
                        onBackButtonPress={() =>
                            this.setState({ modalVisibleSenha: false })
                        }
                        visible={this.state.modalVisibleSenha}
                    >
                        <View style={mainStyles.modalContent}>
                            <View style={mainStyles.modalHeader}>
                                <Text style={mainStyles.modalTitle}>
                                    INFORME O EMAIL PARA RECUPERAR SUA SENHA
                                </Text>
                                <Icon
                                    style={mainStyles.modalClose}
                                    name="close"
                                    onPress={() =>
                                        this.setState({
                                            modalVisibleSenha: false
                                        })
                                    }
                                />
                            </View>
                            <View style={mainStyles.modalBody}>
                                <TextInput
                                    keyboardType="email-address"
                                    placeholder="Digite seu e-mail"
                                    style={defaultFormStyles.textbox}
                                    underlineColorAndroid="rgba(0,0,0,0)"
                                    placeholderTextColor={defaultFormStyles.placeholderColor}
                                    onChangeText={recEmail =>
                                        this.setState({ recEmail })
                                    }
                                    autoFocus
                                />
                                <View
                                        style={
                                            {paddingHorizontal: 100}
                                        }>
                                    <TouchableWithoutFeedback
                                        onPress={() => this.esqueciSenha()}
                                    >
                                        <View
                                            style={[styles.button, {marginHorizontal: 20}]}
                                        >
                                            <Text
                                                style={
                                                    styles.buttonText
                                                }
                                            >
                                                ENVIAR
                                            </Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </ScrollView>
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    imgBack: {
        flex: 1,
    },
    loader:{
      padding:20,
    },
    logo: {
        width: 157,
        height: 157,
        marginTop: 43,
        marginBottom: 25,
        alignSelf: 'center'
    },
    fields: {
        width: 316,
        alignSelf: 'center',
        marginBottom: 10
    },
    button: {
        width: 310,
        height: 48.5,
        backgroundColor: '#107ae8',
        marginTop: 20,
        alignSelf: 'center',
        borderRadius: 30
    },
    buttonText: {
        fontFamily: "Quicksand-Bold",
        fontSize: 18,
        lineHeight: 23,
        textAlign: "center",
        color: "#fefdfc",
        textTransform: 'uppercase',
        marginTop: 14
    },
    form: {

    },
    accountCheck: {
        fontFamily: "Quicksand-Regular",
        fontSize: 14,
        lineHeight: 18,
        letterSpacing: 0,
        color: "#094e96",
        alignSelf: 'center',
        marginBottom: 89
    },
    login: {
        fontWeight: 'bold'
    },
    just: {
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: 26
    }
});