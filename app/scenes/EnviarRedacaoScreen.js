import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  AlertIOS
} from 'react-native';


import CameraScreen from './camera-kit/CameraScreen';
import GalleryScreen from './camera-kit/GalleryScreen';

export default class EnviarRedacaoScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
          example: undefined,
          correcoes: []
        };
    }
    render() {
        /* if (this.state.example) {
            const Example = this.state.example;
            return <Example />;
        } */
        return (
            <GalleryScreen/>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    developmentModeText: {
        marginBottom: 20,
        color: 'rgba(0,0,0,0.4)',
        fontSize: 14,
        lineHeight: 19,
        textAlign: 'center',
    },
    contentContainer: {
    },
    headerContainer: {
        flexDirection: 'column',
        backgroundColor: '#F5FCFF',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 100
    },
    headerText: {
        color: 'black',
        fontSize: 24
    },
    buttonText: {
        color: 'blue',
        marginBottom: 20,
        fontSize: 20
    }
});
