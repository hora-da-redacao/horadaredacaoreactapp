import React from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Orientation from 'react-native-orientation';

import Pdf from 'react-native-pdf';

import {retrieveItem} from '../store/store';
import Providers from '../providers/Providers';

export default class PDFViewScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isAuthenticated: false,
        };
    }

    componentDidMount(){
        var user = retrieveItem('horadaredacao.user');
        user.then(u => {
            if(u==null){
                Actions.login({ type: 'replace' });
            }else{
                console.log(u);
                let id = u._id.$oid;
                let values = { tipo: 'ppt', userid: id }
                Providers.setstatistic(values)
                .then(response => {
                    console.log("[GetStats - Success]")
                    console.log(response);
                })
                .catch(error => {
                    console.log("GetStats - Error")
                    //this.setModalVisible(false);
                    console.log(error);
                });
            }
        });
    }

    DefinindoOrientacao() {
        //Orientation.lockToLandscape();
    }

    componentWillUnmount() {
        //Orientation.lockToPortrait();
    }
    
    render() {
        const source = {uri:this.props.pdf,cache:true};
        //const source = require('./test.pdf');  // ios only
        //const source = {uri:'bundle-assets://test.pdf'};

        //const source = {uri:'file:///sdcard/test.pdf'};
        //const source = {uri:"data:application/pdf;base64,..."};

        this.DefinindoOrientacao();
        return (
            <View style={styles.container}>
                <Pdf
                    source={source}
                    onLoadComplete={(numberOfPages,filePath)=>{
                        console.log(`number of pages: ${numberOfPages}`);
                    }}
                    onPageChanged={(page,numberOfPages)=>{
                        console.log(`current page: ${page}`);
                    }}
                    onError={(error)=>{
                        console.log(error);
                    }}
                    style={styles.pdf}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,
    },
    pdf: {
        flex:1,
        width: wp('96%')
        //width: Dimensions.get('window').width,
    }
});