import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ImageBackground,
    Image,
    Animated, 
    StyleSheet
} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';
import { Actions } from 'react-native-router-flux';

export default class ComeceGratis extends Component {

    constructor(props) {
      super(props);
  
      this.state = {
        opacity: new Animated.Value(0),
        products: null
      };
    }  

    closeModal = () => {
      Actions.pop();
    };

    render(){
        return(
            <View style={styles.container}>
                <ImageBackground source={require('../assets/images/drawable-hdpi/imagem2.png')} style={styles.imgBack}>
                    <View style={{ alignItems: 'flex-end', right: 10}}>
                        <TouchableWithoutFeedback style={{ position: 'absolute', backgroundColor: '#FFF', paddingHorizontal: 26, marginHorizontal: 26, width: 90}} onPress={this.closeModal}>
                            <View style={styles.iconClose}>
                                <Icon name='x' size={18} style={styles.statsIcon} />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                    <View style={styles.just}>
                        <View style={styles.plano}>
                            <Icon name="star" size={23} color="#107ae8" style={styles.star}/>
                            <Text style={styles.planoText}>plano prata</Text>
                        </View>
                        <Text style={styles.title}>comece grátis</Text>
                        <View style={styles.exp}>
                            <Text style={styles.expText}>Experimente 7 dias grátis</Text>
                        </View>
                        <View style={styles.list}>
                            <View style={styles.listItem}>
                                <Icon name="check-circle" size={15} color="#fff" style={styles.check}/>
                                <Text style={styles.listText}>Todos os Jogos</Text>
                            </View>
                            <View style={styles.listItem}>
                                <Icon name="check-circle" size={15} color="#fff" style={styles.check}/>
                                <Text style={styles.listText}>Todos os Temas em Vídeo</Text>
                            </View>
                            <View style={styles.listItem}>
                                <Icon name="check-circle" size={15} color="#fff" style={styles.check}/>
                                <Text style={styles.listText}>Todos os Esquemas em Vídeo</Text>
                            </View>
                            <View style={styles.listItem}>
                                <Icon name="check-circle" size={15} color="#fff" style={styles.check}/>
                                <Text style={styles.listText}>Todos os Podcasts</Text>
                            </View>
                            <View style={styles.listItem}>
                                <Icon name="check-circle" size={15} color="#fff" style={styles.check}/>
                                <Text style={styles.listText}>Todos os Ebooks</Text>
                            </View>
                            <View style={styles.listItem}>
                                <Icon name="check-circle" size={15} color="#fff" style={styles.check}/>
                                <Text style={styles.listText}>Todas as Aulas Exclusivas</Text>
                            </View>
                            <View style={styles.listItem}>
                                <Icon name="check-circle" size={15} color="#fff" style={styles.check}/>
                                <Text style={styles.listText}>Correção de Redação</Text>
                            </View>
                            {/* <View style={styles.listItem}>
                                <Icon name="check-circle" size={15} color="#fff" style={styles.check}/>
                                <Text style={styles.listText}>Monitoria Online</Text>
                            </View> */}
                        </View>
                    </View>
                    <TouchableOpacity style={styles.button} onPress={() => {Actions.cadastro()}}>
                        <Text style={styles.buttonText}>criar conta</Text>
                    </TouchableOpacity>
                    <Text style={styles.text}>7 dias de graça, depois R$ 8,90 / mês pagos anualmente.{"\n"}Cancele a qualquer momento</Text>
                    <View style={styles.subTexts}>
                        <TouchableWithoutFeedback style={styles.terms}>
                            <Text style={styles.termsText}>Termos e Condições</Text>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback style={styles.pol}>
                            <Text style={styles.polText}>Política de Privacidade</Text>
                        </TouchableWithoutFeedback>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(0,90,215,0.8)',
    },
    iconClose: {
        opacity: 1,
        backgroundColor: "#0A5CAF",
        justifyContent: "center",
        alignItems: "center",
        width: 40,
        height: 40,
        top: 10,
        right: 0,
        borderRadius: 20
    },
    statsIcon: {
        color: "#FFFFFF",
        alignSelf: "center"
    },
    imgBack: {
        flex: 1,
    },
    just: {
        width: "100%",
        marginHorizontal: 30
    },
    plano: {
        width: 150,
        height: 34,
        borderRadius: 4,
        backgroundColor: "#ffffff",
        marginTop: 40,
        flexDirection: "row"
    },
    star: {
        marginTop: 5,
        marginLeft: 3
    },
    planoText: {
        fontFamily: "Quicksand-Bold",
        fontSize: 16,
        textAlign: "left",
        color: "#107ae8",
        textTransform: 'uppercase',
        marginVertical: 9,
        marginLeft: 4
    },
    title: {
        fontFamily: "Quicksand-Regular",
        fontSize: 25,
        lineHeight: 31,
        textAlign: "left",
        color: "#ffffff",
        marginTop: 25,
        textTransform: 'uppercase'
    },
    exp: {
        width: 177,
        height: 28,
        borderRadius: 16,
        backgroundColor: "#094e96",
        marginTop: 24
    },
    expText: {
        fontFamily: "Quicksand-Regular",
        fontSize: 12,
        textAlign: "center",
        color: "#ffffff",
        marginVertical: 8
    },
    list: {
        marginTop: 25,
        marginBottom: 44
    },
    listItem: {
        flexDirection: 'row',
        marginBottom: 5
    },
    check: {
        marginRight: 5
    },
    listText: {
        fontFamily: "Quicksand-Regular",
        fontSize: 16,
        lineHeight: 20,
        textAlign: "left",
        color: "#ffffff"
    },
    button: {
        width: 300,
        height: 47,
        backgroundColor: "#ffffff",
        borderRadius: 20,
        alignSelf: 'center',
        marginBottom: 13
    },
    buttonText: {
        fontFamily: "Quicksand-Bold",
        fontSize: 20,
        textAlign: "center",
        color: "#107ae8",
        marginVertical: 13,
        textTransform: 'uppercase'
    },
    text: {
        fontFamily: "Quicksand-Regular",
        fontSize: 12,
        lineHeight: 15,
        textAlign: "center",
        color: "#ffffff",
        alignSelf: "center"
    },
    subTexts: {
        flexDirection: "row",
        justifyContent: 'space-around',
        marginBottom: 34,
        marginTop: 36,
        width: 316,
        alignSelf: 'center'
    },
    terms: {fontFamily: "Quicksand-Bold",},
    termsText: {
        fontFamily: "Quicksand-Bold",
        fontSize: 12,
        lineHeight: 15,
        textAlign: "center",
        color: "#ffffff"
    },
    pol: {},
    polText: {
        fontFamily: "Quicksand-Bold",
        fontSize: 12,
        lineHeight: 15,
        textAlign: "center",
        color: "#ffffff"
    }
});