import React, { Component } from 'react';
import {
    ScrollView,
    StyleSheet,
    Text,
    View,
    TouchableWithoutFeedback,
    Image,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Actions as NavigationActions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import restApp from '../reducers/actions';

import Auth from '../auth/auth';
import Colors from '../constants/Colors';
import env from '../../env.json';

import {retrieveItem} from '../store/store';


class Drawer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isAuthenticated: false,
            user: null,
        };
    }

    componentDidMount(){
        this.getUser();
    };

    componentWillMount(){
        this.getUser();
    };

    getUser(){
        var user = retrieveItem('horadaredacao.user');
        user.then(u => {
            this.setState({
                user: u
            });
        });
    }

    

    logout() {
        //restApp(null);
        Auth.logout().then(response => {
            console.log(response);
            if (response) {
                console.log(this.state);
                this.setState({
                    user: null
                });
                //Actions.login({ type: 'replace' });
                Actions.reset('rootStack');
                // Actions.replace('prehome');
                // Actions.replace('prehome');
            }
        });
    }

    render() {
        var drawerItens = [];
        var cont = 0;
        //console.log('[Tipo Usuario]');
        if(this.state.user != null){
            console.log(this.state.user.tipo);
            env.menu_itens.forEach(element => {
                if(element.active == true && this.state.user.tipo!=3){
                    if(element.route=="Esquema"){
                        return;
                    }else if(element.route=="AulaEnem" || element.route=="AulaEnem2020" || element.route=="AulaUERJ"  || element.route=="AulaConcurso"  || element.route=="AulaMilitar" ){
                        return;
                    }else{
                        drawerItens.push(<TouchableWithoutFeedback key={cont} onPress={NavigationActions[element.route]}>
                            <View style={styles.button}>
                                <Text style={styles.buttonText}>{element.text}</Text>
                            </View>
                        </TouchableWithoutFeedback>);
                        cont ++;
                    }
                }else if(element.active == true && this.state.user.tipo==3){
                    drawerItens.push(<TouchableWithoutFeedback key={cont} onPress={NavigationActions[element.route]}>
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>{element.text}</Text>
                        </View>
                    </TouchableWithoutFeedback>);
                    cont ++;
                    return;
                }
            });
        }else{
            this.getUser();
        }
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.header}>
                        <Image
                            style={styles.logo}
                            source={require('../assets/images/logo-hredacao.png')}
                        />

                        {
                            this.state.isAuthenticated &&
                            <View style={styles.userInfo}>
                                <Text style={styles.userWelcome}>BEM-VINDO</Text>
                                <Text style={styles.userName}>{this.state.user.username}</Text>
                            </View>
                        }
                    </View>

                    <TouchableWithoutFeedback
                        onPress={() => {
                            Actions.home();
                        }}
                    >
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>HOME</Text>
                        </View>
                    </TouchableWithoutFeedback>
                    
                    {drawerItens}
                    
                    <TouchableWithoutFeedback
                    onPress={() => {
                        this.logout();
                    }}>
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>SAIR</Text>
                        </View>
                    </TouchableWithoutFeedback>

                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        padding: 20,
    },
    buttonText: {
        fontSize: 18,
        color: Colors.primary,
        alignSelf: 'flex-start',
        //padding: 10,
        fontFamily: 'Quicksand-Bold',
        textTransform: 'uppercase'
    },
    button: {
        //height: 40,
        //backgroundColor: Colors.primaryLight,
        //marginBottom: 10,
        justifyContent: 'center',
        paddingVertical: 10,
        alignSelf: 'stretch',
        borderBottomWidth: 1,
        borderBottomColor: Colors.primary,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        marginBottom: 20,
    },
    logo: {
        height: 110,
        resizeMode: 'contain',
        marginLeft: 10,
    },
    userInfo: {
        flex: 1,
    },
    userWelcome: {
        color: Colors.textInPrimary,
        fontFamily: 'Quicksand-Bold',
        fontSize: 8,
        textAlign: 'right',
    },
    userName: {
        color: Colors.textInPrimary,
        fontFamily: 'Quicksand-Bold',
        fontSize: 14,
        lineHeight: 16,
        textAlign: 'right',
    },
});

export default connect()(Drawer);
