import React from 'react';
import {
    StyleSheet,
    Dimensions,
    View,
    Text,
    ScrollView, 
    ActivityIndicator
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Orientation from 'react-native-orientation';

import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
  } from "react-native-chart-kit";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {retrieveItem} from '../store/store';
import Providers from '../providers/Providers';

import colors from '../constants/Colors';

const data = {
    labels: ["seg", "ter", "qua", "qui", "sex", "sab", "dom"],
    datasets: [
      {
        data: [20, 45, 28, 80, 99, 43]
      }
    ]
};

const chartConfig = {
    backgroundGradientFrom: "#FFFFFF",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#FFFFFF",
    backgroundGradientToOpacity: 0.5,
    decimalPlaces: 2, //optional, defaults to 2dp
    color: (opacity = 1) => `rgba(150, 150, 150, ${opacity})`,
    strokeWidth: 1, // optional, default 3
    barPercentage: 0.3,
    useShadowColorFromDataset: false, // optional
    paddingLeft: 0
};

export default class EstatisticaScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isAuthenticated: false,
            statsTotal: null,
            data: null,
            user: null,
            numRedacoes: 0,
            media: 0
        };
    }

    componentDidMount(){
        var user = retrieveItem('horadaredacao.user');
        user.then(u => {
            if(u==null){
                Actions.login({ type: 'replace' });
            }else{
                console.log("[GetStats - USER]")
                console.log(u);
                this.setState({user: u});
                let id = u._id.$oid;
                let values = { userid: id }
                Providers.getstatistics(values)
                .then(response => {
                    //console.log("[GetStats - Success]")
                    //console.log(response.stats.week.dataset.data);
                    let weekdata = {
                        labels: response.stats.week.labels,
                        datasets: [
                          {
                            data: response.stats.week.dataset.data
                          }
                        ]
                    }
                    this.setState({statsTotal:response.stats, data: weekdata});
                })
                .catch(error => {
                    console.log("GetStats - Error")
                    //this.setModalVisible(false);
                    console.log(error);
                });
                Providers.media_usuario(values)
                .then(response => {
                    //console.log("[GetStats - Success]")
                    //console.log(response.stats.week.dataset.data);
                    this.setState({numRedacoes:response.qtd_redacoes, media: response.media});
                })
                .catch(error => {
                    console.log("GetStats - Error")
                    //this.setModalVisible(false);
                    console.log(error);
                });
            }
        });
    }

    DefinindoOrientacao() {
        //Orientation.lockToLandscape();
    }

    componentWillUnmount() {
        //Orientation.lockToPortrait();
    }
    
    render() {
        const source = {uri:this.props.pdf,cache:true};
        //const source = require('./test.pdf');  // ios only
        //const source = {uri:'bundle-assets://test.pdf'};

        //const source = {uri:'file:///sdcard/test.pdf'};
        //const source = {uri:"data:application/pdf;base64,..."};

        this.DefinindoOrientacao();
        return (
            <ScrollView style={styles.container}
            contentContainerStyle={styles.contentContainer}>

                    <View style={{ height: 100, marginTop: 10, paddingBottom:10, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={styles.titulo}>Confira seu desempenho no Hora da Redação</Text>
                    </View>
                    
                    {this.state.statsTotal == null && (
                        <ActivityIndicator size="large" color="#FFFFFF" style={styles.loader} />
                    )}

                    {this.state.statsTotal != null && (
                        <View style={styles.boxStats}>
                            <View style={styles.boxStatsData}>
                                <Icon name='monitor' size={62} style={styles.statsIcon} />
                                <Text style={styles.boxStatsTitle}>Vídeos</Text>
                                <Text style={styles.boxStatsNumbers}>{this.state.statsTotal.videos}</Text>
                            </View>
                            <View style={styles.boxStatsData}>
                                <Icon name='presentation' size={62} style={styles.statsIcon} />
                                <Text style={styles.boxStatsTitle}>Textos</Text>
                                <Text style={styles.boxStatsNumbers}>{this.state.statsTotal.ppts}</Text>
                            </View>
                            <View style={styles.boxStatsData}>
                                <Icon name='headphones' size={62} style={styles.statsIcon} />
                                <Text style={styles.boxStatsTitle}>Podcasts</Text>
                                <Text style={styles.boxStatsNumbers}>{this.state.statsTotal.audios}</Text>
                            </View>
                        </View>
                    )}

                    {(this.state.user != null && this.state.statsTotal != null) && (
                        <View style={styles.boxStatsRow}>
                            <View style={styles.boxStatsJogo}>
                                <Icon name='file-document-edit-outline' size={62} style={styles.statsIcon} />
                                <Text style={styles.boxStatsTitle}>Você fez</Text>
                                <Text style={styles.boxStatsNumbers}>{this.state.numRedacoes}</Text>
                                <Text style={styles.boxStatsTitle}>Jogos de Redação</Text>
                            </View>
                            <View style={styles.boxStatsJogo}>
                                <Icon name='chart-line-variant' size={62} style={styles.statsIcon} />
                                <Text style={styles.boxStatsTitle}>Sua média é</Text>
                                <Text style={styles.boxStatsNumbers}>{this.state.media}</Text>
                                <Text style={styles.boxStatsTitle}>pontos</Text>
                            </View>
                        </View>
                    )}

                    {this.state.data != null && (
                        
                        <View style={styles.boxStatsColum}>
                            <View>
                                <Text style={styles.boxStatsTitleChart}>Conteúdos vistos/ouvidos nos últimos 7 dias</Text>
                            </View>
                            <View>
                                {/* <BarChart
                                    style={styles.graphStyle}
                                    data={this.state.data}
                                    width={wp('90%')}
                                    height={220}
                                    yAxisLabel=""
                                    chartConfig={chartConfig}
                                    verticalLabelRotation={30}
                                /> */}
                                <LineChart
                                data={this.state.data}
                                width={wp('90%')}
                                height={220}
                                yAxisLabel=""
                                yAxisSuffix=""
                                yAxisInterval={1} // optional, defaults to 1
                                segments={2}
                                chartConfig={{
                                backgroundColor: "#FFFFFF",
                                backgroundGradientFrom: "#FFFFFF",
                                backgroundGradientTo: "#FFFFFF",
                                decimalPlaces: 0, // optional, defaults to 2dp
                                color: (opacity = 1) => `rgba(150, 150, 150, ${opacity})`,
                                labelColor: (opacity = 1) => `rgba(150, 150, 150, ${opacity})`,
                                style: {
                                    borderRadius: 16
                                },
                                propsForDots: {
                                    r: "6",
                                    strokeWidth: "2",
                                    stroke: `rgba(150, 150, 150, 1)`
                                }
                                }}
                                bezier
                                style={styles.graphStyle}
                            />
                            </View>
                        </View>
                    )}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        /*padding: 20,*/
        backgroundColor: colors.darkBlue,
    },
    contentContainer:{
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingBottom: 30,
    },
    pdf: {
        flex:1,
        width: wp('96%')
        //width: Dimensions.get('window').width,
    },
    loader:{
      padding:20,
    },
    boxStatsColum:{
        elevation: 2,
        backgroundColor: '#fff',
        marginBottom: 20,
        alignItems: 'center',
        width: wp('90%'),
        padding: 20,
        borderRadius: 8
    },
    boxStats:{
        elevation: 2,
        backgroundColor: '#fff',
        flexDirection: 'row',
        marginBottom: 20,
        width: wp('90%'),
        padding: 20,
        borderRadius: 8
    },
    boxStatsData: {
        width: '33%',
        alignItems: 'center',
        height: 130,
        justifyContent: 'space-between'
    },
    boxStatsTitle: {
        color: '#222',
        fontSize: 14,
        fontFamily: 'Quicksand-Regular',
        textAlign: 'center'
    },
    boxStatsTitleChart: {
        color: '#222',
        fontSize: 16,
        fontFamily: 'Quicksand-Regular',
        textAlign: 'center',
        marginBottom: 15
    },
    boxStatsNumbers: {
        color: colors.primary,
        fontSize: 30,
        fontFamily: 'Quicksand-Bold'
    },
    statsIcon: {
        color: colors.lineGrey
    },
    graphStyle: {
        paddingLeft: 0
    },
    boxStatsJogo: {
        elevation: 2,
        backgroundColor: '#fff',
        alignItems: 'center',
        marginBottom: 20,
        width: wp('42%'),
        padding: 20,
        borderRadius: 8
    },
    boxStatsRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: wp('90%'),
    },
    titulo: {
      fontSize: 24,
      color: "#FFFFFF",
      fontFamily: "Quicksand-Bold",
      lineHeight: 38,
      textAlign: 'center',
      marginHorizontal: 20
    },
});