import React, { Component } from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  SafeAreaView,
  View,
  ImageBackground,
  FlatList
} from 'react-native';

import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import { Actions } from 'react-native-router-flux';

import ImageLoad from 'react-native-image-placeholder';

import { VideoThumb } from '../component/VideoThumb';
import {retrieveItem} from '../store/store';
import Providers from '../providers/Providers';
import { VideoGrid } from '../component/VideoGrid';

const columns = 2;

export default class AulaEnem2020Screen extends Component {
  constructor(props) {
    super(props);

    this.state = {
        user: {},
        videos: []  
    };
  }

  componentDidMount(){
    var user = retrieveItem('horadaredacao.user');
    user.then(u => {
      if(u==null){
        Actions.login({ type: 'replace' });
      }else{
        console.log(u);
        let id = u._id.$oid;
        let values = { categoria: '4', userid: id }
        Providers.videos(values)
        .then(response => {
          this.setState({
            videos: response
          });
          console.log(response);
        })
        .catch(error => {
            //this.setModalVisible(false);
            console.log(error);
        });
      }
    });
  };

  render() {
  return (
    <View style={styles.container}>
      <ParallaxScroll
            headerHeight={1}
            isHeaderFixed={false}
            parallaxHeight={1}
            renderParallaxBackground={({ animatedValue }) => <ImageBackground 
            style={{
              width: "100%",
              height: 250,
              backgroundColor:'##FFF',
              position: 'absolute',
              top:0
            }}
            imageStyle={{
              resizeMode: "cover",
              alignSelf: "flex-end"
            }}
            source={require('../assets/images/bgHome.png')}>
            </ImageBackground>}
            parallaxBackgroundScrollSpeed={5}
            parallaxForegroundScrollSpeed={2.5}
          >
          <ScrollView style={styles.contentContainer}>
            <View>
              <View style={{ height: 120,marginTop: 60, paddingBottom:20, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.titulo}>Aula ENEM</Text>
                <Text style={styles.descricao}>Muito mais conteúdo para você assistir sempre que precisar</Text>
              </View>
            <VideoGrid data={this.state.videos} columns={1}></VideoGrid>
            </View>
          </ScrollView>
      </ParallaxScroll>
    </View>
  )};
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    paddingHorizontal: 10,
  },
  titulo: {
    fontSize: 32,
    color: "#FFFFFF",
    fontFamily: "Quicksand-Bold",
    lineHeight: 38,
    textAlign: 'center',
  },
  descricao: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Quicksand-Regular",
    lineHeight: 24,
    textAlign: 'center',
    marginVertical: 10
  },
});
