import React from 'react';
import { StyleSheet, Dimensions, View, Text, ScrollView, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Orientation from 'react-native-orientation';

import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
  } from "react-native-chart-kit";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {retrieveItem} from '../store/store';
import Providers from '../providers/Providers';

import colors from '../constants/Colors';

export default class NotificacoesScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isAuthenticated: false,
            statsTotal: null,
            data: null,
            user: null,
            notificacoes: null
        };
    }

    componentDidMount(){
        var user = retrieveItem('horadaredacao.user');
        user.then(u => {
            if(u==null){
                Actions.login({ type: 'replace' });
            }else{
                console.log("[GetStats - USER]")
                console.log(u);
                this.setState({user: u});
                let id = u._id.$oid;
                let values = { userid: id }
                Providers.notificacoes(values)
                .then(response => {
                    //console.log("[GetStats - Success]")
                    //console.log(response.stats.week.dataset.data);

                    this.setState({notificacoes:response.notificacoes}, function () {this.setRead()});
                })
                .catch(error => {
                    console.log("GetStats - Error")
                    //this.setModalVisible(false);
                    console.log(error);
                });
            }
        });
    }

    DefinindoOrientacao() {
        //Orientation.lockToLandscape();
    }

    setRead() {
        //Orientation.lockToPortrait();
        var user = retrieveItem('horadaredacao.user');
        user.then(u => {
            if(u==null){
                Actions.login({ type: 'replace' });
            }else{
                console.log(u);
                this.setState({user: u});
                let id = u._id.$oid;
                let values = { userid: id }
                Providers.setread(values)
                .then(response => {
                    //console.log("[GetStats - Success]")
                    console.log(response);
                })
                .catch(error => {
                    console.log("GetStats - Error")
                    //this.setModalVisible(false);
                    console.log(error);
                });
            }
        });
    }
    
    render() {
        const source = {uri:this.props.pdf,cache:true};
        //const source = require('./test.pdf');  // ios only
        //const source = {uri:'bundle-assets://test.pdf'};

        //const source = {uri:'file:///sdcard/test.pdf'};
        //const source = {uri:"data:application/pdf;base64,..."};

        this.DefinindoOrientacao();
        return (
            <ScrollView style={styles.container}
            contentContainerStyle={styles.contentContainer}>
                    
                    {this.state.notificacoes != null && (
                        <FlatList
                        data={this.state.notificacoes}
                        keyExtractor={(item, index) => index}
                        renderItem={({ item }) => {
                            return (
                                <View style={styles.boxNotificacao}>
                                    <Text style={styles.boxStatsTitle}>{item.mensagem}</Text>
                                </View>
                            );
                        }}
                        />
                    )}

                    {this.state.notificacoes == null && (
                        <View style={{ height: 100, marginTop: 10, paddingBottom:10, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={styles.titulo}>Aguarde novas notificações</Text>
                        </View>
                    )}

            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: colors.darkBlue,
    },
    contentContainer:{
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingBottom: 30,
    },
    pdf: {
        flex:1,
        width: wp('96%')
        //width: Dimensions.get('window').width,
    },
    boxStatsColum:{
        elevation: 2,
        backgroundColor: '#fff',
        marginBottom: 20,
        alignItems: 'center',
        width: wp('90%'),
        padding: 20,
        borderRadius: 8
    },
    boxNotificacao:{
        elevation: 1,
        backgroundColor: 'rgba(255, 255, 255, 1)',
        flexDirection: 'row',
        marginBottom: 10,
        width: wp('90%'),
        padding: 20,
        borderRadius: 4
    },
    boxStatsTitle: {
        color: '#222',
        fontSize: 14,
        fontFamily: 'Quicksand-Regular',
        textAlign: 'center'
    },
    statsIcon: {
        color: colors.lineGrey
    },
    titulo: {
      fontSize: 24,
      color: "#FFFFFF",
      fontFamily: "Quicksand-Bold",
      lineHeight: 38,
      textAlign: 'center',
      marginHorizontal: 20
    },
});