import React from 'react'
import { Dimensions, StyleSheet, View, Image, Text, Slider, ImageBackground, ScrollView, TouchableOpacity, Platform, Alert} from 'react-native';

import Sound from 'react-native-sound';
import { Actions } from 'react-native-router-flux';

import ParallaxScroll from '@monterosa/react-native-parallax-scroll';

//https://github.com/oblador/react-native-vector-icons#icon-component
import Icon from 'react-native-vector-icons/AntDesign';

import {retrieveItem} from '../store/store';
import Providers from '../providers/Providers';


export default class PodcastPlayerScreen extends React.Component{

    static navigationOptions = props => ({
        //title: props.navigation.state.params.title,
        //title: this.props.audio.titulo,
        headerStyle: {
            backgroundColor: "#0E5AA7",
            shadowColor: 'transparent',
            shadowOpacity: 0,
        },
        headerTitleStyle: {
            alignSelf: 'center',
            marginRight: 56
        },
        headerLeft: <View
                        style={{marginLeft: 20}}> 
                        <Icon 
                        name= 'left'
                        size = {24} 
                        color = 'white'
                        onPress = { () => Actions.pop() }/>
                    </View>,
    })

    constructor(props) {
        super(props);
        this.state = {
            card:[],
            playState:'paused', //playing, paused
            playSeconds:0,
            duration:0
        }
        this.sliderEditing = false;
    }

    componentDidMount() {
        this.setState({trackTitle: this.props.audio.titulo});
        this.setState({ card: this.props.audio });
        this.play();
        
        this.timeout = setInterval(() => {
            if(this.sound && this.sound.isLoaded() && this.state.playState == 'playing' && !this.sliderEditing){
                this.sound.getCurrentTime((seconds, isPlaying) => {
                    this.setState({playSeconds:seconds});
                })
            }
        }, 100);
        //Actions.refresh({title: this.props.audio.title})
        var user = retrieveItem('horadaredacao.user');
        user.then(u => {
            if(u==null){
                Actions.login({ type: 'replace' });
            }else{
                console.log(u);
                let id = u._id.$oid;
                let values = { tipo: 'audio', userid: id }
                Providers.setstatistic(values)
                .then(response => {
                    console.log("[GetStats - Success]")
                    console.log(response);
                })
                .catch(error => {
                    console.log("GetStats - Error")
                    //this.setModalVisible(false);
                    console.log(error);
                });
            }
        });
    }
    componentWillUnmount(){
        if(this.sound){
            this.sound.release();
            this.sound = null;
        }
        if(this.timeout){
            clearInterval(this.timeout);
        }
    }

    onSliderEditStart = () => {
        this.sliderEditing = true;
    }
    onSliderEditEnd = () => {
        this.sliderEditing = false;
    }
    onSliderEditing = value => {
        if(this.sound){
            this.sound.setCurrentTime(value);
            this.setState({playSeconds:value});
        }
    }

    play = async () => {
        if(this.sound){
            this.sound.play(this.playComplete);
            this.setState({playState:'playing'});
        }else{
            //const filepath = this.props.navigation.state.params.filepath;
            const filepath = this.props.audio.arquivo;
            console.log('[Play]', filepath);
    
            //this.sound = new Sound(filepath, '', (error) => {
            this.sound = new Sound(filepath, Sound.MAIN_BUNDLE, (error) => {
                if (error) {
                    console.log('failed to load the sound', error);
                    Alert.alert('Notice', 'audio file error. ' + error.message);
                    this.setState({playState:'paused'});
                }else{
                    this.setState({playState:'playing', duration:this.sound.getDuration()});
                    this.sound.play(this.playComplete);
                }
            });    
        }
    }
    playComplete = (success) => {
        if(this.sound){
            if (success) {
                console.log('successfully finished playing');
            } else {
                console.log('playback failed due to audio decoding errors');
                //Alert.alert('Notice', 'audio file error. (Error code : 2)');
            }
            this.setState({playState:'paused', playSeconds:0});
            this.sound.setCurrentTime(0);
        }
    }

    pause = () => {
        if(this.sound){
            this.sound.pause();
        }

        this.setState({playState:'paused'});
    }

    jumpPrev15Seconds = () => {this.jumpSeconds(-15);}
    jumpNext15Seconds = () => {this.jumpSeconds(15);}
    jumpSeconds = (secsDelta) => {
        if(this.sound){
            this.sound.getCurrentTime((secs, isPlaying) => {
                let nextSecs = secs + secsDelta;
                if(nextSecs < 0) nextSecs = 0;
                else if(nextSecs > this.state.duration) nextSecs = this.state.duration;
                this.sound.setCurrentTime(nextSecs);
                this.setState({playSeconds:nextSecs});
            })
        }
    }

    getAudioTimeString(seconds){
        const h = parseInt(seconds/(60*60));
        const m = parseInt(seconds%(60*60)/60);
        const s = parseInt(seconds%60);

        //return ((h<10?'0'+h:h) + ':' + (m<10?'0'+m:m) + ':' + (s<10?'0'+s:s));
        return ((m<10?'0'+m:m) + ':' + (s<10?'0'+s:s));
    }

    render(){

        const currentTimeString = this.getAudioTimeString(this.state.playSeconds);
        const durationString = this.getAudioTimeString(this.state.duration);

        return (
            <View style={styles.container}>

                <ParallaxScroll
                        headerHeight={1}
                        isHeaderFixed={false}
                        parallaxHeight={1}
                        renderParallaxBackground={({ animatedValue }) => <ImageBackground 
                        style={{
                        width: "100%",
                        height: 250,
                        backgroundColor:'##FFF',
                        position: 'absolute',
                        top:0
                        }}
                        imageStyle={{
                        resizeMode: "cover",
                        alignSelf: "flex-end"
                        }}
                        source={require('../assets/images/bgHome.png')}>
                        </ImageBackground>}
                        parallaxBackgroundScrollSpeed={5}
                        parallaxForegroundScrollSpeed={2.5}
                    >
                    <ScrollView style={styles.contentContainer}>
                        <View>
                            <View style={{ height: 120,marginTop: 10, paddingBottom:20, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={styles.titulo}>{this.props.audio.titulo}</Text>
                            </View>
                            <Image
                            style={{
                                alignSelf: 'stretch',
                                height: 200,
                                marginHorizontal: 20,
                                borderRadius: 8
                            }}
                            source={{uri: this.props.audio.imagem}}
                            />
                            <View style={{flexDirection:'row', justifyContent:'center', marginTop:25}}>
                                <TouchableOpacity onPress={this.jumpPrev15Seconds} style={{justifyContent:'center'}}>
                                    <Icon name="banckward" size={30} color="#374B60" />
                                </TouchableOpacity>
                                {this.state.playState == 'playing' && 
                                <TouchableOpacity onPress={this.pause} style={{marginHorizontal:20}}>
                                    <Icon name="pause" size={30} color="#374B60" />
                                </TouchableOpacity>}
                                {this.state.playState == 'paused' && 
                                <TouchableOpacity onPress={this.play} style={{marginHorizontal:20}}>
                                    <Icon name="caretright" size={30} color="#374B60" />
                                </TouchableOpacity>}
                                <TouchableOpacity onPress={this.jumpNext15Seconds} style={{justifyContent:'center'}}>
                                    <Icon name="forward" size={30} color="#374B60" />
                                </TouchableOpacity>
                            </View>
                            <View style={{marginVertical:15, marginHorizontal:28, flexDirection:'row'}}>
                                <Text style={{color:'#333', alignSelf:'center'}}>{currentTimeString}</Text>
                                <Slider
                                    onTouchStart={this.onSliderEditStart}
                                    // onTouchMove={() => console.log('onTouchMove')}
                                    onTouchEnd={this.onSliderEditEnd}
                                    // onTouchEndCapture={() => console.log('onTouchEndCapture')}
                                    // onTouchCancel={() => console.log('onTouchCancel')}
                                    onValueChange={this.onSliderEditing}
                                    value={this.state.playSeconds} maximumValue={this.state.duration} maximumTrackTintColor='gray' minimumTrackTintColor='#333' thumbTintColor='#107AE8' 
                                    style={{flex:1, alignSelf:'center', marginHorizontal:Platform.select({ios:5})}}/>
                                <Text style={{color:'#333', alignSelf:'center'}}>{durationString}</Text>
                            </View>
                        </View>
                    </ScrollView>
                </ParallaxScroll>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    view: {
        flex:1,
        width:Dimensions.get('window').width,
    },
    controles:{
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: 200,
        margin: 3,
    },
    playeractions:{
        margin: 5
    },
    button: {
      fontSize: 20,
      backgroundColor: 'rgba(220,220,220,1)',
      borderRadius: 4,
      borderWidth: 1,
      borderColor: 'rgba(80,80,80,0.5)',
      overflow: 'hidden',
      padding: 7,
    },
    titulo: {
      fontSize: 26,
      color: "#FFFFFF",
      fontFamily: "Quicksand-Bold",
      lineHeight: 38,
      textAlign: 'center',
      marginHorizontal: 20
    },
    descricao: {
      fontSize: 20,
      color: "#FFFFFF",
      fontFamily: "Quicksand-Bold",
      lineHeight: 24,
      textAlign: 'center',
      marginVertical: 10
    }
});