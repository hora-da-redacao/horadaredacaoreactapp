
import React, { Component } from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  ImageBackground,
  View,
  FlatList
} from 'react-native';

import ParallaxScroll from '@monterosa/react-native-parallax-scroll';

import { ListItem } from 'react-native-elements'
import { Actions } from 'react-native-router-flux';

import {retrieveItem, storeItem} from '../store/store';
import Providers from '../providers/Providers';
import { jogoStyles } from '../styles/jogo';

import colors from '../constants/Colors'

export default class JogoScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
        user: {},
        jogo: []  
    };
  }
  
  componentDidMount(){

    storeItem("intro",[]);
    storeItem("desenv1",[]);
    storeItem("desenv2",[]);
    storeItem("conclusao",[]);

    var user = retrieveItem('horadaredacao.user');
    user.then(u => {
      if(u==null){
        Actions.login({ type: 'replace' });
      }else{
        let id = u._id.$oid;
        storeItem('categoriaJogo',1);
        let values = { categoria: '1', userid: id }
        Providers.jogo(values)
        .then(response => {
          this.setState({
            jogo: response
          });
        })
        .catch(error => {
            //this.setModalVisible(false);
            console.log(error);
        });
      }
    });
  };

  nav(item){
    if(Boolean(item.liberado) ){
      Actions.IntroScreen({ tema: item });
    }else{
      Actions.CompreModal();
    }
  }

  render() {
    return (
    <View style={jogoStyles.container}>
    <ParallaxScroll
          headerHeight={1}
          isHeaderFixed={false}
          parallaxHeight={1}
          renderParallaxBackground={({ animatedValue }) => <ImageBackground 
          style={{
            width: "100%",
            height: 250,
            backgroundColor:'##FFF',
            position: 'absolute',
            top:0
          }}
          imageStyle={{
            resizeMode: "cover",
            alignSelf: "flex-end"
          }}
          source={require('../assets/images/bgHome.png')}>
          </ImageBackground>}
          parallaxBackgroundScrollSpeed={5}
          parallaxForegroundScrollSpeed={2.5}
        >
        <ScrollView contentContainerStyle={jogoStyles.contentContainer}>
        <View>
            <View style={{ height: 150, padding:20, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={styles.titulo}>Jogos</Text>
              <Text style={styles.descricao}>Encare o desafio de corrigir uma redação. Leia cada parágrafo</Text>
            </View>
            <FlatList
            data={this.state.jogo}
            keyExtractor={(item, index) => index}
            renderItem={({ item, index }) => {
                return (
                  <ListItem
                  title={"Tema " + (index+1)}
                  subtitle={item.titulo_tema}
                  leftIcon={{ name: Boolean(item.liberado) ? 'library-books' : 'lock'}}
                  bottomDivider
                  chevron
                  onPress={() => this.nav(item)}
                  />
                );
            }}
            />
        </View>
        </ScrollView>
      </ParallaxScroll>
    </View>
  )};
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  titulo: {
    fontSize: 32,
    color: "#FFFFFF",
    fontFamily: "Quicksand-Bold",
    lineHeight: 38,
    textAlign: 'center',
  },
  descricao: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Quicksand-Regular",
    lineHeight: 24,
    textAlign: 'center',
    marginVertical: 10
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
