import React from "react";
import {
    View,
    Image,
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import { GiftedChat, Bubble, SystemMessage, Send } from "react-native-gifted-chat";
import { Dialogflow_V2 } from "react-native-dialogflow";
import Voice from 'react-native-voice';
import {retrieveItem} from '../store/store';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

//Importando configurações
import {
    private_key,
    client_email
} from '../../app.json';

import CustomView from '../component/customview';

//Styles
import { mainStyles } from '../styles/main';

export default class Chat extends React.Component {

  state = {
    messages: [],
    user:  {
      _id: 2,
      name: "Hória",
      avatar: require("../assets/images/horiaavatar.jpg")
    }
  };

  componentDidMount() {

    var user = retrieveItem('horadaredacao.user');
    user.then(u => {
      if(u==null){
        Actions.login({ type: 'replace' });
      }else{
        this.setState({
            messages: [{
              _id: 2,
              text: "O que deseja saber sobre redação?",
              createdAt: new Date(),
              user: this.state.user
            },
            {
              _id: 1,
              text: "Olá, "+u.Nome+" sou Hória, inteligência artificial, suas perguntas podem me ensinar redação. Eu sei um pouco, posso tirar suas dúvidas?",
              createdAt: new Date(),
              user: this.state.user
            },
          ]
        });

        this.renderBubble = this.renderBubble.bind(this);

        Dialogflow_V2.setConfiguration(
          client_email,
          private_key,
          Dialogflow_V2.LANG_PORTUGUESE_BRAZIL,
          'assistenteredacao'
        );
      }
    });
  }

  onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
    messages.forEach(message => {  
      //console.log(message.text);
      Dialogflow_V2.requestQuery(
        message.text, 
        result=>{
          //console.log(result);
          messages = [];
          cont = 0;
          result.queryResult.fulfillmentMessages.reverse().forEach(fulfillmentMessage => {
            let card = (fulfillmentMessage.card == undefined ? fulfillmentMessage.payload : fulfillmentMessage.card)
            item = {
              _id: result.responseId + '-' + cont,
              text: (fulfillmentMessage.text == undefined ? "" :fulfillmentMessage.text.text['0']),
              cardType: (fulfillmentMessage.payload != undefined ? card.cardType : result.queryResult.parameters.tipo_conteudo),
              card: (card == {} ? undefined : card),
              createdAt: new Date(),
              user: this.state.user
            }
            messages.push(item);
            console.log(item);
            cont++
          });
          
          this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
          }))
        }, 
        error=>console.log(error)
      );
    });
        
  }

  renderCustomView(props) {
    return (
      <CustomView
        {...props}
      />
    );
  }

  renderSend(props) {
    return (
        <Send
            {...props}
            style={{paddingBottom: 10}}
        >
          <View style={{marginRight: 10, marginBottom: 7,}}>
              <Icon name="send" size={30} style={mainStyles.sendIcon} />
          </View>
         
        </Send>
    );
  }

  renderBubble(props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: {
            backgroundColor: '#f1E7D9',
          }
        }}
      />
    );
  }

  render() {
    return <GiftedChat
      messages={this.state.messages}
      onSend={messages => this.onSend(messages)}
      placeholder="Digite aqui..."
      locale="pt-BR"
      renderBubble={this.renderBubble}
      renderSend={this.renderSend}
      renderCustomView={this.renderCustomView}
      user={{
        _id: 1,
      }}
    />;
  }
}