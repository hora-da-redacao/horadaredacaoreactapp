import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ImageBackground,
    Image,
    ScrollView,
    StyleSheet,
    Alert,
    ActivityIndicator
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import t from 'tcomb-form-native';
import { defaultFormStyles, mainStyles } from '../styles/main';
import { isValidCPF, isValidEmail } from '../component/tcomb-validators';
import inputShowPasswordTemplate from '../component/input-show-password';
import Auth from '../auth/auth';


const Form = t.form.Form;

export default class Cadastro extends Component {
    constructor(props) {
        super(props)

        this.state = {
            logging: false,
            user: {},
        };

        this.samePassword = t.refinement(t.String, (s) => {
            return s == this.state.user.senha;
        });

        this.Email = t.refinement(t.String, email => {
            return isValidEmail(email);
        });

        this.User = t.struct({
            nome: t.String,
            email: this.Email,
            senha: t.String,
            confirmacaoSenha: this.samePassword,
        });

        this.formOptions = {
            fields: {
                nome: {
                    label: '',
                    placeholder: 'Nome Completo',
                    error: 'Preencha o nome',
                    returnKeyType: 'next',
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    onSubmitEditing: () => this._form.getComponent('email').refs.input.focus(),
                },
                email: {
                    label: '',
                    placeholder: 'E-mail',
                    returnKeyType: 'next',
                    textContentType: 'emailAddress',
                    keyboardType: 'email-address',
                    autoCapitalize: 'none',
                    error: 'E-mail inválido',
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    onSubmitEditing: () => this._form.getComponent('senha').refs.input.refs.input
                        .focus(),
                },
                senha: {
                    label: '',
                    placeholder: 'Senha',
                    template: inputShowPasswordTemplate,
                    error: 'Preencha a senha',
                    returnKeyType: 'next',
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    onSubmitEditing: () => this._form.getComponent('confirmacaoSenha').refs.input
                        .refs.input
                        .focus(),
                },
                confirmacaoSenha: {
                    label: '',
                    placeholder: 'Confirmar senha',
                    template: inputShowPasswordTemplate,
                    error: 'Confirmação incorreta',
                    returnKeyType: 'go',
                    placeholderTextColor: defaultFormStyles.placeholderColor,
                    onSubmitEditing: () => this.cadastrar(),
                },
            },
            stylesheet: {
                ...Form.stylesheet,
                ...defaultFormStyles,
            },
            auto: 'none'
        };

        this.validate = null;
    }

    onChange(user) {
        this.setState({ user });
        if (user.confirmacaoSenha != null && user.confirmacaoSenha != '') {
            var values = this._form.getValue();
            var result = this._form.validate(values, this.User);
            this.validate = result.isValid();
        }
    };

    cadastrar() {

        var values = this._form.getValue();
        var result = this._form.validate(values, this.User);

        if (result.isValid()) {
            this.setState({ logging: true });

            console.log('sou valido');
            let data = {
                'email': values.email,
                'nome': values.nome,
                'password': values.senha,
            };

            Auth.cadastrar(data)
                .then(response => {
                    var user = {
                        username: values.email,
                        password: values.senha
                    };
                    Auth.login(user)
                    .then(response => {
                        console.log(response);
                        if(response === true){
                            //Actions.drawer({ type: 'reset' });
                            Alert.alert(
                                'PARABÉNS!',
                                'Sua conta foi criada com sucesso.',
                                [
                                    { text: 'OK', onPress: () => Actions.home({ type: 'replace' })},
                                ],
                                { cancelable: false }
                            );
                        }
                        //this.setModalVisible(false);
                    })
                })
                .catch(error => {
                    console.log(error);
                    // alert(error);
                    Alert.alert(
                        'ERRO AO CRIAR CONTA',
                        'Este email já está cadastrado.',
                        [
                            { text: 'OK', onPress: () => console.log('OK Pressed') },
                        ],
                        { cancelable: false }
                    );
                    this.setState({ logging: false });
                });
        } else {
            //alert('Verifique as informações e tente novamente');
            console.log(result.firstError().message);
        }
    }
 
    render(){
        return(
            <View style={styles.container}>
                <ScrollView keyboardShouldPersistTaps="always">
                    <ImageBackground source={require('../assets/images/drawable-hdpi/imagem3.png')} style={styles.imgBack}>
                        <Image source={require('../assets/images/logo-hredacao.png')} style={styles.logo}/>
                        <Text style={styles.title}>cadastre-se</Text>
                        <View style={styles.fields}>
                            <Form
                                type={this.User}
                                options={this.formOptions}
                                ref={c => this._form = c}
                                value={this.state.user}
                                onChange={(v) => this.onChange(v)}
                            />
                        </View>
                        
                        {this.state.logging && (
                            <ActivityIndicator size="large" color="#107ae8" style={styles.loader} />
                        )}
                        {!this.state.logging && (
                        <TouchableOpacity style={styles.button} onPress={() => this.cadastrar()}>
                            <Text style={styles.buttonText}>criar conta</Text>
                        </TouchableOpacity>
                        )}
                        <View style={styles.just}>
                            <Text style={styles.accountCheck}>Já tem uma conta?</Text>
                            <TouchableWithoutFeedback onPress={() => Actions.login({ type: 'replace' })}><Text style={[styles.accountCheck, styles.login]}> Entrar</Text></TouchableWithoutFeedback>
                        </View>
                    </ImageBackground>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    imgBack: {
        flex: 1,
    },
    loader:{
      padding:20,
    },
    logo: {
        width: 127,
        height: 127,
        marginTop: 43,
        marginBottom: 25,
        alignSelf: 'center'
    },
    title: {
        fontFamily: "Quicksand-Bold",
        fontSize: 25,
        lineHeight: 25,
        textAlign: "left",
        color: "#094e96",
        textTransform: 'uppercase',
        marginBottom: 21,
        alignSelf: 'center'
    },
    fields: {
        width: 316,
        alignSelf: 'center',
    },
    button: {
        width: 310,
        height: 48.5,
        backgroundColor: '#107ae8',
        marginTop: 35.5,
        alignSelf: 'center',
        borderRadius: 30
    },
    buttonText: {
        fontFamily: "Quicksand-Bold",
        fontSize: 18,
        lineHeight: 23,
        textAlign: "center",
        color: "#fefdfc",
        textTransform: 'uppercase',
        marginTop: 14
    },
    form: {

    },
    accountCheck: {
        fontFamily: "Quicksand-Regular",
        fontSize: 14,
        lineHeight: 18,
        letterSpacing: 0,
        color: "#094e96",
        alignSelf: 'center',
        marginBottom: 89
    },
    login: {
        fontWeight: 'bold'
    },
    just: {
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: 26
    }
});