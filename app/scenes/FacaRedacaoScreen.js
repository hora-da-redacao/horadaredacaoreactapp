import React, { Component } from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  SafeAreaView,
  View,
  Alert,
  ImageBackground,
  ActivityIndicator,
  TouchableWithoutFeedback,
  FlatList
} from 'react-native';

import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import {PermissionsAndroid} from 'react-native';

import { Actions } from 'react-native-router-flux';
import ActionButton from 'react-native-action-button';
import ImagePicker from 'react-native-image-picker';
//import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import * as RNIap from 'react-native-iap';
import RNHeicConverter from 'react-native-heic-converter';

import { CorrecaoGrid } from '../component/CorrecaoGrid';
import {retrieveItem} from '../store/store';
import Providers from '../providers/Providers';
import colors from '../constants/Colors'

import env from '../../env.json';

const itemSkusCorrecao = Platform.select({
  ios: [
    'correcoes'
  ],
  android: [
    'correcoes'
  ]
});

export default class FacaRedacaoScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
        user: {},
        correcoes: null,
        products: null,
        creditos: 0,
        filePath: {}, 
        photo: "",
        isLoading: true
    };
  }

  getAlert(title, msg){
    Alert.alert(
      title,
      msg,
      [
          {
              text: 'OK',
              onPress: () => {
              }
          },
      ],
      { cancelable: false }
    );
  }

  showImagePicker(){
    Alert.alert(
      'Enviar Redação',
      'Escolha como quer enviar sua redação',
      [
        {
          text: 'Escolher na Galeria',
          onPress: () => this.chooseFileFromGallery()
        },
        { text: 'Tirar Foto',
          onPress: () => this.chooseFileFromCam()
        },
        {
          text: 'Cancelar',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel'
        }
      ],
      { cancelable: false }
    );
  }

  createFormData = (photo, body) => {
    const data = new FormData();
  
    data.append("file", {
      name: 'redacao.jpg',
      type: 'image/jpeg', // This is important for Android!!
      uri:
        Platform.OS === "android" ? photo.uri : (photo.uri!= undefined ? photo.uri.replace("file://", "") : photo.path.replace("file://", ""))
    });
  
    Object.keys(body).forEach(key => {
      data.append(key, body[key]);
    });
  
    console.log(data);
    return data;
  };

  handleUploadPhoto = () => { 
    let url = env.api_url + "correcoes/envia_redacao.json";
    let usuid = this.state.user._id.$oid;

    fetch(url, {
      method: "POST",
      header:{
        'Content-Type': 'multipart/form-data',
      },
      body: this.createFormData(this.state.photo, { usuid: usuid })
    })
    .then(response => response.json())
    .then(response => {
      //console.log(response.info, response.menssagem);
      //alert("Upload success!");
      Providers.check_creditos({ userid: usuid })
      .then(response => {
        console.log(response);
        this.setState({creditos: response.creditos});
        let title = response.info=="0" ? "Atenção!" : "Sucesso!";
        this.getAlert(title, response.menssagem);
        this.setState({ photo: null }, () => this.listRedacoes({ userid: usuid }));
      });
    })
    .catch(error => {
      console.log("upload error", error);
      alert("Upload failed!");
      this.getAlert("Erro!", JSON.stringify(error));
      this.setState({ isLoading: false })
    });
  };

  chooseFileFromCam = () => {
    var options = {
      title: 'Escolha a Imagem',
      cancelButtonTitle: 'Cancelar',
      takePhotoButtonTitle: 'Tirar Foto...',
      chooseFromLibraryButtonTitle: 'Escolher na Galeria',
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };
    //ImagePicker.showImagePicker(options, response => {
      //launchImageLibrary(options, response => {
      launchCamera(options, response => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        if (response.uri) {
          console.log(response);
          if(Platform.OS === 'android' || response.fileName == undefined){
            this.setState({ isLoading: true, photo: response },
              () => this.handleUploadPhoto()
            );
          }else{
            if(response.fileName.split('.').pop()=='heic' || response.fileName.split('.').pop()=='HEIC'){
              RNHeicConverter
              .convert({ // options
                  path: response.origURL,
              })
              .then((result) => {
                  console.log(result); // { success: true, path: "path/to/jpg", error, base64, }
                  this.setState({ isLoading: true, photo: result },
                    () => this.handleUploadPhoto()
                  );
              });
            }else{
              console.log(response.uri);
              this.setState({ isLoading: true, photo: response },
                () => this.handleUploadPhoto()
              );
            }
          }
        }
      }
    });
  };

  chooseFileFromGallery = () => {
    var options = {
      title: 'Escolha a Imagem',
      cancelButtonTitle: 'Cancelar',
      takePhotoButtonTitle: 'Tirar Foto...',
      chooseFromLibraryButtonTitle: 'Escolher na Galeria',
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };
    //ImagePicker.showImagePicker(options, response => {
      launchImageLibrary(options, response => {
      //launchCamera(options, response => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        if (response.uri) {
          console.log(response);
          if(Platform.OS === 'android' || response.fileName == undefined){
            this.setState({ isLoading: true, photo: response },
              () => this.handleUploadPhoto()
            );
          }else{
            if(response.fileName.split('.').pop()=='heic' || response.fileName.split('.').pop()=='HEIC'){
              RNHeicConverter
              .convert({ // options
                  path: response.origURL,
              })
              .then((result) => {
                  console.log(result); // { success: true, path: "path/to/jpg", error, base64, }
                  this.setState({ isLoading: true, photo: result },
                    () => this.handleUploadPhoto()
                  );
              });
            }else{
              console.log(response.uri);
              this.setState({ isLoading: true, photo: response },
                () => this.handleUploadPhoto()
              );
            }
          }
        }
      }
    });
  };
  
  chooseFile = () => {
    var options = {
      title: 'Escolha a Imagem',
      cancelButtonTitle: 'Cancelar',
      takePhotoButtonTitle: 'Tirar Foto...',
      chooseFromLibraryButtonTitle: 'Escolher na Galeria',
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        if (response.uri) {
          this.setState({ isLoading: true, photo: response },
            () => this.handleUploadPhoto()
          );
        }
      }
    });
  };

  listRedacoes(values){
    Providers.correcoes(values)
    .then(response => {
      this.setState({
        correcoes: response, isLoading: false
      });
      console.log("=========LISTA CORRECOES============");
      console.warn(response);
      console.warn(this.state.correcoes[0].empty);
      console.warn(this.state.correcoes[0]);
      console.log("=========LISTA CORRECOES============");
    })
    .catch(error => {
        //this.setModalVisible(false);
        console.log(error);
    });
  }

  async componentDidMount(): void {
    console.log("teste");
    var user = retrieveItem('horadaredacao.user');
    user.then(u => {
      if(u==null){
        Actions.login({ type: 'replace' });
      }else{
        let id = u._id.$oid;
        let values = { userid: id }
        //this.setState({
        //  user: u
        //},() => this.listRedacoes(values));

        this.setState({
          user: u
        }, async function () {

          Providers.check_creditos({ userid: id })
          .then(response => {
            console.log(response);
            this.setState({creditos: response.creditos});
          });

          this.listRedacoes(values);

          //RNIap.initConnection();
          //await RNIap.initConnection();
          //const products = await RNIap.getSubscriptions(itemSkus);
          //const subs = await RNIap.getAvailablePurchases();
          //RNIap.initConnection().then((success) => {
          //  if(!success){
          //      return Promisse.reject("A conexão falhou");
          //  }else{
          //    RNIap.getProducts(itemSkus).then((products) => {
          //        //handle success of fetch product list
          //        console.log("Produtos");
          //        console.log(products);
          //        this.setState({products: products});
          //    }).catch((error) => {
          //        console.log(error.message);
          //    })
          //    return true;
          //  }
          //});

          try{
            console.log("=====================");
            console.log(itemSkusCorrecao);
            console.log("=====================");
            const result = await RNIap.initConnection();
            console.log('Conectou?', result);
            await RNIap.flushFailedPurchasesCachedAsPendingAndroid();
            RNIap.getProducts(itemSkusCorrecao).then((products) => {
                //handle success of fetch product list
                console.log("=====================");
                console.log(products);
                console.log("=====================");
                this.setState({products: products});
            }).catch((error) => {
                console.log(error.message);
            })
          }catch(err){
            console.log('Deu Erro', err);
          }
        });
      }
    });

  };

  requestPurchase = async (sku) => {
      console.log("=====================");
      console.log(itemSkusCorrecao[0]);
      console.log("=====================");


      Alert.alert(
        'Créditos de Correção',
        'Deseja comprar créditos de correção?',
        [
          {
            text: 'Sim',
            onPress: () => {
              RNIap.requestPurchase(itemSkusCorrecao[0]).then((result) => {
                let usuid = this.state.user._id.$oid;
                var data = {
                  userid: usuid,
                  result: result
                }
                if(Platform.OS=="android"){
                  RNIap.consumePurchaseAndroid(result.purchaseToken);
                }
                Providers.compra_creditos(data)
                .then(response => {
                    console.log(response);
                    this.setState({creditos: 1});
                    this.chooseFile();
                    //this.showImagePicker();
                })
                .catch(error => {
                    //this.setModalVisible(false);
                    console.log(error);
                });
                console.log('result request', result);
              }).catch((e) => {
                  console.log('error request', e);
              });
            }
          },
          {
            text: 'Cancelar',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel'
          }
        ],
        { cancelable: false }
      );
  }

  check_creditos(){
    this.setState({ isLoading: false })
    let usuid = this.state.user._id.$oid;
    Providers.check_creditos({ userid: usuid })
    .then(response => {
      console.log(response);
      this.setState({creditos: response.creditos});
      
      if(response.creditos==0){
        Alert.alert(
          "Atenção!",
          "Para enviar uma redação é necessário que você adicione créditos à sua conta. Deseja comprar créditos?",
          [
              {
                text: "Cancelar",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              {
                text: 'Sim',
                onPress: () => {
                  this.requestPurchase("correcoes")
                }
              },
          ],
          { cancelable: true }
        );
      }else{
        //this.showImagePicker();
        this.chooseFile();
      }
      this.setState({ isLoading: false })
    })
    .catch(error => {
        //this.setModalVisible(false);
        console.log(error);
    });
  }
  
  //async onCheckGalleryAuthoPressed() {
  //    const success = await CameraKitGallery.checkDevicePhotosAuthorizationStatus();
  //    //console.log(success);
  //    if (success==true) {
  //      //Alert.alert('Tem permissão')
  //      //Actions.EnviarRedacaoScreen()
  //      //this.chooseFile();
  //      this.showImagePicker();
  //    } else {
  //      await CameraKitGallery.requestDevicePhotosAuthorization()
  //    }
  //}

  render() {
  
    return (
      <View style={styles.container}>
        <ParallaxScroll
            headerHeight={1}
            isHeaderFixed={false}
            parallaxHeight={1}
            renderParallaxBackground={({ animatedValue }) => <ImageBackground 
            style={{
              width: "100%",
              height: 250,
              backgroundColor:'##FFF',
              position: 'absolute',
              top:0
            }}
            imageStyle={{
              resizeMode: "cover",
              alignSelf: "flex-end"
            }}
            source={require('../assets/images/bgHome.png')}>
            </ImageBackground>}
            parallaxBackgroundScrollSpeed={5}
            parallaxForegroundScrollSpeed={2.5}
          >
            <ScrollView
              contentContainerStyle={styles.contentContainer}
              removeClippedSubviews={true}
              >
              <View>
                <View style={{ height: 150,marginTop: 60, paddingBottom:20, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={styles.titulo}>Faça sua redação</Text>
                  <Text style={styles.descricao}>Envie sua redação para correção em altíssimo nível</Text>
              
                  {this.state.isLoading && (
                    <ActivityIndicator size="large" color="#FFFFFF" style={styles.loader} />
                  )}
                </View>

                {this.state.correcoes != null && (
                  this.state.correcoes[0]==undefined && (
                  <View style={{height: 150,marginTop: 60, paddingBottom:20, paddingHorizontal: 20, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Icon
                        name="file-document-edit-outline"
                        size={100}
                        color="#D1D1D1"
                    />
                    <Text style={{color: "#333333", fontSize:16, fontFamily: "Quicksand-Regular", marginTop:20, textAlign: 'center'}}>
                        Você ainda não enviou uma redação para correção. Clique no botão abaixo para enviar.
                    </Text>
                  </View>
                  )
                )}
                {(this.state.correcoes != null && this.state.correcoes != []) && (
                  <CorrecaoGrid data={this.state.correcoes} columns={1}></CorrecaoGrid>
                )}
            </View>
          </ScrollView>
        </ParallaxScroll>
        <View style={{position: 'absolute', width: wp("100%"), bottom: 0}}>
            {(this.state.user.tipo==1 || this.state.creditos > 0)&& (
              <View style={styles.toolbarBtn}>
                <TouchableOpacity style={styles.toolbarBtn} onPress={() => this.check_creditos()}>
                    <Icon
                        name="camera"
                        size={24}
                        color="white"
                    />
                    <Text style={styles.toolbarText}>Enviar Redação</Text>
                </TouchableOpacity>
              </View>
            )}

            {(this.state.products != null && this.state.creditos == 0) && (
              ((this.state.user.tipo==3 || this.state.user.tipo==2 || this.state.user.tipo==7) && this.state.products !== []) && (
                <View style={styles.toolbarBtn}>
                  {console.log(typeof(this.state.products))}
                  <Text style={styles.txtCorrecao}>Para enviar sua redação você precisa comprar uma correção. Custa só {this.state.products[0].currency} {this.state.products[0].price}.</Text>
                  <Text style={styles.txtCorrecaoCompre} onPress={() => this.requestPurchase(this.state.products[0])}>Toque aqui para comprar</Text>
                </View>
              )
            )}
        </View>
      </View>
  )};
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    developmentModeText: {
      marginBottom: 20,
      color: 'rgba(0,0,0,0.4)',
      fontSize: 14,
      lineHeight: 19,
      textAlign: 'center',
    },
    contentContainer: {
      marginBottom: 70
    },
    fabIcon: {
      color: '#FFFFFF'
    },
    loader:{
      padding:20,
    },
    titulo: {
      fontSize: 32,
      color: "#FFFFFF",
      fontFamily: "Quicksand-Bold",
      lineHeight: 38,
      textAlign: 'center',
    },
    descricao: {
      fontSize: 20,
      color: "#FFFFFF",
      fontFamily: "Quicksand-Regular",
      lineHeight: 24,
      textAlign: 'center',
      marginVertical: 10
    },
    txtCorrecao: {
      fontSize: 16,
      color: "#FFFFFF",
      fontFamily: "Quicksand-Regular",
      lineHeight: 24,
      textAlign: 'center',
      marginVertical: 10
    },
    txtCorrecaoCompre: {
      fontSize: 18,
      color: "#FFFFFF",
      fontFamily: "Quicksand-Bold",
      lineHeight: 24,
      textAlign: 'center',
      marginVertical: 10,
    },
    toolbar:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    toolbarBtn:{
        backgroundColor: colors.primary,
        borderRadius: 0,
        flexGrow: 2,
        flexDirection: 'column',
        justifyContent: 'center',
        padding: 10,
        alignItems: 'center'
    },
    toolbarText:{
        color: '#FFFFFF',
        fontSize: 18,
    }
});