import React, { Component } from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  SafeAreaView,
  View,
  ImageBackground,
  FlatList
} from 'react-native';

import ParallaxScroll from '@monterosa/react-native-parallax-scroll';

import { Actions } from 'react-native-router-flux';

import { PodcastGrid } from '../component/PodcastGrid';
import {retrieveItem} from '../store/store';
import Providers from '../providers/Providers';

const columns = 2;

export default class PodcastsScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
        user: {},
        podcasts: []  
    };
  }

  componentDidMount(){
    var user = retrieveItem('horadaredacao.user');
    user.then(u => {
      if(u==null){
        Actions.login({ type: 'replace' });
      }else{
        let id = u._id.$oid;
        let values = { userid: id }
        Providers.podcasts(values)
        .then(response => {
          //console.log(response);
          this.setState({
            podcasts: response
          });
        })
        .catch(error => {
            //this.setModalVisible(false);
            console.log(error);
        });
      }
    });
  };
  render() {
  return (
    <View style={styles.container}>
      <ParallaxScroll
            headerHeight={1}
            isHeaderFixed={false}
            parallaxHeight={1}
            renderParallaxBackground={({ animatedValue }) => <ImageBackground 
            style={{
              width: "100%",
              height: 250,
              backgroundColor:'##FFF',
              position: 'absolute',
              top:0
            }}
            imageStyle={{
              resizeMode: "cover",
              alignSelf: "flex-end"
            }}
            source={require('../assets/images/bgHome.png')}>
            </ImageBackground>}
            parallaxBackgroundScrollSpeed={5}
            parallaxForegroundScrollSpeed={2.5}
          >
          <ScrollView style={styles.contentContainer}>
            <View>
              <View style={{ height: 120,marginTop: 60, paddingBottom:20, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.titulo}>Podcasts</Text>
                <Text style={styles.descricao}>Pílulas semanais sobre os mais variados assuntos</Text>
              </View>
              <PodcastGrid data={this.state.podcasts} columns={1}></PodcastGrid>
          </View>
        </ScrollView>
      </ParallaxScroll>
    </View>
  )};
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    paddingHorizontal: 10,
  },
  titulo: {
    fontSize: 32,
    color: "#FFFFFF",
    fontFamily: "Quicksand-Bold",
    lineHeight: 38,
    textAlign: 'center',
  },
  descricao: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Quicksand-Regular",
    lineHeight: 24,
    textAlign: 'center',
    marginVertical: 10
  },
});
