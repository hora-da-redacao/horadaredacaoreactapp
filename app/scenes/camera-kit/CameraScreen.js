import React, { Component } from 'react';
import {
  Alert,
  BackHandler
} from 'react-native';
import FormData from 'form-data';
import { Actions } from 'react-native-router-flux';
import { CameraKitCameraScreen } from 'react-native-camera-kit';
import {retrieveItem} from '../../store/store';

import env from '../../../env.json';

export default class CameraScreen extends Component {

  constructor(props) {
      super(props);
      this.state = {
        user: undefined
      };
  }

  componentDidMount() {
    BackHandler.addEventListener('backPress', () => {
      Actions.EnviarRedacaoScreen();
    });
    
    var user = retrieveItem('horadaredacao.user');
    user.then(u => {
      if(u==null){
        Actions.login({ type: 'replace' });
      }else{
        this.setState({
          user: u
        });
      }
    });
  };

  onBottomButtonPressed(event) {
    const captureImages = event.captureImages;
    if(event.type == 'left'){
      Actions.EnviarRedacaoScreen();
    }else{
      console.log(captureImages);
      captureImages.forEach((photo, index) => {
        let usuid = this.state.user._id.$oid;
        let url = env.api_url + "correcoes/envia_redacao.json";
        const file = {
          uri: 'file://' + photo.uri,
          name: `${new Date().getTime()}.jpg`,
          type: 'image/jpeg',
        }
        
        const body = new FormData()
        body.append('file', file);
        body.append('usuid', usuid);
        
        fetch(url, {
          method: 'POST',
          body
        })
        .then(response => {
          console.log("upload succes", response);
          alert("Sua redação foi enviada com sucesso! Aguarde enquando o professor faz a correção");
          Actions.FacaRedacao();
          this.setState({ photo: null });
        })
        .catch(error => {
          console.log("upload error", error);
          console.log(url);
          alert(error);
        });
      });
    }
  }

  render() {
    return (
      <CameraKitCameraScreen
        actions={{ rightButtonText: 'Ok', leftButtonText: 'Cancelar' }}
        onBottomButtonPressed={(event) => this.onBottomButtonPressed(event)}
        flashImages={{
          on: require('../../assets/images/flashOn.png'),
          off: require('../../assets/images/flashOff.png'),
          auto: require('../../assets/images/flashAuto.png')
        }}
        cameraOptions={{
          flashMode: 'auto',             // on/off/auto(default)
          focusMode: 'on',               // off/on(default)
          zoomMode: 'off',                // off/on(default)
        }}
        cameraFlipImage={require('../../assets/images/cameraFlipIcon.png')}
        captureButtonImage={require('../../assets/images/cameraButton.png')}
      />
    );
  }
}



