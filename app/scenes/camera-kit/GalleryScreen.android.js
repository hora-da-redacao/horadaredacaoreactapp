import React, {Component, RNFetchBlob} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Alert,
  BackHandler
} from 'react-native';
import {
    CameraKitCamera,
    CameraKitGallery,
    CameraKitGalleryView
} from 'react-native-camera-kit';

import { Actions } from 'react-native-router-flux';
import {retrieveItem} from '../../store/store';
import CameraScreen from './CameraScreen';

import env from '../../../env.json';

export default class GalleryScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      album: this.props.albumName,
      images: {},
      shouldRenderCameraScreen: false
    }
  }

  componentDidMount() {
    
    var user = retrieveItem('horadaredacao.user');
    user.then(u => {
      if(u==null){
        Actions.login({ type: 'replace' });
      }else{
        this.setState({
          user: u
        });
      }
    });
  };

  componentWillUnmount() {
  }

  selectedImage(photo){
    console.log('uepa');
    let usuid = this.state.user._id.$oid;
    let url = env.api_url + "correcoes/envia_redacao.json";
    console.log(photo);
    const file = {
      uri: 'file://' + photo,
      name: `${new Date().getTime()}.jpg`,
      type: 'image/jpeg',
    }
    
    const body = new FormData()
    body.append('file', file);
    body.append('usuid', usuid);
    
    /*fetch(url, {
      method: 'POST',
      body
    })*/
    fetch('POST', url, {
        'Content-Type': 'multipart/form-data',
      }, [ 
        { name: 'file', filename: `${new Date().getTime()}.jpg`, type: 'image/png', data: RNFetchBlob.wrap('file://' + photo) }
    ])
    .then(response => {
      console.log("upload succes", response);
      alert("Sua redação foi enviada com sucesso! Aguarde enquando o professor faz a correção");
      Actions.FacaRedacao();
      this.setState({ photo: null });
    })
    .catch(error => {
      console.log("upload error", error);
      console.log(url);
      alert(error);
    });
  };

  render() {
    if (this.state.shouldRenderCameraScreen) {
      return (<CameraScreen/>);
    }

    return (
        <CameraKitGalleryView
          ref={(gallery) => {
            this.gallery = gallery;
          }}
          style={{flex:1, margin: 0, backgroundColor: '#ffffff'}}
          albumName={this.state.album}
          minimumInteritemSpacing={10}
          minimumLineSpacing={10}
          columnCount={2}
          selectedImages={Object.keys(this.state.images)}
          onSelected={(result) => {
            //console.log(result);
            // this.selectedImage(this.state.images)
            //console.log(this.state.images);
          }}
          onTapImage={event => {
            var fromTapEvent = CameraKitGallery.getImageForTapEvent(event.nativeEvent); // 2
            var uriFromId  = CameraKitGallery.getImageUriForId(fromTapEvent.selectedImageId) // 3

            console.log(uriFromId);
            this.onTapImage.bind(this);
            this.selectedImage(event.nativeEvent.selected)
            //console.log(event.nativeEvent.selected);
            // event.nativeEvent.selected - ALL selected images ids
          }}
          selection={{
            selectedImage: require('../../assets/images/selected.png'),
            imagePosition: 'bottom-right',
            imageSizeAndroid: 'large',
            enable: (Object.keys(this.state.images).length < 2)
          }}
          fileTypeSupport={{
            supportedFileTypes: ['image/jpeg'],
            unsupportedOverlayColor: "#00000055",
            unsupportedImage: require('../../assets/images/unsupportedImage.png'),
            //unsupportedText: 'JPEG!!',
            unsupportedTextColor: '#ff0000'
          }}
          customButtonStyle={{
              image: require('../../assets/images/openCamera.png'),
              backgroundColor: '#06c4e9'
          }}
          onCustomButtonPress={() => this.setState({shouldRenderCameraScreen: true})}
        />
    )
  }

  onTapImage(event) {
    const uri = event.nativeEvent.selected;
    console.log('Tapped on an image: ' + uri);

    if (this.state.images[uri]) {
      delete this.state.images[uri];
    } else {
      this.state.images[uri] = true;
    }
    this.setState({images: {...this.state.images}})
  }
}
