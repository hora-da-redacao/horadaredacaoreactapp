import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  AlertIOS
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import {retrieveItem, storeItem} from '../../store/store';
import Providers from '../../providers/Providers';
import { jogoStyles } from '../../styles/jogo';
import Paragrafos from './Paragrafos'


export default class Dev1Screen extends Component {
    constructor(props) {
        super(props);
        this.state = {
          paragrafos: []  
        };
    }
    shuffleArray(array) {
        let i = array.length - 1;
        for (; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            const temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }
    componentDidMount(){
        var user = retrieveItem('horadaredacao.user');
        user.then(u => {
          if(u==null){
            Actions.login({ type: 'replace' });
          }else{
            let values = { id: this.props.tema.id.$oid }
            Providers.jogo_tema(values)
            .then(data => {
                let tema = this.shuffleArray([{
                    Desenv1: data.desenv11T1,
                    NotaDesenv1: data.notaDesenv11T1,
                    descrcorrDesenv1: data.descrcorrDesenv11T1,
                    corrcomp1Desenv1: data.corrcomp1Desenv11T1,
                    corrcomp2Desenv1: data.corrcomp2Desenv11T1,
                    corrcomp3Desenv1: data.corrcomp3Desenv11T1,
                    corrcomp4Desenv1: data.corrcomp4Desenv11T1
                },{
                    Desenv1: data.desenv12T1,
                    NotaDesenv1: data.notaDesenv12T1,
                    descrcorrDesenv1: data.descrcorrDesenv12T1,
                    corrcomp1Desenv1: data.corrcomp1Desenv12T1,
                    corrcomp2Desenv1: data.corrcomp2Desenv12T1,
                    corrcomp3Desenv1: data.corrcomp3Desenv12T1,
                    corrcomp4Desenv1: data.corrcomp4Desenv12T1
                },{
                    Desenv1: data.desenv13T1,
                    NotaDesenv1: data.notaDesenv13T1,
                    descrcorrDesenv1: data.descrcorrDesenv13T1,
                    corrcomp1Desenv1: data.corrcomp1Desenv13T1,
                    corrcomp2Desenv1: data.corrcomp2Desenv13T1,
                    corrcomp3Desenv1: data.corrcomp3Desenv13T1,
                    corrcomp4Desenv1: data.corrcomp4Desenv13T1
                }]);     
            
                this.setState({
                    paragrafos: tema
                });
            })
            .catch(error => {
                //this.setModalVisible(false);
                console.log(error);
            });
          }
        });
    };
    nav(item){
        storeItem('desenv1',item);
        Actions.Dev2Screen({ tema: this.props.tema });
    }
    render() {
        return (
            <View style={jogoStyles.container}>
                <ScrollView
                    contentContainerStyle={jogoStyles.contentContainer}>
                    <Paragrafos 
                        tema={this.props.tema} 
                        data={this.state.paragrafos} 
                        tipo='1º Desenvolvimento' 
                        storeIn='desenv1' />
                </ScrollView>
            </View>
        );
    }
}
