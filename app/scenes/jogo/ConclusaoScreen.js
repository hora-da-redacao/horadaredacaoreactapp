import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  AlertIOS
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import {retrieveItem,storeItem} from '../../store/store';
import Providers from '../../providers/Providers';
import { jogoStyles } from '../../styles/jogo';
import Paragrafos from './Paragrafos'


export default class ConclusaoScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
          paragrafos: []  
        };
    }
    shuffleArray(array) {
        let i = array.length - 1;
        for (; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            const temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }
    componentDidMount(){
        var user = retrieveItem('horadaredacao.user');
        user.then(u => {
          if(u==null){
            Actions.login({ type: 'replace' });
          }else{
            let values = { id: this.props.tema.id.$oid }
            Providers.jogo_tema(values)
            .then(data => {
                let tema = this.shuffleArray([{
                    Conclu: data.conclu1T1,
                    NotaConclu: data.notaConclu1T1,
                    descrcorrConclu: data.descrcorrConclu1T1,
                    corrcomp1Conclu: data.corrcomp1Conclu1T1,
                    corrcomp2Conclu: data.corrcomp2Conclu1T1,
                    corrcomp3Conclu: data.corrcomp3Conclu1T1,
                    corrcomp4Conclu: data.corrcomp4Conclu1T1,
                    corrcomp5Conclu: data.corrcomp5Conclu1T1
                },{
                    Conclu: data.conclu2T1,
                    NotaConclu: data.notaConclu2T1,
                    descrcorrConclu: data.descrcorrConclu2T1,
                    corrcomp1Conclu: data.corrcomp1Conclu2T1,
                    corrcomp2Conclu: data.corrcomp2Conclu2T1,
                    corrcomp3Conclu: data.corrcomp3Conclu2T1,
                    corrcomp4Conclu: data.corrcomp4Conclu2T1,
                    corrcomp5Conclu: data.corrcomp5Conclu2T1
                },{
                    Conclu: data.conclu3T1,
                    NotaConclu: data.notaConclu3T1,
                    descrcorrConclu: data.descrcorrConclu3T1,
                    corrcomp1Conclu: data.corrcomp1Conclu3T1,
                    corrcomp2Conclu: data.corrcomp2Conclu3T1,
                    corrcomp3Conclu: data.corrcomp3Conclu3T1,
                    corrcomp4Conclu: data.corrcomp4Conclu3T1,
                    corrcomp5Conclu: data.corrcomp5Conclu3T1
                }]);     
            
                this.setState({
                    paragrafos: tema
                });
            })
            .catch(error => {
                //this.setModalVisible(false);
                console.log(error);
            });
          }
        });
    };
    render() {
        return (
            <View style={jogoStyles.container}>
                <ScrollView contentContainerStyle={jogoStyles.contentContainer}>
                    <Paragrafos 
                        tema={this.props.tema} 
                        data={this.state.paragrafos} 
                        tipo='Conclusão' 
                        storeIn='conclusao' />
                </ScrollView>
            </View>
        );
    }
}
