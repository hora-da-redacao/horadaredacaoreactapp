import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  AlertIOS
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import {retrieveItem, storeItem} from '../../store/store';
import Providers from '../../providers/Providers';
import { jogoStyles } from '../../styles/jogo';
import Paragrafos from './Paragrafos'


export default class IntroScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
          paragrafos: []  
        };
    }
    shuffleArray(array) {
        let i = array.length - 1;
        for (; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            const temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }
    componentDidMount(){
        var user = retrieveItem('horadaredacao.user');
        user.then(u => {
          if(u==null){
            Actions.login({ type: 'replace' });
          }else{
            let values = { id: this.props.tema.id.$oid }
            Providers.jogo_tema(values)
            .then(data => {
                let tema = this.shuffleArray([{
                    Introd: data.introd1T1,
                    NotaIntrod: data.notaIntrod1T1,
                    descrcorrIntrod: data.descrcorrIntrod1T1,
                    corrcomp1Introd: data.corrcomp1Introd1T1,
                    corrcomp2Introd: data.corrcomp2Introd1T1,
                    corrcomp3Introd: data.corrcomp3Introd1T1,
                    corrcomp4Introd: data.corrcomp4Introd1T1
                  },{
                    Introd: data.introd2T1,
                    NotaIntrod: data.notaIntrod2T1,
                    descrcorrIntrod: data.descrcorrIntrod2T1,
                    corrcomp1Introd: data.corrcomp1Introd2T1,
                    corrcomp2Introd: data.corrcomp2Introd2T1,
                    corrcomp3Introd: data.corrcomp3Introd2T1,
                    corrcomp4Introd: data.corrcomp4Introd2T1
                  },{
                    Introd: data.introd3T1,
                    NotaIntrod: data.notaIntrod3T1,
                    descrcorrIntrod: data.descrcorrIntrod3T1,
                    corrcomp1Introd: data.corrcomp1Introd3T1,
                    corrcomp2Introd: data.corrcomp2Introd3T1,
                    corrcomp3Introd: data.corrcomp3Introd3T1,
                    corrcomp4Introd: data.corrcomp4Introd3T1
                }]);     
            
                this.setState({
                    paragrafos: tema
                });
            })
            .catch(error => {
                //this.setModalVisible(false);
                console.log(error);
            });
          }
        });
    };
    render() {
        return (
            <View style={jogoStyles.container}>
                <ScrollView
                    contentContainerStyle={jogoStyles.contentContainer}>
                    <View>
                        <Paragrafos 
                        tema={this.props.tema} 
                        data={this.state.paragrafos} 
                        tipo='Introdução' 
                        storeIn='intro' />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

