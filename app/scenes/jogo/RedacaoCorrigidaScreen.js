import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  AlertIOS
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button } from 'react-native-elements'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Accordian from '../../component/Accordion';
import { jogoStyles, accordionStyles } from '../../styles/jogo';


export default class RedacaoCorrigidaScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
        };
    }
    render() {
      
        return (
            <View style={jogoStyles.container}>
                <ScrollView
                    contentContainerStyle={jogoStyles.contentContainer}>
                    <View>
                        <Text style={jogoStyles.temaJogo}>{this.props.redacao.descrTema}</Text>
                        <View>
                            <Accordian 
                                paragrafo = {this.props.redacao.Introd}
                                correcao = {this.props.redacao.descrcorrIntrod}
                                desccor = 'Correção da Introdução'
                                nota = {Number.parseInt(this.props.redacao.NotaIntrod)}
                                comp1 = {{correcao:this.props.redacao.corrcomp1Introd}}
                                comp2 = {{correcao:this.props.redacao.corrcomp2Introd}}
                                comp3 = {{correcao:this.props.redacao.corrcomp3Introd}}
                                comp4 = {{correcao:this.props.redacao.corrcomp4Introd}}
                                comp5 = {undefined}
                            />
                            <Accordian 
                                paragrafo = {this.props.redacao.Desenv1}
                                correcao = {this.props.redacao.descrcorrDesenv1}
                                desccor = 'Correção do Desenvolvimento 1'
                                nota = {Number.parseInt(this.props.redacao.NotaDesenv1)}
                                comp1 = {{correcao:this.props.redacao.corrcomp1Desenv1}}
                                comp2 = {{correcao:this.props.redacao.corrcomp2Desenv1}}
                                comp3 = {{correcao:this.props.redacao.corrcomp3Desenv1}}
                                comp4 = {{correcao:this.props.redacao.corrcomp4Desenv1}}
                                comp5 = {undefined}
                            />
                            <Accordian 
                                paragrafo = {this.props.redacao.Desenv2}
                                correcao = {this.props.redacao.descrcorrDesenv2}
                                desccor = 'Correção da Introdução'
                                nota = {Number.parseInt(this.props.redacao.NotaDesenv2)}
                                comp1 = {{correcao:this.props.redacao.corrcomp1Desenv2}}
                                comp2 = {{correcao:this.props.redacao.corrcomp2Desenv2}}
                                comp3 = {{correcao:this.props.redacao.corrcomp3Desenv2}}
                                comp4 = {{correcao:this.props.redacao.corrcomp4Desenv2}}
                                comp5 = {undefined}
                            />
                            <Accordian 
                                paragrafo = {this.props.redacao.Conclu}
                                correcao = {this.props.redacao.descrcorrConclu}
                                desccor = 'Correção da Introdução'
                                nota = {Number.parseInt(this.props.redacao.NotaConclu)}
                                comp1 = {{correcao:this.props.redacao.corrcomp1Conclu}}
                                comp2 = {{correcao:this.props.redacao.corrcomp2Conclu}}
                                comp3 = {{correcao:this.props.redacao.corrcomp3Conclu}}
                                comp4 = {{correcao:this.props.redacao.corrcomp4Conclu}}
                                comp5 = {{correcao:this.props.redacao.corrcomp5Conclu}}
                            />
                        </View>
                    </View>
                </ScrollView>
                <View>
                    <TouchableOpacity style={jogoStyles.toolbarBtn} onPress={() => Actions.Jogo()}>
                            <Icon
                                name="note"
                                size={24}
                                color="white"
                            />
                            <Text style={jogoStyles.toolbarText}>Nova Redação</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    async onCheckCameraAuthoPressed() {
        const success = await CameraKitCamera.checkDeviceCameraAuthorizationStatus();
        if (success) {
            AlertIOS.alert('You have permission 🤗')
        }
        else {
            AlertIOS.alert('No permission 😳')
        }
    }

    async onCheckGalleryAuthoPressed() {
        const success = await CameraKitGallery.checkDevicePhotosAuthorizationStatus();
        if (success) {
            AlertIOS.alert('You have permission 🤗')
        }
        else {
            AlertIOS.alert('No permission 😳')
        }
    }
}