import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  AlertIOS
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import {retrieveItem,storeItem} from '../../store/store';
import Providers from '../../providers/Providers';
import { jogoStyles } from '../../styles/jogo';
import Paragrafos from './Paragrafos'


export default class Dev2Screen extends Component {
    constructor(props) {
        super(props);
        this.state = {
          paragrafos: []  
        };
    }
    shuffleArray(array) {
        let i = array.length - 1;
        for (; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            const temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }
    componentDidMount(){
        var user = retrieveItem('horadaredacao.user');
        user.then(u => {
          if(u==null){
            Actions.login({ type: 'replace' });
          }else{
            let values = { id: this.props.tema.id.$oid }
            Providers.jogo_tema(values)
            .then(data => {
                let tema = this.shuffleArray([{
                    Desenv2: data.desenv21T1,
                    NotaDesenv2: data.notaDesenv21T1,
                    descrcorrDesenv2: data.descrcorrDesenv21T1,
                    corrcomp1Desenv2: data.corrcomp1Desenv21T1,
                    corrcomp2Desenv2: data.corrcomp2Desenv21T1,
                    corrcomp3Desenv2: data.corrcomp3Desenv21T1,
                    corrcomp4Desenv2: data.corrcomp4Desenv21T1
                },{
                    Desenv2: data.desenv22T1,
                    NotaDesenv2: data.notaDesenv22T1,
                    descrcorrDesenv2: data.descrcorrDesenv22T1,
                    corrcomp1Desenv2: data.corrcomp1Desenv22T1,
                    corrcomp2Desenv2: data.corrcomp2Desenv22T1,
                    corrcomp3Desenv2: data.corrcomp3Desenv22T1,
                    corrcomp4Desenv2: data.corrcomp4Desenv22T1
                },{
                    Desenv2: data.desenv23T1,
                    NotaDesenv2: data.notaDesenv23T1,
                    descrcorrDesenv2: data.descrcorrDesenv23T1,
                    corrcomp1Desenv2: data.corrcomp1Desenv23T1,
                    corrcomp2Desenv2: data.corrcomp2Desenv23T1,
                    corrcomp3Desenv2: data.corrcomp3Desenv23T1,
                    corrcomp4Desenv2: data.corrcomp4Desenv23T1
                }]);     
            
                this.setState({
                    paragrafos: tema
                });
            })
            .catch(error => {
                //this.setModalVisible(false);
                console.log(error);
            });
          }
        });
    };
    render() {
        return (
            <View style={jogoStyles.container}>
                <ScrollView
                    contentContainerStyle={jogoStyles.contentContainer}>
                    <Paragrafos 
                        tema={this.props.tema} 
                        data={this.state.paragrafos} 
                        tipo='2º Desenvolvimento' 
                        storeIn='desenv2' />
                </ScrollView>
            </View>
        );
    }
}