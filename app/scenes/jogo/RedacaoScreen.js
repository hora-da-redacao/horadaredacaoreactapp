import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  AlertIOS
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button, Overlay } from 'react-native-elements'

import {retrieveItem, storeItem} from '../../store/store';
import Providers from '../../providers/Providers';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { jogoStyles, overlayStyle } from '../../styles/jogo';


export default class RedacaoScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            userId:'',
            intro:'',
            desenv1:'',
            desenv2:'',
            conclusao:'',
            categoriaJogo:0,
            title:'',
            nota:'',
            redacao: {}
        };
    }
    componentDidMount(){
        var user = retrieveItem('horadaredacao.user');
        user.then(u => {
          if(u==null){
            Actions.login({ type: 'replace' });
          }else{
            this.setState({ userId: u._id.$oid});

            var categoriaJogo = retrieveItem('categoriaJogo');
            categoriaJogo.then(obj => {this.setState({ categoriaJogo: obj});});

            var intro = retrieveItem('intro');
            intro.then(obj => {this.setState({ intro: obj});});
            
            var desenv1 = retrieveItem('desenv1');
            desenv1.then(obj => {this.setState({ desenv1: obj});});
            
            var desenv2 = retrieveItem('desenv2');
            desenv2.then(obj => {this.setState({ desenv2: obj});});
            
            var conclusao = retrieveItem('conclusao');
            conclusao.then(obj => {this.setState({ conclusao: obj});});
          }
        });
    };
    showPopup(title, nota){
        this.setState({ isVisible: true});
        this.setState({ title: title});
        this.setState({ nota: nota});
    }
    corrigir(){
        let fatorDivisao = ((this.state.categoriaJogo != 1) ? 100 : 1);
        let NotaTotal = (Number.parseInt(this.state.intro.NotaIntrod) + Number.parseInt(this.state.desenv1.NotaDesenv1) + Number.parseInt(this.state.desenv2.NotaDesenv2) + Number.parseInt(this.state.conclusao.NotaConclu));
        var NotaExibir = NotaTotal / fatorDivisao;

        console.log(NotaExibir);
        if (NotaTotal == 1000) {
            this.showPopup("Parabéns!", NotaExibir);
        }else if(NotaTotal >= 910 && NotaTotal <= 990){
            this.showPopup("Excelente!", NotaExibir);
        } else if (NotaTotal >= 810 && NotaTotal <= 900) {
            this.showPopup("Muit Bom! Você está no caminho!", NotaExibir);
        } else if (NotaTotal >= 710 && NotaTotal <= 800) {
            this.showPopup("Bom! Você está no caminho!", NotaExibir);
        } else if (NotaTotal >= 500 && NotaTotal <= 700) {
            this.showPopup("Regular!", NotaExibir);
        } else {
            this.showPopup("Precisamos Melhorar!", NotaExibir);
        }    
        this.gravaRedacao(NotaExibir);
    }
    gravaRedacao(notaFinal){
        let body = {
            id_usuario: this.state.userId,
            NotaFinal: notaFinal,
            descrTema: this.props.tema.titulo_tema,
            Introd: this.state.intro.Introd,
            NotaIntrod: this.state.intro.NotaIntrod,
            descrcorrIntrod: this.state.intro.descrcorrIntrod,
            corrcomp1Introd: this.state.intro.corrcomp1Introd,
            corrcomp2Introd: this.state.intro.corrcomp2Introd,
            corrcomp3Introd: this.state.intro.corrcomp3Introd,
            corrcomp4Introd: this.state.intro.corrcomp4Introd,
            Desenv1: this.state.desenv1.Desenv1,
            NotaDesenv1: this.state.desenv1.NotaDesenv1,
            descrcorrDesenv1: this.state.desenv1.descrcorrDesenv1,
            corrcomp1Desenv1: this.state.desenv1.corrcomp1Desenv1,
            corrcomp2Desenv1: this.state.desenv1.corrcomp2Desenv1,
            corrcomp3Desenv1: this.state.desenv1.corrcomp3Desenv1,
            corrcomp4Desenv1: this.state.desenv1.corrcomp4Desenv1,
            Desenv2: this.state.desenv2.Desenv2,
            NotaDesenv2: this.state.desenv2.NotaDesenv2,
            descrcorrDesenv2: this.state.desenv2.descrcorrDesenv2,
            corrcomp1Desenv2: this.state.desenv2.corrcomp1Desenv2,
            corrcomp2Desenv2: this.state.desenv2.corrcomp2Desenv2,
            corrcomp3Desenv2: this.state.desenv2.corrcomp3Desenv2,
            corrcomp4Desenv2: this.state.desenv2.corrcomp4Desenv2,
            Conclu: this.state.conclusao.Conclu,
            NotaConclu: this.state.conclusao.NotaConclu,
            descrcorrConclu: this.state.conclusao.descrcorrConclu,
            corrcomp1Conclu: this.state.conclusao.corrcomp1Conclu,
            corrcomp2Conclu: this.state.conclusao.corrcomp2Conclu,
            corrcomp3Conclu: this.state.conclusao.corrcomp3Conclu,
            corrcomp4Conclu: this.state.conclusao.corrcomp4Conclu,
            corrcomp5Conclu: this.state.conclusao.corrcomp5Conclu
        };
        this.setState({ redacao: body});
        Providers.grava_redacao(body).then(obj => {
            console.log(obj);
        });
    }
    closeOverlay(){
        this.setState({ isVisible: false});
        Actions.RedacaoCorrigidaScreen({redacao: this.state.redacao});
    }
    render() {
      
        return (
            <View style={jogoStyles.container}>
                <Overlay 
                    isVisible={this.state.isVisible}
                    width={300}
                    height='auto'>
                    <View style={overlayStyle.content}>
                        <Text style={overlayStyle.title}>{this.state.title}</Text>
                        <Text style={overlayStyle.text}>Sua nota na redação foi:</Text>
                        <Text style={overlayStyle.title}>{this.state.nota}</Text>
                        <Text style={overlayStyle.text}>Clique nos parágrafos e veja</Text>
                        <Text style={overlayStyle.text}>o que o professor escreveu para você.</Text>
                    </View>
                    <Button
                        onPress={() => this.closeOverlay()}
                        title="Ok"
                    />
                </Overlay>
                <ScrollView
                    contentContainerStyle={jogoStyles.contentContainer}>
                    <View>
                        <Text style={jogoStyles.temaJogo}>{this.props.tema.titulo_tema}</Text>
                        <Text style={jogoStyles.textParagrafo}>{this.state.intro.Introd}</Text>
                        <Text style={jogoStyles.textParagrafo}>{this.state.desenv1.Desenv1}</Text>
                        <Text style={jogoStyles.textParagrafo}>{this.state.desenv2.Desenv2}</Text>
                        <Text style={jogoStyles.textParagrafo}>{this.state.conclusao.Conclu}</Text>
                    </View>
                </ScrollView>
                <View style={jogoStyles.toolbar}>
                    <TouchableOpacity style={jogoStyles.toolbarBtn} onPress={() => Actions.Jogo()}>
                            <Icon
                                name="eraser"
                                size={24}
                                color="white"
                            />
                            <Text style={jogoStyles.toolbarText}>Apagar Redação</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={jogoStyles.toolbarBtn} onPress={() => this.corrigir()}>
                            <Icon
                                name="spellcheck"
                                size={24}
                                color="white"
                            />
                            <Text style={jogoStyles.toolbarText}>Corrgir Redação</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
