import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  AlertIOS
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import {storeItem} from '../../store/store';
import { jogoStyles } from '../../styles/jogo';

export default class Paragrafos extends Component {
    constructor(props) {
        super(props);
        this.state = {
          paragrafos: []  
        };
    }
    nav(item){
        storeItem(this.props.storeIn,item);
        if(this.props.storeIn=='intro'){
            Actions.Dev1Screen({ tema: this.props.tema });
        }
        if(this.props.storeIn=='desenv1'){
            Actions.Dev2Screen({ tema: this.props.tema });
        }
        if(this.props.storeIn=='desenv2'){
            Actions.ConclusaoScreen({ tema: this.props.tema });
        }
        if(this.props.storeIn=='conclusao'){
            Actions.RedacaoScreen({ tema: this.props.tema });
        }
    }
    paragrafo(item){
        if(this.props.storeIn=='intro'){
            return item.Introd.trim();
        }
        if(this.props.storeIn=='desenv1'){
            return item.Desenv1.trim();
        }
        if(this.props.storeIn=='desenv2'){
            return item.Desenv2.trim();
        }
        if(this.props.storeIn=='conclusao'){
            return item.Conclu.trim();
        }
    }
    render() {
        return (
            <View>
                <Text style={jogoStyles.temaJogo}>{this.props.tema.titulo_tema}</Text>
                <FlatList
                data={this.props.data}
                keyExtractor={(item, index) => index}
                renderItem={({ item, index }) => {
                    return (
                        <View>
                            <TouchableOpacity onPress={() => this.nav(item)}>
                                <Text style={jogoStyles.textTipo}>{this.props.tipo} {index+1}</Text>
                                <Text style={[jogoStyles.textParagrafo, (index != 2 ? jogoStyles.borderBottom : {})]}>{this.paragrafo(item)}</Text>
                            </TouchableOpacity>
                        </View>
                    );
                }}
                />
            </View>
        );
    }
}

