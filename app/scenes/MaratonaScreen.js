import React, { Component } from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  SafeAreaView,
  View,
  ImageBackground,
  FlatList
} from 'react-native';

import ParallaxScroll from '@monterosa/react-native-parallax-scroll';

import { Actions } from 'react-native-router-flux';

import { MaratonasGrid } from '../component/MaratonasGrid';
import {retrieveItem} from '../store/store';
import Providers from '../providers/Providers';

const columns = 2;

export default class MaratonaScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
        user: {},
        maratona: []  
    };
  }

  componentDidMount(){
    var user = retrieveItem('horadaredacao.user');
    user.then(u => {
      if(u==null){
        Actions.login({ type: 'replace' });
      }else{
        let id = u._id.$oid;
        let values = { userid: id }
        Providers.maratona(values)
        .then(response => {
          this.setState({
            maratona: response,
            user: u
          });
        })
        .catch(error => {
            //this.setModalVisible(false);
            console.log(error);
        });
      }
    });
  };
  render() {
  return (
    <View style={styles.container}>
    <ParallaxScroll
          headerHeight={1}
          isHeaderFixed={false}
          parallaxHeight={1}
          renderParallaxBackground={({ animatedValue }) => <ImageBackground 
          style={{
            width: "100%",
            height: 250,
            backgroundColor:'##FFF',
            position: 'absolute',
            top:0
          }}
          imageStyle={{
            resizeMode: "cover",
            alignSelf: "flex-end"
          }}
          source={require('../assets/images/bgHome.png')}>
          </ImageBackground>}
          parallaxBackgroundScrollSpeed={5}
          parallaxForegroundScrollSpeed={2.5}
        >
        <ScrollView style={styles.contentContainer}>
          <View>
            <View style={{ height: 150,marginTop: 60, paddingBottom:20, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={styles.titulo}>Maratona</Text>
              <Text style={styles.descricao}>42 dias antes do ENEM receba diariamente temas e podcasts para exercitar sua redação</Text>
              {this.state.user.tipo!="3" &&(
              <Text style={styles.descricaoMensagem}>Confira abaio um exemplo do que te espera nessa maratona</Text>
              )}
            </View>
            <MaratonasGrid data={this.state.maratona} columns={1}></MaratonasGrid>
          </View>
        </ScrollView>
      </ParallaxScroll>
    </View>
  )};
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    paddingHorizontal: 10,
  },
  titulo: {
    fontSize: 32,
    color: "#FFFFFF",
    fontFamily: "Quicksand-Bold",
    lineHeight: 38,
    textAlign: 'center',
  },
  descricao: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Quicksand-Regular",
    lineHeight: 24,
    textAlign: 'center',
    marginVertical: 10
  },
  descricaoMensagem: {
    fontSize: 16,
    color: "#FFFFFF",
    fontFamily: "Quicksand-Bold",
    lineHeight: 24,
    textAlign: 'center',
    marginVertical: 20
  },
});
