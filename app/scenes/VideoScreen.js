/**
 * @format
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  PixelRatio,
  Platform,
  ImageBackground,
  Dimensions,
} from 'react-native';
import YouTube, { YouTubeStandaloneIOS, YouTubeStandaloneAndroid } from 'react-native-youtube';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Card, Button, Image } from 'react-native-elements';
import Orientation from 'react-native-orientation';
import { Actions } from 'react-native-router-flux';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import Colors from '../constants/Colors';

export default class VideoScreenDefault extends React.Component {
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    controls: 2,
    fullscreen: true,
    playerWidth: Dimensions.get('window').width,
  };

  _youTubeRef = React.createRef();

  render() {
    return (
        <View style={styles.container}>
          <ParallaxScroll
                headerHeight={1}
                isHeaderFixed={false}
                parallaxHeight={1}
                renderParallaxBackground={({ animatedValue }) => <ImageBackground 
                style={{
                  width: "100%",
                  height: 250,
                  backgroundColor:'##FFF',
                  position: 'absolute',
                  top:0
                }}
                imageStyle={{
                  resizeMode: "cover",
                  alignSelf: "flex-end"
                }}
                source={require('../assets/images/bgHome.png')}>
                </ImageBackground>}
                parallaxBackgroundScrollSpeed={5}
                parallaxForegroundScrollSpeed={2.5}
              >
      <ScrollView style={styles.contentContainer}>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <View style={{ height: 0,marginTop: 60, paddingBottom:20, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    
                </View>
                <YouTube
                ref={this._youTubeRef}
                apiKey="AIzaSyA9YHVqf6jPqSYvsIuZZBu7_uLR41MOHig"
                videoId={this.props.uid}
                play={this.state.isPlaying}
                loop={this.state.isLooping}
                fullscreen={this.state.fullscreen}
                controls={this.state.controls}
                style={[
                    {   
                        width: wp('90%'),
                        height: PixelRatio.roundToNearestPixel(350 / (16 / 9)) 
                    },
                    styles.player,
                ]}
                onError={e => {
                    this.setState({ error: e.error });
                }}
                onReady={e => {
                    this.setState({ isReady: true });
                }}
                onChangeState={e => {
                    this.setState({ status: e.state });
                    //if(e.state=='playing'){
                    //    this.setState({ fullscreen: true });
                    //}
                }}
                onChangeQuality={e => {
                    this.setState({ quality: e.quality });
                }}
                onChangeFullscreen={e => {
                    this.setState({ fullscreen: e.isFullscreen });
                    if(e.isFullscreen==false){
                        Orientation.lockToPortrait();
                        this.setState({ controls: 0 });
                        //Actions.pop();
                    }
                }}
                onProgress={e => {
                    this.setState({ currentTime: e.currentTime });
                }}
                />
                <Button
                    title={'Continuar Assistindo'}
                    color={this.state.status == 'playing' ? 'red' : undefined}
                    buttonStyle={styles.btn}
                    onPress={() => {
                    this.setState(state => ({ isPlaying: !state.isPlaying, fullscreen: true,  controls: 2 }));
                    }}
                />
            </View>
      </ScrollView>
      </ParallaxScroll>
    </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentContainer: {
        paddingHorizontal: 10,
    },
    titulo: {
      fontSize: 32,
      color: "#FFFFFF",
      fontFamily: "Quicksand-Bold",
      lineHeight: 38,
      textAlign: 'center',
    },
    buttonGroup: {
        flexDirection: 'row',
        alignSelf: 'center',
        paddingBottom: 5,
    },
    btn: {
        borderRadius: 4, 
        marginTop: 4, 
        backgroundColor: Colors.primary,
        fontFamily: "Quicksand-Regular",
        alignSelf: 'stretch',
        width: wp('90%')
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    player: {
        marginVertical: 10,
    },
});