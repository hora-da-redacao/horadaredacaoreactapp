import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  ImageBackground,
  View,
  Text,
  ScrollView
} from 'react-native';

import ParallaxScroll from '@monterosa/react-native-parallax-scroll';

import { Actions } from 'react-native-router-flux';

import { Pptgrid } from '../component/PptGrid';
import {retrieveItem} from '../store/store';
import Providers from '../providers/Providers';

const columns = 2;

export default class EbooksScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
        user: {},
        ppts: []  
    };
  }

  componentDidMount(){
    var user = retrieveItem('horadaredacao.user');
    user.then(u => {
      if(u==null){
        Actions.login({ type: 'replace' });
      }else{
        let id = u._id.$oid;
        let values = { categoria: '4', userid: id }
        Providers.ppts(values)
        .then(response => {
          this.setState({
            ppts: response
          });
        })
        .catch(error => {
            //this.setModalVisible(false);
            console.log(error);
        });
      }
    });
  };
  render() {
  return (
    <View style={styles.container}>
      <ParallaxScroll
            headerHeight={1}
            isHeaderFixed={false}
            parallaxHeight={1}
            renderParallaxBackground={({ animatedValue }) => <ImageBackground 
            style={{
              width: "100%",
              height: 250,
              backgroundColor:'##FFF',
              position: 'absolute',
              top:0
            }}
            imageStyle={{
              resizeMode: "cover",
              alignSelf: "flex-end"
            }}
            source={require('../assets/images/bgHome.png')}>
            </ImageBackground>}
            parallaxBackgroundScrollSpeed={5}
            parallaxForegroundScrollSpeed={2.5}
          >
          <ScrollView style={styles.contentContainer}>
            <View>
              <View style={{ height: 120,marginTop: 60, paddingBottom:20, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={styles.titulo}>Redações Exemplares</Text>
                <Text style={styles.descricao}>Redações em alto nível para você ler, analisar e se inspirar</Text>
              </View>
              <Pptgrid data={this.state.ppts} columns={1}></Pptgrid>
            </View>
          </ScrollView>
      </ParallaxScroll>
    </View>
  )};
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingHorizontal: 15,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  titulo: {
    fontSize: 32,
    color: "#FFFFFF",
    fontFamily: "Quicksand-Bold",
    lineHeight: 38,
    textAlign: 'center',
  },
  descricao: {
    fontSize: 20,
    color: "#FFFFFF",
    fontFamily: "Quicksand-Regular",
    lineHeight: 24,
    textAlign: 'center',
    marginVertical: 10
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  item: {
    alignItems: "center",
    flexGrow: 1,
    margin: 4,
    width: 120,
    height: 50,
  },
  text: {
    color: "#333333",
    fontSize: 18
  },
  itemEmpty: {
    display: 'none'
  },
});
