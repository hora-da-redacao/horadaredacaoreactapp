import { createStackNavigator, createAppContainer } from 'react-navigation';
import React, { Component } from 'react';
import {
  Image,
  Platform,
  TouchableOpacity,
  ImageBackground,
  Linking,
  StyleSheet, Text, View, TextInput, FlatList, Picker, ScrollView, TouchableWithoutFeedback
} from 'react-native';

import PropTypes from "prop-types";
import {Image as ReactImage} from 'react-native';
import ImageLoad from 'react-native-image-placeholder'; 
import Orientation from 'react-native-orientation';
import YouTube, { YouTubeStandaloneIOS, YouTubeStandaloneAndroid } from 'react-native-youtube';

import Svg, {Defs, Pattern} from 'react-native-svg';
import {Path as SvgPath} from 'react-native-svg';
import {Text as SvgText} from 'react-native-svg';
import {Image as SvgImage} from 'react-native-svg';

import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
 
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  CameraKitCamera,
  CameraKitGallery,
} from 'react-native-camera-kit';

import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Actions } from 'react-native-router-flux';
import { Cadeado } from '../component/Cadeado';
import HomeItemMenu from '../component/HomeItemMenu';
import Providers from '../providers/Providers';
import {retrieveItem} from '../store/store';
import colors from '../constants/Colors';


import env from '../../env.json';
import FacaRedacaoScreen from './FacaRedacaoScreen';
import { TouchableHighlight } from 'react-native-gesture-handler';
import OneSignal from 'react-native-onesignal'; 
import {BackHandler} from "react-native";

export default class HomeScreen extends Component {
  constructor(props) {
      super(props);
      this.state = {
          user: {},
          nomeUsuario: '',
          homeData: null,
          countNotification: 0
      };

      this.backHandler = null;
      this.onOpened = this.onOpened.bind(this);
      this.onReceived = this.onReceived.bind(this);
      //this.onRegistered = this.onRegistered.bind(this);
      this.onIds = this.onIds.bind(this);
      this.countNot = this.countNot.bind(this);

      OneSignal.init("c52b9e72-62c6-41b7-b1ca-a6a6929b7c17", {kOSSettingsKeyAutoPrompt : false, kOSSettingsKeyInAppLaunchURL: false, kOSSettingsKeyInFocusDisplayOption:2});
      OneSignal.inFocusDisplaying(2); // Controls what should happen if a notification is received while the app is open. 2 means that the notification will go directly to the device's notification center.
      
  }

  handleBackButton(){
    //console.log('back');
    //console.log(Actions.currentScene);
    if(Actions.currentScene === 'home'){
      BackHandler.exitApp();
    }else{
      //console.log('back oder');
      Actions.pop();
      return true;
    }
  }
  
  componentDidMount(){
    Orientation.lockToPortrait();
    Actions.refresh({ key: 'drawer', open: true });
  }

  componentWillMount(){
    var user = retrieveItem('horadaredacao.user');
    user.then(u => {
      if(u==null){
        Actions.login({ type: 'replace' });
      }else{
        let id = u._id.$oid;
        let values = { userid: id }
        //console.log(u.Nome.split(' ')[0]);
        this.setState({
          user: u
        });
        this.setState({
          nomeUsuario: u.Nome.split(' ')[0]
        });

        Providers.home(values)
        .then(response => {
          this.setState({
            homeData: response
          }, function(){
            //console.log(response);
          });
        })
        .catch(error => {
            //this.setModalVisible(false);
            console.log(error);
        });
        Providers.countunread(values)
        .then(response => {
            this.setState({countNotification: response.count});
        })
        .catch(error => {
            console.log("GetStats - Error")
            //this.setModalVisible(false);
            console.log(error);
        });
      }
    });
    Orientation.lockToPortrait();
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
    
    this.backHandler = BackHandler.addEventListener('hardwareBackPress',this.handleBackButton.bind(this));
    if (this.backHandler){
      this.backHandler.remove();
      this.backHandler = BackHandler.addEventListener('hardwareBackPress',this.handleBackButton.bind(this));
    }
  };

  componentWillUpdate(){
    if (this.backHandler){
      this.backHandler.remove();
      this.backHandler = BackHandler.addEventListener('hardwareBackPress',this.handleBackButton.bind(this));
    }
  }

  componentWillUnmount(){
    if (this.backHandler){
        this.backHandler.remove();
    }
  }

  setStatistic(){
    let id = this.state.user._id.$oid;
    let values = { tipo: 'video', userid: id }
    Providers.setstatistic(values)
    .then(response => {
        //console.log("[GetStats - Success]")
        //console.log(response);
    })
    .catch(error => {
        console.log("GetStats - Error")
        //this.setModalVisible(false);
        console.log(error);
    });
  }

  playVideo(uid){
    if(Platform.OS === 'android'){
      //console.log("Play Android: ", uid);
      YouTubeStandaloneAndroid.playVideo({
        apiKey: "AIzaSyA9YHVqf6jPqSYvsIuZZBu7_uLR41MOHig",
        videoId: uid,
        autoplay: true,
        controls: 2,
        lightboxMode: false,
      })
      .then(() => {
        console.log('Android Standalone Player Finished');
        this.setStatistic();
      })
      .catch(errorMessage => {
        this.setState({ error: errorMessage });
      })
    }else{
      YouTubeStandaloneIOS.playVideo(uid)
      .then(message => {
        //console.log("OK Play IOS: ", uid);
        //console.log(message);
        this.setStatistic();
      })
      .catch(errorMessage => {
        console.log("OK Play IOS: ", uid);
        this.setState({ error: errorMessage });
      })
    }
  }

  countNot(){
    let id = this.state.user._id.$oid;
    let values = { userid: id }
    Providers.countunread(values)
    .then(response => {
        this.setState({countNotification: response.count});
    })
    .catch(error => {
        console.log("GetStats - Error")
        //this.setModalVisible(false);
        console.log(error);
    });
  }

  renderIcon(){
    return <Icon name="chat" />;
  }

  async onCheckGalleryAuthoPressedHome() {
    const success = await CameraKitGallery.checkDevicePhotosAuthorizationStatus();
    const isUserAuthorizedCamera = await CameraKitCamera.requestDeviceCameraAuthorization();
    //console.log(success);
    if (success==true) {} else {
      await CameraKitGallery.requestDevicePhotosAuthorization()
    }
    if(isUserAuthorizedCamera){
      console.log(true);
    }
  }

  showModal(){
    Actions.CompreModal();
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
    this.countNot();
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    console.log('Device info: ', device);
  }

  render() {
    var homeItens = [];
    var cont = 0;
    this.onCheckGalleryAuthoPressedHome();
    env.menu_itens.forEach(element => {
      if(element.active == true){
        homeItens.push(<HomeItemMenu  key={cont} iconName={element.iconName} text={element.text} route={element.route}/>);
        cont ++;
      }
    });
    return (
      <View style={styles.container}>
        <ParallaxScroll
          headerHeight={0}
          isHeaderFixed={false}
          parallaxHeight={1}
          renderParallaxBackground={({ animatedValue }) => <ImageBackground 
          style={{
            width: wp('100%'),
            height: 200,
            backgroundColor:'##FFF',
            position: 'absolute',
            top:0
          }}
          imageStyle={{
            resizeMode: "cover",
            alignSelf: "flex-end"
          }}
          source={require('../assets/images/bgHome.png')}>
          </ImageBackground>}
          parallaxBackgroundScrollSpeed={2}
          parallaxForegroundScrollSpeed={2.5}
        >
        <ScrollView
            style={styles.scroll_view}
            contentContainerStyle={styles.contentContainer}
          >
            <View style={{marginBottom: 28, flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={styles.home_perfil}>
                  <TouchableOpacity onPress={() => Actions.perfil()} style={{width: 150, height: 50}}>
                    {(this.state.user.full_imagem === '' || this.state.user.imagem_file_name === null || this.state.user.imagem_file_name === undefined) && (
                    <View style={styles.home_perfil_grupo}>
                          <Svg style={styles.home_perfil_grupo_elipse} preserveAspectRatio="none" viewBox="0 0 50 50" fill="rgba(255, 255, 255, 1)"><SvgPath d="M 25 0 C 38.8071174621582 0 50 11.1928825378418 50 25 C 50 38.8071174621582 38.8071174621582 50 25 50 C 11.1928825378418 50 0 38.8071174621582 0 25 C 0 11.1928825378418 11.1928825378418 0 25 0 Z"  /></Svg>
                          <Svg style={styles.home_perfil_grupo_iconAwesomeUser} preserveAspectRatio="none" viewBox="0 0 26.5 31" fill="rgba(155, 155, 155, 1)"><SvgPath d="M 13.25 15.5 C 17.43203163146973 15.5 20.8214282989502 12.03066444396973 20.8214282989502 7.75 C 20.8214282989502 3.469335556030273 17.43203163146973 0 13.25 0 C 9.067968368530273 0 5.678571701049805 3.469336032867432 5.678571701049805 7.75 C 5.678571701049805 12.03066444396973 9.067968368530273 15.5 13.25 15.5 Z M 18.54999923706055 17.4375 L 17.56216430664062 17.4375 C 16.24899482727051 18.05507850646973 14.78794479370117 18.40625 13.24999904632568 18.40625 C 11.7120532989502 18.40625 10.25691890716553 18.05507850646973 8.937834739685059 17.4375 L 7.949999332427979 17.4375 C 3.560937404632568 17.4375 0 21.08242225646973 0 25.57500076293945 L 0 28.09375 C 0 29.6982421875 1.271763443946838 31 2.839285850524902 31 L 23.66071510314941 31 C 25.22823715209961 31 26.5 29.6982421875 26.5 28.09375 L 26.5 25.57500076293945 C 26.5 21.08242225646973 22.93906211853027 17.4375 18.54999923706055 17.4375 Z"  /></Svg>
                    </View>
                    )}
                    
                    {(this.state.user.full_imagem != '' && this.state.user.imagem_file_name != null && this.state.user.imagem_file_name != undefined) && (
                    <Image source={{uri: this.state.user.full_imagem}} style={{width: 50, height: 50, borderRadius: 50, borderWidth: 2, borderColor: "#FFFFFF"}}/>
                    )}
                  <View style={styles.home_perfil_grupo}>
                    <Text style={styles.home_perfil_nome}>Olá,</Text>
                    <Text style={styles.home_perfil_nome_textBold}>{this.state.user.Nome}</Text>
                  </View>
                  </TouchableOpacity>
              </View>

              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginRight: 26, width: 90}}>
                <TouchableOpacity onPress={() => Actions.NotificacoesScreen()}>
                <View style={styles.iconRank}>
                      <Icon name='bell' size={18} style={styles.statsIcon} />
                    {this.state.countNotification!=0 && (
                    <Text style={styles.badge}>{this.state.countNotification}</Text>
                    )}
                </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => Actions.EstatisticaScreen()}>
                <View style={styles.iconRank}>
                    <Icon name='chart-areaspline' size={18} style={styles.statsIcon} />
                </View>
                </TouchableOpacity>
              </View>
            </View>
            
            {this.state.homeData != null && (
            <View style={styles.como_usar}>
              <View>
                <Text style={styles.como_usar_comoUsarOApp}>Como Usar o App</Text>
              </View>
              <View style={{borderRadius: 8, marginVertical: 15, height: 190}} >
                {/* <TouchableWithoutFeedback  onPress={() => Actions.VideoScreen({ uid: this.state.homeData.video_home.uid, title: this.state.homeData.video_home.title })}> */}
                <TouchableWithoutFeedback  onPress={() => this.playVideo(this.state.homeData.video_home.uid)}>
                    <Image 
                    source={{ uri: `https://img.youtube.com/vi/${this.state.homeData.video_home.uid}/mqdefault.jpg` }} 
                    style={{borderRadius: 8,height: 180, width: '100%'}}
                    resizeMode="cover"
                    />
                </TouchableWithoutFeedback>
              </View>
            </View>
            )}
            <View style={styles.temas_semanais}>
                <View style={{flexDirection: "row", justifyContent: 'space-between'}}>
                  <View style={{width: wp('50%')}}>
                    <Text style={styles.temas_semanais_temaDaSemana}>Tema da Semana</Text>
                  </View>
                  <View style={{width: wp('20%'), alignSelf: 'flex-end'}}>
                    <TouchableWithoutFeedback onPress={() => Actions.Temas()}>
                      <Text style={styles.temas_semanais_verMais}>Ver mais</Text>
                    </TouchableWithoutFeedback>
                  </View>
                </View>
                <View>
                {this.state.homeData != null && (
                <TouchableOpacity  style={{borderRadius: 8}} onPress={() => (Boolean(this.state.homeData.tema.liberado) ?  Actions.PDFViewScreen({ pdf: this.state.homeData.tema.pdf }) : this.showModal())}>
                  <Image
                    style={styles.temas_semanais_imagem5}
                    resizeMode={'cover'}
                    source={{ uri: this.state.homeData.tema.imagem }}
                  />
                  {!Boolean(this.state.homeData.tema.liberado) && (
                    <Cadeado right={10} top={10}></Cadeado>
                  )}
                </TouchableOpacity>
                )}
                </View>
                {/* <ReactImage source={require('../assets/images/imagem5.png')} style={styles.temas_semanais_imagem5} /> */}
            </View>

            <View style={styles.jogo_redacao}>
              <TouchableOpacity style={{borderRadius: 8}} onPress={() => Actions.Jogo()}>
                  <ImageBackground 
                  style={{
                    height: 110,padding: 15,
                  }}
                  imageStyle={{
                    resizeMode: "cover", borderRadius: 8, 
                  }}
                  source={require('../assets/images/retangulo13.png')}>
                    {/* <ReactImage source={require('../assets/images/retangulo13.png')} style={styles.jogo_redacao_retangulo13} /> */}
                    <Text style={styles.jogo_redacao_jogoDaRedacao}>Jogo da Redação</Text>
                    <Text style={styles.jogo_redacao_voceEBomDeRedacaotesteSeuConhecimentoAgora}>Você é bom de redação? Teste seu conhecimento agora</Text>
                    <Svg style={styles.jogo_redacao_iconAwesomeAlignLeft} preserveAspectRatio="none" viewBox="-2.980232238769531e-7 2.249999523162842 56.5 56.5" fill="rgba(255, 255, 255, 1)"><SvgPath d="M 1.618069410324097 42.60714340209961 L 34.7033576965332 42.60714340209961 C 35.13259887695312 42.60748291015625 35.54435348510742 42.43711471557617 35.84787750244141 42.13359451293945 C 36.15139389038086 41.83007431030273 36.32175827026367 41.41831970214844 36.3214225769043 40.98907470703125 L 36.32142639160156 36.15378189086914 C 36.32175827026367 35.72454071044922 36.15139389038086 35.31278228759766 35.84787368774414 35.0092658996582 C 35.54435348510742 34.70574569702148 35.13259506225586 34.53537750244141 34.70335388183594 34.53571319580078 L 1.618069410324097 34.53571319580078 C 1.188828349113464 34.53537750244141 0.7770716547966003 34.70574569702148 0.4735521078109741 35.0092658996582 C 0.1700326353311539 35.31278228759766 -0.000334515847498551 35.72454071044922 2.190989363271001e-07 36.15378189086914 L 2.190989363271001e-07 40.98907470703125 C -0.0003354780492372811 41.41831970214844 0.1700310409069061 41.83007431030273 0.4735506176948547 42.13359451293945 C 0.7770702242851257 42.43711471557617 1.188827276229858 42.60748291015625 1.618068933486938 42.60714721679688 Z M 1.618069410324097 10.3214282989502 L 34.7033576965332 10.3214282989502 C 35.13259887695312 10.32176399230957 35.54435348510742 10.15139675140381 35.84787368774414 9.84787654876709 C 36.15139389038086 9.544357299804688 36.32175827026367 9.132600784301758 36.3214225769043 8.70335865020752 L 36.32142639160156 3.868069171905518 C 36.32175827026367 3.438827991485596 36.15139389038086 3.027071475982666 35.84787368774414 2.723551750183105 C 35.54435348510742 2.420032501220703 35.13259506225586 2.249665260314941 34.70335388183594 2.25 L 1.618069410324097 2.25 C 1.188828229904175 2.249665260314941 0.7770712971687317 2.420032024383545 0.4735518097877502 2.723551750183105 C 0.1700323224067688 3.027071475982666 -0.0003347296733409166 3.438828468322754 2.190989363271001e-07 3.868069648742676 L 2.190989363271001e-07 8.703359603881836 C -0.0003349434991832823 9.132600784301758 0.1700323224067688 9.544357299804688 0.4735521078109741 9.847877502441406 C 0.7770712375640869 10.15139675140381 1.188828349113464 10.32176399230957 1.618069767951965 10.3214282989502 Z M 54.48213958740234 18.39285850524902 L 2.017857313156128 18.39285850524902 C 0.9034255743026733 18.39285850524902 5.279105153022101e-09 19.29628372192383 2.190989363271001e-07 20.41071510314941 L 2.190989363271001e-07 24.4464282989502 C 2.190989363271001e-07 25.56086158752441 0.9034254550933838 26.46428680419922 2.01785683631897 26.46428680419922 L 54.48213958740234 26.46428680419922 C 55.59657287597656 26.46428680419922 56.5 25.56086158752441 56.5 24.4464282989502 L 56.5 20.41071510314941 C 56.5 19.2962818145752 55.59657287597656 18.39285850524902 54.48213958740234 18.39285850524902 Z M 54.48213958740234 50.67856979370117 L 2.017857313156128 50.67856979370117 C 0.9034255743026733 50.67856979370117 5.279105153022101e-09 51.58199691772461 2.190989363271001e-07 52.69642639160156 L 2.190989363271001e-07 56.73214340209961 C 2.190989363271001e-07 57.84657287597656 0.9034254550933838 58.75 2.01785683631897 58.75 L 54.48213958740234 58.75 C 55.59657287597656 58.75 56.5 57.84657287597656 56.5 56.73214340209961 L 56.5 52.69642639160156 C 56.5 51.58199691772461 55.59657287597656 50.67856979370117 54.48213958740234 50.67856979370117 Z"  /></Svg>
                  </ImageBackground>
              </TouchableOpacity>
            </View>

            {this.state.user.tipo=="3" &&(
            <View style={styles.esquema_video}>
                <Text style={styles.esquema_video_esquemaEmVideo}>Esquema em Vídeo</Text>
                <TouchableWithoutFeedback onPress={() => Actions.Esquema()}>
                  <Text style={styles.esquema_video_vermais}>Ver mais</Text>
                </TouchableWithoutFeedback>
                <ScrollView style={styles.esquema_video_repeticao}>
                  
                    {this.state.homeData != null && (
                    <FlatList
                      horizontal={true}
                      data={this.state.homeData.esquema_video}
                      renderItem={({ item }) =>
                      // <TouchableOpacity  style={{borderRadius: 8}} onPress={() => Actions.VideoScreen({ uid: item.uid, title: item.title})}>
                      <TouchableOpacity style={{borderRadius: 8}} onPress={() => this.playVideo(item.uid) }>
                        <View style={styles.esquema_video_repeticao_grupo}>
                          <View style={styles.esquema_video_repeticao_grupo_retangulo}>
                            <ReactImage source={{ uri: `https://img.youtube.com/vi/${item.uid}/mqdefault.jpg` }} style={styles.esquema_video_repeticao_grupo_imagem} />
                            <Text style={styles.esquema_video_repeticao_grupo_texto}>
                              {item.title}
                            </Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                      }
                      keyExtractor={item => item.id}
                      showsHorizontalScrollIndicator={false}
                    />
                    )}
                </ScrollView>
            </View>
            )}

            <View style={styles.esquema_video}>
                <Text style={styles.esquema_video_esquemaEmVideo}>Quadro</Text>
                <TouchableWithoutFeedback onPress={() => Actions.Quadro()}>
                  <Text style={styles.esquema_video_vermais}>Ver mais</Text>
                </TouchableWithoutFeedback>
                <ScrollView style={styles.esquema_video_repeticao}>
                    {this.state.homeData != null && (
                    <FlatList
                      horizontal={true}
                      data={this.state.homeData.quadro}
                      renderItem={({ item }) =>
                      // <TouchableOpacity  style={{borderRadius: 8}} onPress={() => (Boolean(item.liberado) ? Actions.VideoScreen({ uid: item.uid, title: item.title }) : this.showModal())}>
                      <TouchableOpacity style={{borderRadius: 8}} onPress={() =>  (Boolean(item.liberado) ? this.playVideo(item.uid) : this.showModal())}>
                        <View style={styles.esquema_video_repeticao_grupo}>
                          <View style={styles.esquema_video_repeticao_grupo_retangulo}>
                            <ReactImage source={{ uri: `https://img.youtube.com/vi/${item.uid}/mqdefault.jpg` }} style={styles.esquema_video_repeticao_grupo_imagem} />
                            <Text style={styles.esquema_video_repeticao_grupo_texto}>
                              {item.title}
                            </Text>
                            {!Boolean(item.liberado) && (
                              <Cadeado right={7} top={5}></Cadeado>
                            )}
                          </View>
                        </View>
                      </TouchableOpacity>
                      }
                      keyExtractor={item => item.id}
                      showsHorizontalScrollIndicator={false}
                    />
                    )}
                </ScrollView>
            </View>

            <View style={styles.ppt_interdisciplinar}>
                <Text style={styles.ppt_interdisciplinar_pptInterdisciplinar}>PPT Interdisciplinar</Text>
                <TouchableWithoutFeedback onPress={() => Actions.Ppts()}>
                  <Text style={styles.ppt_interdisciplinar_verMais}>Ver mais</Text>
                </TouchableWithoutFeedback>
                <View style={styles.ppt_interdisciplinar_componente51}>
                    {this.state.homeData != null && (
                    <FlatList
                      horizontal={true}
                      data={this.state.homeData.ppts}
                      renderItem={({ item }) =>
                      //<ReactImage source={require('../assets/images/imagem7.png')} style={{ marginRight: 10, padding: 0, width: 147, height: 110, borderRadius: 4 }} />
                      <TouchableOpacity  style={{borderRadius: 8}} onPress={() => (Boolean(item.liberado) ? Actions.PDFViewScreen({ pdf: item.pdf }) : this.showModal())}>
                        <Image
                          style={{ marginRight: 10, padding: 0, width: 147, height: 110, borderRadius: 4 }}
                          resizeMode={'cover'}
                          source={{ uri:  item.imagem }}
                        />
                        {!Boolean(item.liberado) && (
                          <Cadeado right={15} top={5}></Cadeado>
                        )}
                      </TouchableOpacity>
                      }
                      keyExtractor={item => item.id}
                      showsHorizontalScrollIndicator={false}
                    />
                    )}
                    {/* <ReactImage source={require('../assets/images/imagem7.png')} style={styles.ppt_interdisciplinar_componente51_imagem7} />
                    <ReactImage source={require('../assets/images/imagem8.png')} style={styles.ppt_interdisciplinar_componente51_imagem8} />
                    <ReactImage source={require('../assets/images/imagem9.png')} style={styles.ppt_interdisciplinar_componente51_imagem9} /> */}
                </View>
            </View>

            <View style={styles.podcasts}>
                <View style={{flexDirection: "row", justifyContent: "space-between", alignItems: 'flex-start'}}>
                    <Text style={styles.podcasts_podcast}>Podcast</Text>
                    <TouchableWithoutFeedback onPress={() => Actions.Podcasts()}>
                      <Text style={styles.podcasts_verMais}>Ver mais</Text>
                    </TouchableWithoutFeedback>
                </View>
                <View style={styles.podcasts_grupo}>
                    {this.state.homeData != null && (
                    <FlatList
                      horizontal={true}
                      data={this.state.homeData.podcasts}
                      renderItem={({ item }) =>
                      //<ReactImage source={require('../assets/images/imagem7.png')} style={{ marginRight: 10, padding: 0, width: 147, height: 110, borderRadius: 4 }} />
                      <View style={{marginRight: 10,borderRadius: 4}}>
                        <TouchableOpacity style={{width: 147, height: 170, backgroundColor: '#142C44',borderRadius: 4 }} onPress={() => (Boolean(item.liberado) ? Actions.PodcastPlayerScreen({ audio: item }) : this.showModal())}>
                          <View style={{width: 147, borderTopStartRadius: 4, borderTopEndRadius: 4 }}>
                            <Image
                              style={{ padding: 0, height: 80, borderTopStartRadius: 4, borderTopEndRadius: 4 }}
                              resizeMode={'cover'}
                              source={{ uri:  item.imagem }}
                            />
                            {!Boolean(item.liberado) && (
                              <Cadeado right={15} top={5}></Cadeado>
                            )}
                          </View>
                          <View style={{width: 147, height: 90, padding:7}}>
                            <Text style={{fontFamily:'Quicksand-Regular', color: '#FFF'}}>{item.titulo}</Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                      }
                      keyExtractor={item => item.id}
                      showsHorizontalScrollIndicator={false}
                    />
                    )}
                    {/* <ReactImage source={require('../assets/images/imagem10.png')} style={styles.podcasts_grupo337_imagem10} />
                    <ReactImage source={require('../assets/images/imagem11.png')} style={styles.podcasts_grupo337_imagem11} />
                    <ReactImage source={require('../assets/images/imagem12.png')} style={styles.podcasts_grupo337_imagem12} /> */}
                </View>
            </View>

            <View style={styles.ebooks}>
                <View style={{flexDirection: "row", justifyContent: "space-between", alignItems: 'flex-start'}}>
                    <Text style={styles.ebooks_eBook}>E-book</Text>
                    <TouchableWithoutFeedback onPress={() => Actions.Ebook()}>
                      <Text style={styles.ebooks_verMais}>Ver mais</Text>
                    </TouchableWithoutFeedback>
                </View>
                <View style={styles.ebooks_componente}>
                    {this.state.homeData != null && (
                    <FlatList
                      horizontal={true}
                      data={this.state.homeData.ebooks}
                      renderItem={({ item }) =>
                      //<ReactImage source={require('../assets/images/imagem7.png')} style={{ marginRight: 10, padding: 0, width: 147, height: 110, borderRadius: 4 }} />
                      <TouchableOpacity  style={{borderRadius: 8}} onPress={() => (Boolean(item.liberado) ? Actions.PDFViewScreen({ pdf: item.pdf }) : this.showModal())}>
                        <Image
                          style={{ marginRight: 10, padding: 0, width: 120, height: 90, borderRadius: 4 }}
                          resizeMode={'cover'}
                          source={{ uri:  item.imagem }}
                        />
                        {!Boolean(item.liberado) && (
                          <Cadeado right={15} top={5}></Cadeado>
                        )}
                      </TouchableOpacity>
                      }
                      keyExtractor={item => item.id}
                      showsHorizontalScrollIndicator={false}
                    />
                    )}
                    {/* <ReactImage source={require('../assets/images/imagem13.png')} style={styles.ebooks_componente61_imagem13} />
                    <ReactImage source={require('../assets/images/imagem14.png')} style={styles.ebooks_componente61_imagem14} />
                    <ReactImage source={require('../assets/images/imagem15.png')} style={styles.ebooks_componente61_imagem15} /> */}
                </View>
            </View>

            {this.state.user.tipo=="3" &&(
              <View style={styles.jogo_redacao}>
                <TouchableOpacity style={{borderRadius: 8}} onPress={() => Actions.AulaEnem()}>
                    <ImageBackground 
                    style={{
                      height: 110,padding: 15, paddingTop: 25
                    }}
                    imageStyle={{
                      resizeMode: "cover", borderRadius: 8, 
                    }}
                    source={require('../assets/images/retangulo13.png')}>
                      {/* <ReactImage source={require('../assets/images/retangulo13.png')} style={styles.jogo_redacao_retangulo13} /> */}
                      <Text style={styles.jogo_redacao_jogoDaRedacao}>Aulas ENEM 2021</Text>
                      <Text style={styles.jogo_redacao_voceEBomDeRedacaotesteSeuConhecimentoAgora}>Aulas exclusivas para você!</Text>
                      <Icon name='play-circle' size={120} style={{color: "rgba(255,255,255,0.1)", position: 'absolute', alignSelf: 'flex-start'}} />
                    </ImageBackground>
                </TouchableOpacity>
              </View>
            )}
            
            {this.state.user.tipo=="3" &&(
              <View style={styles.jogo_redacao,{paddingTop: 10, paddingBottom: 10, marginHorizontal: 26}}>
                <TouchableOpacity style={{borderRadius: 8}} onPress={() => Actions.AulaEnem2020()}>
                    <ImageBackground 
                    style={{
                      height: 110,padding: 15, paddingTop: 25
                    }}
                    imageStyle={{
                      resizeMode: "cover", borderRadius: 8, 
                    }}
                    source={require('../assets/images/retangulo14.png')}>
                      {/* <ReactImage source={require('../assets/images/retangulo13.png')} style={styles.jogo_redacao_retangulo13} /> */}
                      <Text style={styles.jogo_redacao_jogoDaRedacao}>Aulas ENEM</Text>
                      <Text style={styles.jogo_redacao_voceEBomDeRedacaotesteSeuConhecimentoAgora}>Ainda mais aulas para você!</Text>
                      <Icon name='play-circle-outline' size={120} style={{color: "rgba(255,255,255,0.1)", position: 'absolute', alignSelf: 'flex-start'}} />
                    </ImageBackground>
                </TouchableOpacity>
              </View>
            )}
            
            {this.state.user.tipo=="3" &&(
              <View style={styles.jogo_redacao,{paddingBottom: 20, marginHorizontal: 26}}>
                <TouchableOpacity style={{borderRadius: 8}} onPress={() => Actions.AulaUERJ()}>
                    <ImageBackground 
                    style={{
                      height: 110,padding: 15, paddingTop: 25
                    }}
                    imageStyle={{
                      resizeMode: "cover", borderRadius: 8, 
                    }}
                    source={require('../assets/images/retangulo15.png')}>
                      {/* <ReactImage source={require('../assets/images/retangulo13.png')} style={styles.jogo_redacao_retangulo13} /> */}
                      <Text style={styles.jogo_redacao_jogoDaRedacao}>Aulas UERJ</Text>
                      <Text style={styles.jogo_redacao_voceEBomDeRedacaotesteSeuConhecimentoAgora}>Aulas exclusivas para o vestibular da UERJ!</Text>
                      <Icon name='play-speed' size={120} style={{color: "rgba(255,255,255,0.1)", position: 'absolute', alignSelf: 'flex-start'}} />
                    </ImageBackground>
                </TouchableOpacity>
              </View>
            )}
            
            {this.state.user.tipo=="3" &&(
              <View style={styles.jogo_redacao,{paddingBottom: 20, marginHorizontal: 26}}>
                <TouchableOpacity style={{borderRadius: 8}} onPress={() => Actions.AulaConcurso()}>
                    <ImageBackground 
                    style={{
                      height: 110,padding: 15, paddingTop: 25
                    }}
                    imageStyle={{
                      resizeMode: "cover", borderRadius: 8, 
                    }}
                    source={require('../assets/images/retangulo15.png')}>
                      {/* <ReactImage source={require('../assets/images/retangulo13.png')} style={styles.jogo_redacao_retangulo13} /> */}
                      <Text style={styles.jogo_redacao_jogoDaRedacao}>Aulas Concurso</Text>
                      <Text style={styles.jogo_redacao_voceEBomDeRedacaotesteSeuConhecimentoAgora}>Prepare-se para os principais concursos do país!</Text>
                      <Icon name='play-box' size={105} style={{color: "rgba(255,255,255,0.1)", position: 'absolute', alignSelf: 'flex-start'}} />
                    </ImageBackground>
                </TouchableOpacity>
              </View>
            )}
            
            {this.state.user.tipo=="3" &&(
              <View style={styles.jogo_redacao,{paddingBottom: 20, marginHorizontal: 26}}>
                <TouchableOpacity style={{borderRadius: 8}} onPress={() => Actions.AulaMilitar()}>
                    <ImageBackground 
                    style={{
                      height: 110,padding: 15, paddingTop: 25
                    }}
                    imageStyle={{
                      resizeMode: "cover", borderRadius: 8, 
                    }}
                    source={require('../assets/images/retangulo15.png')}>
                      {/* <ReactImage source={require('../assets/images/retangulo13.png')} style={styles.jogo_redacao_retangulo13} /> */}
                      <Text style={styles.jogo_redacao_jogoDaRedacao}>Aulas Escolas Militares</Text>
                      <Text style={styles.jogo_redacao_voceEBomDeRedacaotesteSeuConhecimentoAgora}>Aulas exclusivas preparatórias para escolas militares!</Text>
                      <Icon name='play' size={100} style={{color: "rgba(255,255,255,0.1)", position: 'absolute', alignSelf: 'flex-start'}} />
                    </ImageBackground>
                </TouchableOpacity>
              </View>
            )}

            <View style={styles.ebooks}>
                <View style={{flexDirection: "row", justifyContent: "space-between", alignItems: 'flex-start'}}>
                    <Text style={styles.ebooks_eBook}>Redações Exemplares</Text>
                    <TouchableWithoutFeedback onPress={() => Actions.Exemplar()}>
                      <Text style={styles.ebooks_verMais}>Ver mais</Text>
                    </TouchableWithoutFeedback>
                </View>
                <View style={styles.ebooks_componente}>
                    {this.state.homeData != null && (
                    <FlatList
                      horizontal={true}
                      data={this.state.homeData.exemplares}
                      renderItem={({ item }) =>
                      //<ReactImage source={require('../assets/images/imagem7.png')} style={{ marginRight: 10, padding: 0, width: 147, height: 110, borderRadius: 4 }} />
                      <TouchableOpacity  style={{borderRadius: 8}} onPress={() => (Boolean(item.liberado) ? Actions.PDFViewScreen({ pdf: item.pdf }) : this.showModal())}>
                        <Image
                          style={{ marginRight: 10, padding: 0, width: 120, height: 90, borderRadius: 4 }}
                          resizeMode={'cover'}
                          source={{ uri:  item.imagem }}
                        />
                        {!Boolean(item.liberado) && (
                          <Cadeado right={15} top={5}></Cadeado>
                        )}
                      </TouchableOpacity>
                      }
                      keyExtractor={item => item.id}
                      showsHorizontalScrollIndicator={false}
                    />
                    )}
                    {/* <ReactImage source={require('../assets/images/imagem13.png')} style={styles.ebooks_componente61_imagem13} />
                    <ReactImage source={require('../assets/images/imagem14.png')} style={styles.ebooks_componente61_imagem14} />
                    <ReactImage source={require('../assets/images/imagem15.png')} style={styles.ebooks_componente61_imagem15} /> */}
                </View>
            </View>

            <View style={styles.BoxAzuis}>
                <View style={styles.boxRedacao}>
                    <View style={{ width: "48%", height: 220}}>
                      <TouchableWithoutFeedback style={{ width: "100%", height: 220}} onPress={() => Actions.Maratona()}>
                        <View style={{ width: "100%", height: 200}}>
                          <ImageBackground 
                            style={{
                              height: 188,padding: 15,
                              justifyContent: 'space-around',
                            }}
                            imageStyle={{
                              resizeMode: "cover", borderRadius: 8, 
                              height: 188,
                            }}
                            source={require('../assets/images/grupoDeMascara2.png')}>
                                {/* <ReactImage source={require('../assets/images/grupoDeMascara2.png')} style={styles.boxRedacao_componente71_grupoDeMascara2} /> */}
                                <Svg style={styles.boxRedacao_componente71_iconAwesomeRunning} preserveAspectRatio="none" viewBox="0 0 55.9149169921875 68.815673828125" fill="rgba(255, 255, 255, 1)"><SvgPath d="M 36.55975341796875 12.9034423828125 C 40.12298583984375 12.9034423828125 43.011474609375 10.01495265960693 43.011474609375 6.45172119140625 C 43.011474609375 2.888489961624146 40.12298583984375 0 36.55975341796875 0 C 32.99652099609375 0 30.1080322265625 2.888489484786987 30.1080322265625 6.45172119140625 C 30.1080322265625 10.01495265960693 32.99652099609375 12.9034423828125 36.55975341796875 12.9034423828125 Z M 15.28117084503174 42.67141342163086 L 13.29189014434814 47.31127548217773 L 4.3011474609375 47.31127548217773 C 1.92610764503479 47.31127548217773 0 49.23738479614258 0 51.61242294311523 C 0 53.98746490478516 1.92610764503479 55.91357040405273 4.3011474609375 55.91357040405273 L 14.71126842498779 55.91357040405273 C 17.29867744445801 55.91357040405273 19.62801742553711 54.37591171264648 20.64013290405273 52.00355911254883 L 21.82160377502441 49.24544906616211 L 20.3874397277832 48.39866256713867 C 18.0594425201416 47.02363586425781 16.34704780578613 44.98865509033203 15.28116989135742 42.67141342163086 Z M 51.61376953125 30.106689453125 L 45.69565963745117 30.106689453125 L 42.19291305541992 22.94931030273438 C 40.51277542114258 19.51511383056641 37.42804718017578 17.00431823730469 33.88900756835938 16.1024227142334 L 24.33508491516113 13.26097583770752 C 20.53125762939453 12.3469820022583 16.57017135620117 13.18704986572266 13.46931171417236 15.56477832794189 L 8.137232780456543 19.65221405029297 C 6.251449108123779 21.09712982177734 5.89391565322876 23.79610061645508 7.3415207862854 25.681884765625 C 8.789126396179199 27.56766891479492 11.4867525100708 27.92116928100586 13.37119102478027 26.47759819030762 L 18.70595741271973 22.39016342163086 C 19.73688888549805 21.59848403930664 21.05008316040039 21.31487655639648 22.10251998901367 21.56488037109375 L 24.07836151123047 22.15225601196289 L 19.04333114624023 33.89842224121094 C 17.34706687927246 37.86085510253906 18.86725234985352 42.5020637512207 22.57833480834961 44.69295883178711 L 34.00056838989258 51.43635177612305 L 30.30830383300781 63.22821807861328 C 29.59861373901367 65.49439239501953 30.86073112487793 67.90705871582031 33.12689971923828 68.61675262451172 C 33.55566787719727 68.75115966796875 33.98847198486328 68.815673828125 34.41455459594727 68.815673828125 C 36.2438850402832 68.815673828125 37.94015121459961 67.63688659667969 38.51677322387695 65.79815673828125 L 42.76953125 52.21459197998047 C 43.56389999389648 49.42288208007812 42.38108444213867 46.42417526245117 39.86088180541992 44.90398788452148 L 31.62956047058105 40.04637908935547 L 35.83796691894531 29.52469635009766 L 38.56247329711914 35.09333801269531 C 39.63776016235352 37.28961181640625 41.91199111938477 38.70764541625977 44.35692596435547 38.70764541625977 L 51.61376953125 38.70764541625977 C 53.98881149291992 38.70764541625977 55.9149169921875 36.78153991699219 55.9149169921875 34.40649795532227 C 55.9149169921875 32.03145980834961 53.98881149291992 30.1066951751709 51.61376953125 30.1066951751709 Z"  /></Svg>
                                <Text style={styles.boxRedacao_componente71_maratonaenem}>Maratona ENEM</Text>
                          </ImageBackground>
                        </View>
                      </TouchableWithoutFeedback>
                    </View>
                    <View style={{ width: "48%", height: 200}}>
                      <TouchableWithoutFeedback style={{ width: "100%", height: 220}} onPress={() => Actions.FacaRedacao()}>
                            <View style={{ width: "100%", height: 220}}>
                              <ImageBackground 
                                  style={{
                                    height: 188,padding: 15,
                                    justifyContent: 'space-around',
                                  }}
                                  imageStyle={{
                                    resizeMode: "cover", borderRadius: 8, 
                                    height: 188,
                                  }}
                                  source={require('../assets/images/grupoDeMascara3.png')}>
                                  {/* <ReactImage source={require('../assets/images/grupoDeMascara3.png')} style={styles.boxRedacao_componente81_grupoDeMascara3} /> */}
                                  <Svg style={styles.boxRedacao_componente71_iconAwesomeRunning} preserveAspectRatio="none" viewBox="0.0019078850746154785 4.499015808105469 63.245880126953125 37.962158203125" fill="rgba(255, 255, 255, 1)"><SvgPath d="M 61.60638427734375 17.15398979187012 C 56.48563385009766 17.49998474121094 49.18016815185547 22.56142234802246 45.48294830322266 24.22220420837402 C 42.60623168945312 25.51721954345703 40.12494277954102 26.63429641723633 37.95999526977539 26.63429641723633 C 35.72584533691406 26.63429641723633 35.38973236083984 25.0328254699707 35.8543586730957 21.50366020202637 C 35.96309661865234 20.71281051635742 37.0109748840332 13.67425060272217 31.63319778442383 13.98070430755615 C 29.15191078186035 14.12898921966553 25.27674865722656 16.43233871459961 14.87707901000977 26.43658447265625 L 18.97961044311523 16.18519973754883 C 21.98483657836914 8.682015419006348 13.72046089172363 1.208489298820496 6.15796422958374 6.022784233093262 L 0.7307611107826233 9.670575141906738 C -0.0007745143375359476 10.13520050048828 -0.2182580679655075 11.11387634277344 0.2463658899068832 11.85529708862305 L 1.946691989898682 14.52441310882568 C 2.41131591796875 15.26583576202393 3.389991998672485 15.48331928253174 4.131413459777832 15.00880908966064 L 9.865071296691895 11.16330432891846 C 11.68402481079102 10.00668621063232 13.88851737976074 11.87506866455078 13.0976676940918 13.84230613708496 L 3.389991998672485 38.12137985229492 C 2.717769861221313 39.79204940795898 3.656903505325317 42.46116638183594 6.326020240783691 42.46116638183594 C 7.146526336669922 42.46116638183594 7.957147121429443 42.14482498168945 8.56017017364502 41.53191757202148 C 12.73189926147461 37.36018753051758 23.85321998596191 26.63429260253906 29.43859481811523 22.17588233947754 C 29.22110939025879 24.99328231811523 29.23099708557129 27.99850845336914 31.47502899169922 30.46002769470215 C 32.98752975463867 32.12080764770508 35.1623649597168 32.96108627319336 37.95010757446289 32.96108627319336 C 41.46938705444336 32.96108627319336 44.67232894897461 31.51778984069824 48.06309509277344 29.99540328979492 C 51.32535171508789 28.53233337402344 57.84985733032227 23.80700492858887 61.74479293823242 23.49066734313965 C 62.58506774902344 23.42146873474121 63.24740219116211 22.76901817321777 63.24740219116211 21.92874145507812 L 63.24740219116211 18.75545883178711 C 63.26716995239258 17.85586738586426 62.5059814453125 17.09467506408691 61.60639190673828 17.15398788452148 Z"  /></Svg>
                                  <Text style={styles.boxRedacao_componente71_maratonaenem}>Faça sua Redação</Text>
                              </ImageBackground>
                            </View>
                      </TouchableWithoutFeedback>
                    </View>
                </View>
            </View>
          </ScrollView>
        
        </ParallaxScroll>

        <ActionButton buttonColor='#95191C' offsetX={25} offsetY={25}>
          {this.state.homeData != null && (
          this.state.homeData.whatsapp == 1 && (
            <ActionButton.Item buttonColor='#142C44' title="Monitoria Online" onPress={() => Linking.openURL('whatsapp://send?text=Olá! Preciso de monitoria online&phone=+5521996813001')}>
              <Icon name="whatsapp" size={28} style={styles.fabIcon} />
            </ActionButton.Item>
          )
          )}
          <ActionButton.Item buttonColor='#107AE8' title="Fale com a Hória" onPress={() => Actions.chat()} >
            <Icon name="chat" size={28} style={styles.fabIcon} />
          </ActionButton.Item>
        </ActionButton>
      </View>
    );
  }
}

HomeScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  scroll_view: {
    //flex: 1,
  },
  imgBack: {
      flex: 1,
  },
  elipse: {
    opacity: 1,
    position: "relative",
    marginTop: 0,
    marginRight: 0,
    marginBottom: 0,
    //marginLeft: -335.5,
    //width: 671,
    height: 477,
    //left: "50%",
    //top: -298
  },
  home_perfil: {
    opacity: 1,
    backgroundColor: "transparent",
    height: 50,
    marginHorizontal: 26
  },
  home_perfil_grupo: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "transparent",
    width: 50,
    height: 50,
    paddingVertical: 14
  },
  home_perfil_grupo_elipse: {
    opacity: 1,
    position: "absolute",
    width: 50,
    height: 50,
    left: 0,
    top: 0
  },
  home_perfil_grupo_iconAwesomeUser: {
    opacity: 1,
    position: "absolute",
    width: 26.5,
    height: 31,
    left: 12,
    top: 10
  },
  home_perfil_nome_textBold: {
    opacity: 1,
    position: "relative",
    backgroundColor: "rgba(255, 255, 255, 0)",
    color: "rgba(255, 255, 255, 1)",
    fontSize: 14,
    fontFamily: "Quicksand-Bold",
    textAlign: "left",
    lineHeight: 14,
    width: 91,
    left: 61
  },
  home_perfil_nome: {
    opacity: 1,
    position: "relative",
    backgroundColor: "rgba(255, 255, 255, 0)",
    color: "rgba(255, 255, 255, 1)",
    fontSize: 14,
    fontFamily: "Quicksand-Regular",
    textAlign: "left",
    lineHeight: 14,
    width: 91,
    left: 61
  },
  badge: {
    fontSize: 12,
    fontFamily: "Quicksand-Bold",
    backgroundColor: '#910404',
    paddingVertical: 3,
    paddingHorizontal: 6,
    position: "absolute",
    borderRadius: 25,
    color: '#FFF',
    top: 0,
    right: 0,
  },
  iconRank: {
    opacity: 1,
    backgroundColor: "#0A5CAF",
    justifyContent: "center",
    alignItems: "center",
    width: 40,
    height: 40,
    top: 0,
    borderRadius: 20
  },
  statsIcon: {
    color: "#FFFFFF",
    alignSelf: "center"
  },
  iconRank_elipse4: {
    opacity: 1,
    position: "absolute",
    width: 40,
    height: 40,
    left: 0,
    top: 0
  },
  como_usar: {
    marginBottom: 0,
    marginTop: 0,
    marginHorizontal: 26
  },
  como_usar_grupoDeMascara1: {
    //width: 308,
    height: 155,
  },
  como_usar_grupoDeMascara1_retangulo6: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "#142C44",
    // width: 323,
    height: 166,
    left: -7,
    top: -6
  },
  como_usar_grupoDeMascara1_imagem4: {
    opacity: 0.7869420051574707,
    position: "absolute",
    //width: 323,
    height: 179,
    left: -7,
    top: -15
  },
  como_usar_grupoDeMascara1_retangulo4: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "rgba(255, 255, 255, 1)",
    borderTopWidth: 1,
    borderTopColor: "rgba(112, 112, 112, 1)",
    borderRightWidth: 1,
    borderRightColor: "rgba(112, 112, 112, 1)",
    borderBottomWidth: 1,
    borderBottomColor: "rgba(112, 112, 112, 1)",
    borderLeftWidth: 1,
    borderLeftColor: "rgba(112, 112, 112, 1)",
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    shadowColor: "rgb(0,  0,  0)",
    shadowOpacity: 0.3137254901960784,
    shadowOffset: {
      width: 3,
      height: 3
    },
    shadowRadius: 6,
    width: 308,
    height: 155,
    left: 0,
    top: 0
  },
  como_usar_comoUsarOApp: {
    color: "rgba(255, 255, 255, 1)",
    textShadowColor: 'rgba(0, 0, 0, 0.9)',
    textShadowOffset: {width: 0.2, height: 0.2},
    textShadowRadius: 2,
    fontSize: 14,
    fontFamily: "Quicksand-Bold",
    textAlign: "left",
    lineHeight: 28,
    width: 124,
  },
  iconRank_iconIonicMdTrophy: {
    opacity: 1,
    position: "absolute",
    width: 18,
    height: 18,
    left: 11,
    top: 11
  },
  temas_semanais: {
    marginLeft: 26,
    marginRight: 26,
    height: 202,
    marginTop: 0,
    marginBottom: 28
  },
  temas_semanais_verMais: {
    opacity: 1,
    color: "rgba(155, 155, 155, 1)",
    fontSize: 12,
    fontFamily: "Quicksand-Regular",
    textAlign: "right",
    lineHeight: 16,
    height: 30,
    textShadowColor: 'rgba(255, 255, 255, 1)',
    textShadowOffset: {width: 0.5, height: 0.5},
    textShadowRadius: 1,
  },
  temas_semanais_temaDaSemana: {
    opacity: 1,
    color: "#142C44",
    fontSize: 16,
    fontFamily: "Quicksand-Bold",
    textAlign: "left",
    lineHeight: 16,
    height: 30,
    textShadowColor: 'rgba(255, 255, 255, 1)',
    textShadowOffset: {width: 0.5, height: 0.5},
    textShadowRadius: 1,
  },
  temas_semanais_imagem5: {
    opacity: 1,
    //width: 308,
    height: 173,
    borderRadius: 8
  },
  jogo_redacao: {
    marginHorizontal: 26,
    borderRadius: 8
  },
  jogo_redacao_retangulo13: {
    opacity: 1,
    position: "absolute",
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    //width: 308,
    height: 108,
    left: 0,
    top: 0
  },
  jogo_redacao_jogoDaRedacao: {
    opacity: 1,
    backgroundColor: "rgba(255, 255, 255, 0)",
    color: "rgba(255, 255, 255, 1)",
    fontSize: 24,
    fontFamily: "Quicksand-Bold",
    textAlign: "right",
    lineHeight: 24,
    width: wp('100%'),
    height: 28,
    top: 5,
    alignSelf: 'flex-end'
  },
  jogo_redacao_voceEBomDeRedacaotesteSeuConhecimentoAgora: {
    opacity: 1,
    color: "rgba(255, 255, 255, 1)",
    fontSize: 14,
    fontFamily: "Quicksand-Regular",
    textAlign: "right",
    lineHeight: 17,
    width: 230,
    top: 10,
    alignSelf: 'flex-end'
  },
  jogo_redacao_iconAwesomeAlignLeft: {
    opacity: 0.5,
    position: "absolute",
    width: 56.5,
    height: 56.5,
    left: 15.5,
    top: 25.5
  },
  esquema_video: {
    opacity: 1,
    position: "relative",
    backgroundColor: "transparent",
    height: 217,
    marginLeft: 26,
    marginRight: 26,
    marginTop: 28
  },
  esquema_video_repeticao: {
    opacity: 1,
    position: "absolute",
    backgroundColor:  "transparent",
    //width: 461,
    height: 186,
    left: 0,
    top: 31
  },
  esquema_video_esquemaEmVideo: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "rgba(255, 255, 255, 0)",
    color: "#142C44",
    fontSize: 16,
    fontFamily: "Quicksand-Bold",
    textAlign: "left",
    lineHeight: 38,
    width: 154,
    height: 40,
    left: 0,
    top: -10
  },
  esquema_video_vermais: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "rgba(255, 255, 255, 0)",
    color: "rgba(155, 155, 155, 1)",
    fontSize: 12,
    fontWeight: "400",
    fontStyle: "normal",
    fontFamily: "Quicksand-Regular",
    textAlign: "right",
    lineHeight: 38,
    width: "50%",
    height: 40,
    top: -7,
    right: 0
  },
  esquema_video_repeticao_grupo: {
    opacity: 1,
    position: "relative",
    backgroundColor: "transparent",
    width: 147,
    height: 186,
    marginRight: 10
  },
  esquema_video_repeticao_grupo_retangulo: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "#142C44",
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    width: 147,
    height: 186,
    left: 0,
    top: 0
  },
  esquema_video_repeticao_grupo_imagem: {
    opacity: 1,
    position: "absolute",
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    width: 147,
    height: 83,
    left: 0,
    top: 0
  },
  esquema_video_repeticao_grupo_texto: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "rgba(255, 255, 255, 0)",
    color: "rgba(255, 255, 255, 1)",
    fontSize: 16,
    fontWeight: "400",
    fontStyle: "normal",
    fontFamily: "Quicksand-Regular",
    textAlign: "left",
    lineHeight: 16,
    width: 138,
    height: 93,
    left: 5,
    top: 88
  },
  ppt_interdisciplinar: {
    opacity: 1,
    position: "relative",
    backgroundColor: "transparent",
    marginTop: 20,
    marginBottom: 10,
    height: 143,
    marginHorizontal: 26
  },
  ppt_interdisciplinar_verMais: {
    opacity: 1,
    color: "rgba(155, 155, 155, 1)",
    fontSize: 12,
    fontFamily: "Quicksand-Regular",
    textAlign: "right",
    lineHeight: 16,
    width: "50%",
    alignSelf: 'flex-end',
  },
  ppt_interdisciplinar_pptInterdisciplinar: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "rgba(255, 255, 255, 0)",
    color: "#142C44",
    fontSize: 16,
    fontFamily: "Quicksand-Bold",
    textAlign: "left",
    lineHeight: 38,
    width: 154,
    height: 40,
    left: 0,
    top: -10
  },
  ppt_interdisciplinar_componente51: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "transparent",
    //width: 463,
    height: 112,
    left: 0,
    top: 31
  },
  ppt_interdisciplinar_componente51_imagem7: {
    opacity: 1,
    position: "absolute",
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    width: 147,
    height: 110,
    left: 0,
    top: 0
  },
  ppt_interdisciplinar_componente51_imagem8: {
    opacity: 1,
    position: "absolute",
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    width: 147,
    height: 110,
    left: 157,
    top: 0
  },
  ppt_interdisciplinar_componente51_imagem9: {
    opacity: 1,
    position: "absolute",
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    width: 149,
    height: 112,
    left: 314,
    top: 0
  },
  podcasts: {
    height: 190,
    marginHorizontal: 26,
    marginBottom: 26
  },
  podcasts_verMais: {
    opacity: 1,
    color: "rgba(155, 155, 155, 1)",
    fontSize: 12,
    fontFamily: "Quicksand-Regular",
    textAlign: "right",
    lineHeight: 40,
    width: "50%",
    alignSelf: 'flex-end',
    top:0
  },
  podcasts_podcast: {
    color: "#142C44",
    fontSize: 16,
    fontFamily: "Quicksand-Bold",
    textAlign: "left",
    lineHeight: 16,
    width: "30%",
    lineHeight: 40,
    left: 0,
    alignSelf: 'flex-start',
  },
  podcasts_grupo: {
    opacity: 1,
    height: 173,
  },
  podcasts_grupo337_imagem10: {
    opacity: 1,
    position: "absolute",
    width: 146.52,
    height: 90,
    left: 0,
    top: 0
  },
  podcasts_grupo337_imagem11: {
    opacity: 1,
    position: "absolute",
    width: 134.86,
    height: 90,
    left: 158,
    top: 0
  },
  podcasts_grupo337_imagem12: {
    opacity: 1,
    position: "absolute",
    width: 162,
    height: 90,
    left: 304,
    top: 0
  },
  ebooks: {
    height: 120,
    marginHorizontal: 26,
    marginBottom: 26
  },
  ebooks_verMais: {
    color: "rgba(155, 155, 155, 1)",
    fontSize: 12,
    fontWeight: "400",
    fontStyle: "normal",
    fontFamily: "Quicksand-Regular",
    textAlign: "right",
    lineHeight: 38,
    width: wp('20%'),
    height: 40,
    alignSelf: 'flex-end'
  },
  ebooks_eBook: {
    backgroundColor: "rgba(255, 255, 255, 0)",
    color: "#142C44",
    fontSize: 16,
    fontFamily: "Quicksand-Bold",
    textAlign: "left",
    lineHeight: 38,
    width: wp('50%'),
    height: 40,
  },
  ebooks_componente: {
    height: 90,
  },
  ebooks_componente61_imagem13: {
    opacity: 1,
    position: "absolute",
    width: 120,
    height: 90,
    left: 0,
    top: 0
  },
  ebooks_componente61_imagem14: {
    opacity: 1,
    position: "absolute",
    width: 120,
    height: 90,
    left: 134,
    top: 0
  },
  ebooks_componente61_imagem15: {
    opacity: 1,
    position: "absolute",
    width: 120,
    height: 90,
    left: 268,
    top: 0
  },
  BoxAzuis:{
    marginHorizontal: 26,
    height: 220,
    marginVertical: 20,
    paddingBottom: 28,
  },
  boxRedacao: {
    height: 220,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  boxMaratona: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "transparent",
    width: 56,
    height: "auto",
    top: 688,
    right: 23,
    bottom: 1091
  },
  boxMaratona_tealCircle: {
    opacity: 1,
    position: "absolute",
    shadowColor: "rgb(0,  0,  0)",
    shadowOpacity: 0.27058823529411763,
    shadowOffset: {
      width: 1,
      height: 2
    },
    shadowRadius: 6,
    width: 56,
    height: 56,
    left: 0,
    top: 0
  },
  boxMaratona_icaddwhite58657250: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "transparent",
    width: 14,
    height: 14,
    left: 21,
    top: 21
  },
  boxMaratona_icaddwhite58657250_icaddwhite: {
    opacity: 1,
    position: "absolute",
    width: 14,
    height: 14,
    left: 0,
    top: 0
  },
  boxMaratona_grupo333: {
    opacity: 0.11999999731779099,
    position: "absolute",
    backgroundColor: "transparent",
    width: 56,
    height: 56,
    left: 0,
    top: 0
  },
  boxMaratona_grupo333_gradientBorder2: {
    opacity: 1,
    position: "absolute",
    width: 56,
    height: 56,
    left: 0,
    top: 0
  },
  boxMaratona_grupo333_gradientBorder1: {
    opacity: 1,
    position: "absolute",
    width: 56,
    height: 56,
    left: 0,
    top: 0
  },
  boxRedacao_componente71: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "transparent",
    width: "50%",
    height: 188,
    left: 0,
    top: 0
  },
  boxRedacao_componente71_grupoDeMascara2: {
    opacity: 1,
    position: "absolute",
    width: 148,
    height: 188,
    left: 0,
    top: 0
  },
  boxRedacao_componente71_grupoDeMascara2_retangulo9: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "rgba(16, 122, 232, 1)",
    borderTopWidth: 1,
    borderTopColor: "rgba(112, 112, 112, 1)",
    borderRightWidth: 1,
    borderRightColor: "rgba(112, 112, 112, 1)",
    borderBottomWidth: 1,
    borderBottomColor: "rgba(112, 112, 112, 1)",
    borderLeftWidth: 1,
    borderLeftColor: "rgba(112, 112, 112, 1)",
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    width: 171,
    height: 209,
    left: -11,
    top: -8
  },
  boxRedacao_componente71_grupoDeMascara2_imagem16: {
    opacity: 0.18915100395679474,
    position: "absolute",
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    width: 158,
    height: 223,
    left: -4,
    top: -2
  },
  boxRedacao_componente71_grupoDeMascara2_retangulo7: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "rgba(255, 255, 255, 1)",
    borderTopWidth: 1,
    borderTopColor: "rgba(112, 112, 112, 1)",
    borderRightWidth: 1,
    borderRightColor: "rgba(112, 112, 112, 1)",
    borderBottomWidth: 1,
    borderBottomColor: "rgba(112, 112, 112, 1)",
    borderLeftWidth: 1,
    borderLeftColor: "rgba(112, 112, 112, 1)",
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    width: 148,
    height: 188,
    left: 0,
    top: 0
  },
  boxRedacao_componente71_iconAwesomeRunning: {
    width: 55.91,
    height: 68.82,
    margin: 25,
    alignSelf: 'center'
  },
  boxRedacao_componente71_maratonaenem: {
    color: "rgba(255, 255, 255, 1)",
    //fontSize: 24,
    fontSize: RFValue(16),
    fontFamily: "Quicksand-Bold",
    textAlign: "center",
    lineHeight: 24,
    paddingTop: 2.4000000000000004,
    width: wp('100%'),
    height: 52,
    alignSelf: 'center'
  },
  boxRedacao_componente81: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "transparent",
    width: 148,
    height: 188,
    top: 0,
    right: 0
  },
  boxRedacao_componente81_grupoDeMascara3: {
    opacity: 1,
    position: "absolute",
    width: 148,
    height: 188,
    left: 0,
    top: 0
  },
  boxRedacao_componente81_grupoDeMascara3_retangulo10: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "rgba(16, 122, 232, 1)",
    borderTopWidth: 1,
    borderTopColor: "rgba(112, 112, 112, 1)",
    borderRightWidth: 1,
    borderRightColor: "rgba(112, 112, 112, 1)",
    borderBottomWidth: 1,
    borderBottomColor: "rgba(112, 112, 112, 1)",
    borderLeftWidth: 1,
    borderLeftColor: "rgba(112, 112, 112, 1)",
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    width: 171,
    height: 209,
    left: -11,
    top: -8
  },
  boxRedacao_componente81_grupoDeMascara3_imagem18: {
    opacity: 0.20000000298023224,
    position: "absolute",
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    width: 362,
    height: 203,
    left: -170,
    top: -8
  },
  boxRedacao_componente81_grupoDeMascara3_retangulo11: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "rgba(255, 255, 255, 1)",
    borderTopWidth: 1,
    borderTopColor: "rgba(112, 112, 112, 1)",
    borderRightWidth: 1,
    borderRightColor: "rgba(112, 112, 112, 1)",
    borderBottomWidth: 1,
    borderBottomColor: "rgba(112, 112, 112, 1)",
    borderLeftWidth: 1,
    borderLeftColor: "rgba(112, 112, 112, 1)",
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    width: 148,
    height: 188,
    left: 0,
    top: 0
  },
  boxRedacao_componente81_facaSuaredacao: {
    opacity: 1,
    position: "absolute",
    backgroundColor: "rgba(255, 255, 255, 0)",
    color: "rgba(255, 255, 255, 1)",
    fontSize: 24,
    fontFamily: "Quicksand-Bold",
    textAlign: "center",
    lineHeight: 24,
    paddingTop: 2.4000000000000004,
    paddingRight: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    width: 106,
    height: 52,
    left: 21,
    top: 114
  },
  boxRedacao_componente81_iconAwesomeSignature: {
    opacity: 1,
    position: "absolute",
    width: 63.25,
    height: 37.96,
    left: 45.37,
    top: 43.02
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: (Platform.OS === 'android'? 30 : 60),
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 150,
    height: 150,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
    paddingTop: 10
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  menuHome: {
    display: 'flex',
    paddingVertical: 15,
    flexDirection: 'column'
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  fabIcon: {
    color: '#FFFFFF'
  },
});
