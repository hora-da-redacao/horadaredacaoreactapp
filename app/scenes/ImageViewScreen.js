import React from 'react';
import { StyleSheet, Image, Dimensions, View, Text, ActivityIndicator } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ImageZoom from 'react-native-image-pan-zoom';
import ImageLoad from 'react-native-image-placeholder'; 

export default class ImageViewScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isAuthenticated: false,
            isLoading: false
        };
    }

    DefinindoOrientacao() {
        //Orientation.lockToLandscape();
    }

    componentWillUnmount() {
        //Orientation.lockToPortrait();
        console.log(this.props.image);
    }
    render() {
        return (
        <View style={styles.container}>
            <ImageZoom cropWidth={Dimensions.get('window').width}
                       cropHeight={Dimensions.get('window').height}
                       imageWidth={Dimensions.get('window').width}
                       imageHeight={Dimensions.get('window').height}>
                <ImageLoad
                    style={{
                        flex: 1,
                        alignSelf: 'stretch',
                        width: undefined,
                        height: hp("100%")
                    }}
                    loadingStyle={{ size: 'small', color: 'gray' }}
                    isShowActivity={true}
                    source={{ uri: this.props.image }}
                    onLoadStart={(e) => this.setState({isLoading: true})}
                    onLoadEnd={(e) =>  this.setState({isLoading: false})}
                />
            </ImageZoom>
        </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    image: {
        flex:1,
        width: wp('100%')
        //width: Dimensions.get('window').width,
    },
    loader:{
      padding:20
    }
});